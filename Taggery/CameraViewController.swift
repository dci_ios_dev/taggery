//
//  CameraViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 07/03/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: SwiftyCamViewController,SwiftyCamViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVCaptureVideoDataOutputSampleBufferDelegate {
    
    @IBOutlet weak var videoTimer: UILabel!
    
    @IBOutlet var utilityButtons : [UIView]!
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var cameraFBbuttton: UIButton!
    @IBOutlet var showCameraView: UIView!
    @IBOutlet var captureButton: SwiftyRecordButton!
    let imagePicker = UIImagePickerController()
    var screenSize = UIScreen.main.bounds
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    
    var videoImageOutputs:AVCaptureVideoDataOutput?
    
  
    
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    // let WebServices = WebServicesHandler()
    
    var capturedImage = UIImage()
    var imageViewIdendity = String()
    // var imagesArray = [UIImage]()
    var imageUrl:URL!
    var cameraPlace = Int()
    var counter = 0
    
    var timer = Timer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
          cameraDelegate = self
        
        captureButton.delegate = self
        captureButton.isEnabled = true
        audioEnabled = true
        maximumVideoDuration = 500.0
        Utilities.setBackgroungImage(self.view)
        self.captureButton.layer.cornerRadius = 40
        defaultCamera = .rear
        videoQuality = .low
        self.videoTimer.isHidden = true
        
        
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeLeft.direction = .left
//        self.view.addGestureRecognizer(swipeLeft)
//        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeRight.direction = .right
//        self.view.addGestureRecognizer(swipeRight)
//        
//        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeUp.direction = .up
//        self.view.addGestureRecognizer(swipeUp)
//        
//        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeDown.direction = .down
//        self.view.addGestureRecognizer(swipeDown)
//        
        
        
    }
    
    func startTimer(){
        self.videoTimer.isHidden = false
        self.timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
    }
    func stopTimer(){
        self.videoTimer.text = ""
        self.videoTimer.isHidden = true
        self.timer.invalidate()
        counter = 0
    }
    
    @objc func timerAction() {
        counter = counter + 1
        var data = secondsToHoursMinutesSeconds(seconds: Double(counter))
       
        
        //counter += 1
        //label.text = "\(counter)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       // previewLayer?.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ImagePicker Methods
    @objc func locationselected(notification:NSNotification){
        print("SHI")
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let tempImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageData  = UIImageJPEGRepresentation(tempImage, 0.7)

        let filtersViewController = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        filtersViewController.imageUrl = self.imageUrl
        filtersViewController.selectedImage = tempImage
        filtersViewController.selectedImageData = imageData!
        self.navigationController?.pushViewController(filtersViewController, animated: false)
                    
        
        self.capturedImage = tempImage;
        // self.loadImageForSellView()
    }
    
    // MARK: - Additional Methods
    
    func loadCamera()
    {
        captureSession = AVCaptureSession()
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)

        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            //Utility.showAlertView(String(describing: error), onView: self)
            print("error")
            input = nil
        }

        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)

            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecType.jpeg]
            if captureSession!.canAddOutput(stillImageOutput!) {
                captureSession!.addOutput(stillImageOutput!)

                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                
                showCameraView.layer.addSublayer(previewLayer!)
                showCameraView.addSubview(captureButton)
                showCameraView.addSubview(cameraFBbuttton)
                showCameraView.addSubview(flashButton)
                

                // previewLayer!.frame = CGRect(x:0, y:0, width:showCameraView.frame.width, height:showCameraView.frame.height)
                print(previewLayer!.frame)
               captureSession!.startRunning()
            }
        }
    }
    
    func loadvideo()
    {
        captureSession = AVCaptureSession()
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            //Utility.showAlertView(String(describing: error), onView: self)
            print("error")
            input = nil
        }
        
        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)
            
            videoImageOutputs = AVCaptureVideoDataOutput()
//stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if #available(iOS 11.0, *) {
                videoImageOutputs!.recommendedVideoSettings(forVideoCodecType: .h264, assetWriterOutputFileType: .mp4)
            } else {
                // Fallback on earlier versions
                
                videoImageOutputs?.videoSettings = [:]
            }
            
            
            if captureSession!.canAddOutput(videoImageOutputs!) {
                captureSession!.addOutput(videoImageOutputs!)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                print(previewLayer!)
                
                showCameraView.layer.addSublayer(previewLayer!)
                
                // previewLayer!.frame = CGRect(x:0, y:0, width:showCameraView.frame.width, height:showCameraView.frame.height)
                
                
                
                captureSession!.startRunning()
            }
        }
        
        
    }
    
    func handleSwiftyCamguesture(_ swiftyCam: SwiftyCamViewController, handleDirection string: String) {
                if(string == "Right"){
        
                    let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                    let transition = CATransition()
                    transition.duration = 0.40
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromLeft
                    navigationController?.view.layer.add(transition, forKey: kCATransition)
                    navigationController?.pushViewController(leftViewController, animated: false)
                }
                if(string == "Left"){
                    let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! PostsViewController
                    let transition = CATransition()
                    transition.duration = 0.40
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromRight
                    navigationController?.view.layer.add(transition, forKey: kCATransition)
                    navigationController?.pushViewController(leftViewController, animated: false)
                }
                if(string == "Up"){
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.view.frame = CGRect(x: 0, y: 100, width: screenSize.size.width, height: screenSize.size.height-200)
                    imagePicker.sourceType = .savedPhotosAlbum
                    present(imagePicker, animated: true, completion: nil)
                }
                if(string == "Down"){
        
                }
    }
    
//    func handleSwiftyCamguesture(_ swiftyCam: SwiftyCamViewController, handleDirection direction: String) {
//        if(direction == "Right"){
//            
//        }
//        if(direction == "Left"){
//            let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//            
//            let transition = CATransition()
//            transition.duration = 0.40
//            transition.type = kCATransitionPush
//            transition.subtype = kCATransitionFromRight
//            navigationController?.view.layer.add(transition, forKey: kCATransition)
//            navigationController?.pushViewController(leftViewController, animated: false)
//        }
//        if(direction == "Up"){
//            imagePicker.delegate = self
//            imagePicker.allowsEditing = false
//            imagePicker.view.frame = CGRect(x: 0, y: 100, width: screenSize.size.width, height: screenSize.size.height-200)
//            imagePicker.sourceType = .savedPhotosAlbum
//            present(imagePicker, animated: true, completion: nil)
//        }
//        if(direction == "Down"){
//            
//        }
//    }
    
    
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
    
        self.videoTimer.isHidden = false
        for utility in self.utilityButtons{
            utility.isHidden = true
        }
        self.startTimer()
       
        
        self.captureButton.growButton()
        
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
       
        for utility in self.utilityButtons{
            utility.isHidden = false
        }
        self.stopTimer()
        self.captureButton.shrinkButton()
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
       

        self.stopTimer()
        for utility in self.utilityButtons{
            utility.isHidden = false
        }
        print("ROOE \(error.localizedDescription)")
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
         self.stopTimer()
        print("Recording Stopped")
        for utility in self.utilityButtons{
            utility.isHidden = false
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
             let videoPreviewViewController = self.storyboard?.instantiateViewController(withIdentifier: "VideoPreviewViewController") as! VideoPreviewViewController
                videoPreviewViewController.mainurl = url
                print("URL \(url)")
            self.navigationController?.pushViewController(videoPreviewViewController, animated: true)
        }
   
    }
    
    
//    @objc override func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
//        if gesture.direction == UISwipeGestureRecognizerDirection.right {
//            print("Swipe Right Pick")
//
////            let rightViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
////            let transition = CATransition()
////            transition.duration = 0.40
////            transition.type = kCATransitionPush
////            transition.subtype = kCATransitionFromLeft
////            navigationController?.view.layer.add(transition, forKey: kCATransition)
////            navigationController?.pushViewController(rightViewController, animated: false)
//
//
//        }
//        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
//            print("Swipe Left Orange")
//
//
//
//        }
//        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
//            print("Swipe Up")
//
//
//
//        }
//        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
//            print("Swipe Down")
//        }
//    }
    
    // MARK: - Action Methods
    
    @IBAction func CaptureSelected(_ sender: AnyObject) {
        
            takePhoto()
        
        
//        AudioServicesPlaySystemSound(1108)
//        if let videoConnection = stillImageOutput!.connection(with: AVMediaType.video) {
//            videoConnection.videoOrientation = AVCaptureVideoOrientation.portrait
//            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: {(sampleBuffer, error) in
//                if (sampleBuffer != nil) {
//                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
//                    let dataProvider = CGDataProvider(data: imageData! as CFData)
//                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
//                    self.capturedImage = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
//
//
//                    let filtersViewController = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
//                    filtersViewController.imageUrl = self.imageUrl
//                    filtersViewController.selectedImage = self.capturedImage
//                    filtersViewController.selectedImageData = imageData!
//                    self.navigationController?.pushViewController(filtersViewController, animated: false)
//
//
////                    let triggerTime = (Int64(NSEC_PER_SEC) * 1)
////                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
////                        //self.loadImageForSellView()
////                    })
//                }
//            })
//        }
    }
    
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        
        
        let imageData = UIImageJPEGRepresentation(photo, 0.7)
        self.capturedImage = photo
        
        let filtersViewController = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        filtersViewController.imageUrl = self.imageUrl
        filtersViewController.selectedImage = self.capturedImage
        filtersViewController.selectedImageData = imageData!
        self.navigationController?.pushViewController(filtersViewController, animated: false)
        
        
    
    }
    
    
    @IBAction func FlashSelected(_ sender: AnyObject) {
        
        
        
        // let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    
                    let offimage = UIImage(named: "flashon")
                    self.flashButton.setImage(offimage, for: UIControlState.normal)
                    device?.torchMode = AVCaptureDevice.TorchMode.off

                } else {
                    do {
                        let onImage = UIImage(named: "flashoff")
                        self.flashButton.setImage(onImage, for: UIControlState.normal)
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        //Utility.showAlertView(String(describing: error), onView: self)
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                //Utility.showAlertView(String(describing: error), onView: self)
                print(error)
            }
        }
    }
    
    @IBAction func CameraFBSelected(_ sender: AnyObject) {
        
        
        
        switchCamera()
        
        
//        if let session = captureSession {
//            //Indicate that some changes will be made to the session
//            session.beginConfiguration()
//
//            //Remove existing input
//            let currentCameraInput:AVCaptureInput = session.inputs.first as! AVCaptureInput
//            session.removeInput(currentCameraInput)
//
//            //Get new input
//            var newCamera:AVCaptureDevice! = nil
//            if let input = currentCameraInput as? AVCaptureDeviceInput {
//                if (input.device.position == .back)
//                {
//                    newCamera = cameraWithPosition(.front)
//                }
//                else
//                {
//                    newCamera = cameraWithPosition(.back)
//                }
//            }
//
//            //Add input to session
//            var err: NSError?
//            var newVideoInput: AVCaptureDeviceInput!
//            do {
//                newVideoInput = try AVCaptureDeviceInput(device: newCamera)
//            } catch let err1 as NSError {
//                err = err1
//                newVideoInput = nil
//            }
//
//            if(newVideoInput == nil || err != nil)
//            {
//                //print("Error creating capture device input: \(err!.localizedDescription)")
//            }
//            else
//            {
//                session.addInput(newVideoInput)
//            }
//
//            //Commit all the configuration changes at once
//            session.commitConfiguration()
//        }
    }
    
    // MARK: - Camera Methods
    
    func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice?
    {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
        for device in devices {
            let device = device as! AVCaptureDevice
            if device.position == position {
                return device
            }
        }
        
        return nil
    }
    func secondsToHoursMinutesSeconds (seconds : Double) -> (Double, Double, Double) {
        let (hr,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        var Minutes = Int(min)
        var Seconds = Int(60 * secf)
        self.videoTimer.text = "\(Minutes):\(Seconds)"
        return (hr, min, 60 * secf)
    }
    
    
}

