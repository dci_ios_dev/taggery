//
//  MapViewController.swift
//  Taggery
//
//  Created by Aravind on 27/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController,UIGestureRecognizerDelegate,MKMapViewDelegate {

    @IBOutlet weak var mapBaseViewOutlet: UIView!
    @IBOutlet weak var mapViewOutlet: MKMapView!
    
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        mapViewOutlet?.showsUserLocation = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector(("handleTap:")))
        gestureRecognizer.delegate = self
        mapViewOutlet.addGestureRecognizer(gestureRecognizer)
        requestLocationAccess()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(triggerTouchAction(_:)))
        mapViewOutlet.addGestureRecognizer(gesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   @objc func triggerTouchAction(_ gestureReconizer: UITapGestureRecognizer) {
        //Add alert to show it works
            let location = gestureReconizer.location(in: mapViewOutlet)
            let coordinate = mapViewOutlet.convert(location,toCoordinateFrom: mapViewOutlet)
    
    let allAnnotation = self.mapViewOutlet.annotations
    
    self.mapViewOutlet.removeAnnotations(allAnnotation)

    let annotation = MKPointAnnotation()
    annotation.coordinate = coordinate
    
    self.getAddressFromLatLon(pdblLatitude: "\(coordinate.latitude)", withLongitude: "\(coordinate.longitude)")
    
    let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: String(annotation.hash))
    
    let rightButton = UIButton(type: .contactAdd)
    rightButton.tag = annotation.hash
    
    pinView.animatesDrop = true
    pinView.canShowCallout = true
    pinView.rightCalloutAccessoryView = rightButton
    self.mapViewOutlet.addAnnotation(pinView.annotation!)
    
    }
    
    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            print("location access denied")
            
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print("\(pm.country!)")
                    print("\(pm.locality!)")
                    print("\(pm.subLocality!)")
                    print("\(pm.thoroughfare!)")
                    print("\(pm.postalCode!)")
                    print("\(pm.subThoroughfare!)")
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                }
        })
        
    }
}
