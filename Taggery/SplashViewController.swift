//
//  SplashViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 11/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import CoreLocation

import SwiftyJSON

class SplashViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var spashImageViewOutlet: UIImageView!
    
    let locationManager = CLLocationManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.determineMyCurrentLocation()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//
//
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//
////            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
////            self.navigationController?.pushViewController(vc, animated: true)
//        }

       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         WebServiceHandler.sharedInstance.getCountry(viewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func determineMyCurrentLocation() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            //locationManager.startUpdatingHeading()
        }
        else{
        
            Utilities.showAlertView("Location not enabled", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
                
        SharedVariables.sharedInstance.currentLatitude = "\(userLocation.coordinate.latitude)"
        SharedVariables.sharedInstance.currentLongitude = "\(userLocation.coordinate.longitude)"

        Constants.UserLatitude = userLocation.coordinate.latitude
        Constants.UserLongitude = userLocation.coordinate.longitude

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    func didReceiveCountryList(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            SharedVariables.sharedInstance.countryListArray = responseDict["country"] as! [[String : Any]] as NSArray
            
            let isLogin:Bool = UserDefaults.standard.bool(forKey: "isLogin")
            if isLogin
            {
      
                
                let userdetails = Utilities.getJSON("userDetail")
               
                if(userdetails != nil){
                    Constants.userId = UserDefaults.standard.string(forKey: "userId")!
                   
                     //UserDefaults.standard.set("\(userDict["id"])", forKey: "userId")
                   
                    if(Constants.fromFCm == true){
                        
                        //Constants.fromFCm = false
                        if Constants.fcmKey == 1{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAnalyticsViewController") as! MyAnalyticsViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    
                    
                }else{
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
//                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
            
            
        }
    }
    // gET JSON
    func getJSON(_ key: String)->JSON {
        var p = ""
        if let buildNumber = UserDefaults.standard.value(forKey: key) as? String {
            p = buildNumber
        }else {
            p = ""
        }
        if  p != "" && p != "null"{
            if(p.count > 0){
                if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                    return try! JSON(data: json)
                } else {
                    return JSON("nil")
                }
            }
            else {
                return JSON("nil")
            }

            
        } else {
            return JSON("nil")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

