//
//  SelectCountryViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 12/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class SelectCountryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var searchBarOutlet: UISearchBar!
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    var searchActive : Bool = false
    var data = [String]()
    var filtered:[[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for  dict in SharedVariables.sharedInstance.countryListArray {
//            data.append(dict["nicename"] as! String)
//        }
        
        /* Setup delegates */
        tableViewOutlet.delegate = self
        tableViewOutlet.dataSource = self
        searchBarOutlet.delegate = self
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//        var firstname = String()
//
//        let results = SharedVariables.sharedInstance.countryListArray.filter({ person in
//
//            if  firstname = person["firstname"] as! String,  let query = searchBar.text {
//                return firstname.rangeOfString(query, options: [.CaseInsensitiveSearch, .DiacriticInsensitiveSearch]) != nil
//            }
//            return false
//        })
//        filtered = NSMutableArray(array: results) as! [[String : Any]]
        
//        filtered = searchText.isEmpty ? data : data.filter({(dataString: String) -> Bool in
//            // If dataItem matches the searchText, return true to include it
//            return dataString.range(of: searchText, options: .caseInsensitive) != nil
//        })
        
        let searchPredicate = NSPredicate(format: "nicename CONTAINS[C] %@", searchText)
        filtered = (SharedVariables.sharedInstance.countryListArray as NSArray).filtered(using: searchPredicate) as! [[String : Any]]
        
        print ("array = \(filtered)")
        
//        if(filtered.count == 0){
//            searchActive = false;
//        } else {
//            searchActive = true;
//        }
        searchActive = !filtered.isEmpty
        self.tableViewOutlet.reloadData()
    }
    
    func filter(array : [[String : String]], byString filterString : String) -> [[String : String]] {
        return array.filter{
            dictionary in
            return dictionary["name"] == filterString
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return SharedVariables.sharedInstance.countryListArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableViewOutlet.dequeueReusableCell(withIdentifier: "Cell");
        
        
        let namelabel:UILabel = cell!.viewWithTag(1) as! UILabel
        let imageView:UIImageView = cell!.viewWithTag(2) as! UIImageView
        let phonrCodelabel:UILabel = cell!.viewWithTag(3) as! UILabel
        var countryDict: [String:Any]
    
        if(searchActive){
            guard filtered.count > 0 else {
                return cell!
            }
            countryDict = filtered[indexPath.row]
        } else {
             countryDict = SharedVariables.sharedInstance.countryListArray[indexPath.row] as! Dictionary
        }
            namelabel.text = countryDict["nicename"] as? String
            phonrCodelabel.text = "+\(String(describing: countryDict["phonecode"]!))"
            let url = "\(String(describing: countryDict["flag"]!))"
            
            imageView.layer.cornerRadius = imageView.frame.size.height / 2;
            imageView.clipsToBounds = true;
            
            imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeHolder.jpg"))
        
        
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(searchActive){
            if(filtered.count > 0){
                SharedVariables.sharedInstance.selectedCountryDict = filtered[indexPath.row];
                print(SharedVariables.sharedInstance.selectedCountryDict)
                self.dismiss(animated: true, completion: {
                    self.searchActive = false
                })
            } else {
                SharedVariables.sharedInstance.selectedCountryDict = SharedVariables.sharedInstance.countryListArray[indexPath.row] as! [String : Any]
                print(SharedVariables.sharedInstance.selectedCountryDict)
                self.dismiss(animated: true, completion: {
                    self.searchActive = false
                })
            }
           
        } else {
            SharedVariables.sharedInstance.selectedCountryDict = SharedVariables.sharedInstance.countryListArray[indexPath.row] as! [String : Any]
            print(SharedVariables.sharedInstance.selectedCountryDict)
            self.dismiss(animated: true, completion: {
                self.searchActive = false
            })
        }
        
        
    }
    
     func tableView(_ tableView: UITableView,
                    heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 60
    }
}
