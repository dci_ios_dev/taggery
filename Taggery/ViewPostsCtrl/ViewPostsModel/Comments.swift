/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Comments : Codable {
	let commentID : Int?
	let commentUserID : Int?
	let commentText : String?
	let commentUserName : String?
	let commentUserFirstName : String?
	let commentLastName : String?
	let commentPicture : String?
	let userTotalLikesCount : Int?
	let commentMentionUserID : Int?
	let commentMentionUserName : String?
	let commentCreatedAt : String?

	enum CodingKeys: String, CodingKey {

		case commentID = "CommentID"
		case commentUserID = "CommentUserID"
		case commentText = "CommentText"
		case commentUserName = "CommentUserName"
		case commentUserFirstName = "CommentUserFirstName"
		case commentLastName = "CommentLastName"
		case commentPicture = "CommentPicture"
		case userTotalLikesCount = "UserTotalLikesCount"
		case commentMentionUserID = "CommentMentionUserID"
		case commentMentionUserName = "CommentMentionUserName"
		case commentCreatedAt = "CommentCreatedAt"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        commentID = try values.decodeIfPresent(Int.self, forKey: .commentID)
        commentUserID = try values.decodeIfPresent(Int.self, forKey: .commentUserID)
        commentText = try values.decodeIfPresent(String.self, forKey: .commentText)
        commentUserName = try values.decodeIfPresent(String.self, forKey: .commentUserName)
        commentUserFirstName = try values.decodeIfPresent(String.self, forKey: .commentUserFirstName)
        commentLastName = try values.decodeIfPresent(String.self, forKey: .commentLastName)
        commentPicture = try values.decodeIfPresent(String.self, forKey: .commentPicture)
        userTotalLikesCount = try values.decodeIfPresent(Int.self, forKey: .userTotalLikesCount)
        commentMentionUserID = try values.decodeIfPresent(Int.self, forKey: .commentMentionUserID)
        commentMentionUserName = try values.decodeIfPresent(String.self, forKey: .commentMentionUserName)
		commentCreatedAt = try values.decodeIfPresent(String.self, forKey: .commentCreatedAt)
	}

}
