//
//  ViewPostViewController.swift
//  Taggery
//
//  Created by Gowsika on 19/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import ParallaxHeader
import SwiftyJSON
import Kingfisher
import AVKit
import FontAwesome_swift
class viewedPostclass:UITableViewCell{
    
    @IBOutlet weak var likeButton: UIImageView!
    @IBOutlet weak var userAvatar: UIImageView!{
        didSet{
            userAvatar.layer.masksToBounds = false
            userAvatar.layer.borderColor = UIColor.black.cgColor
            userAvatar.layer.cornerRadius = userAvatar.frame.height/2
            userAvatar.clipsToBounds = true
        }
    }
    @IBOutlet weak var userName: UILabel!
    
}

class ViewPostViewController: UIViewController {

    @IBOutlet weak var viewsCountLabel: UILabel!
    
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet var mainHeader: UIView!
    @IBOutlet weak var viewForPreview: UIView!
    @IBOutlet weak var postDescription: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var mainHeaderImage: UIImageView!
    var viewedPosts = [Viewed]()
    var postsDatas : Post!
    var postId = 0
    
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.setBackgroungImage(self.view)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        var customViews = mainHeader!
        customViews.frame = CGRect(x:0,y:0,width:mainHeader.frame.width,height:self.view.frame.height / 3)
        tableView.parallaxHeader.view = customViews
        tableView.parallaxHeader.height = self.view.frame.height / 3
        tableView.parallaxHeader.minimumHeight = 0
        tableView.parallaxHeader.mode = .top
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        self.getDataFromApi(postId: postId)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        self.tableView.addGestureRecognizer(swipeRight)
        
    }
    
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            self.dismiss(animated: true) {
                
            }
            //self.navigationController?.popViewController(animated: true)
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            self.navigationController?.popViewController(animated: true)
          
        }
    }
    
    func getDataFromApi(postId:Int){
        
        let params = ["user_id":"\(Constants.userId)","PostID":"\(postId)"]
        let url = "\(Constants.k_Webservice_URL)/getpostdetails"
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Json4Swift_Base.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                    self.postsDatas = datas.post
                    self.viewedPosts = (datas.post?.viewed)!
                    self.updateUi()
                }
                
                //  self.suggestionsarray.removeAll()

              //  self.locationsArray = datas.location!
               // self.postsArray = datas.posts!
            
                DispatchQueue.main.async {
                    Utilities.hideLoading()
               //     self.tabelViewPosts.reloadData()
                    
                }
                
            }
            catch{
                print("ERROR \(error)")
            }
        }
    }

    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func updateUi(){
        self.postDescription.text = self.postsDatas.postText!
        if(self.postsDatas.postMediaType! == 1){
          let imageUrl = "\(Constants.taggeryPostUrl)\(self.postsDatas.postImage!)"
            mainHeaderImage.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
        }
        else
        {
            self.mainHeaderImage.isHidden = true
            //let imageUrl = "\(Constants.taggeryPostUrl)\(self.postsDatas.postImage!)"
            self.mainHeaderImage.isHidden = true
            //let URLS = URL(string: "https://res.cloudinary.com/cloudtaggery18/video/upload/v1545373610/Add_Post/VID_1545373656891.mp4")
            
            let videoUrl = URL(string:"\(Constants.taggeryVideoUrl)\(self.postsDatas.postImage!)")
            player = AVPlayer(url: videoUrl!)
            playerController = AVPlayerViewController()
            guard player != nil && playerController != nil else {
                return
            }
            playerController!.showsPlaybackControls = true
            playerController!.player = player!
            playerController!.view.frame = viewForPreview.bounds
            self.addChildViewController(playerController!)
            viewForPreview.addSubview(playerController!.view)
            playerController?.didMove(toParentViewController: self)
        }
        
        let viewsCount = self.postsDatas.viewscount!
        let likesCount = self.postsDatas.likestcount!
        let likesAttributedString = NSMutableAttributedString()
        var viewsAttributedString = NSMutableAttributedString()
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.red,.font : UIFont(name:"FontAwesome5FreeSolid", size:14)]
        let myAttributewhite = [ NSAttributedStringKey.foregroundColor: UIColor.white]
        
        var likesString = NSAttributedString(string: "\u{f004}", attributes: myAttribute)
        var likesStringWhite = NSAttributedString(string: " \(likesCount)", attributes: myAttributewhite)
        likesAttributedString.append(likesString)
        likesAttributedString.append(likesStringWhite)
       
        let myAttributeViews = [ NSAttributedStringKey.foregroundColor: UIColor.white,.font : UIFont(name:"FontAwesome5FreeSolid", size:14)]
        let myAttributeViewswhite = [ NSAttributedStringKey.foregroundColor: UIColor.white]
        
        var myAttributeViewsString = NSAttributedString(string: "\u{f06e}", attributes: myAttributeViews)
        var myAttributeViewsStringWhite = NSAttributedString(string: " \(likesCount)", attributes: myAttributeViewswhite)
        viewsAttributedString.append(myAttributeViewsString)
        viewsAttributedString.append(myAttributeViewsStringWhite)
    
        self.viewLabel.attributedText = viewsAttributedString
        self.likeLabel.attributedText = likesAttributedString
        self.tableView.reloadData()
        
    }
    
    
}
extension ViewPostViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewedPosts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "viewedPostclass", for: indexPath) as! viewedPostclass
        cell.userName.text = "\(self.viewedPosts[indexPath.row].userFirstName!)"
        let profileUrl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(self.viewedPosts[indexPath.row].userPicture!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.userAvatar.frame.width, height: cell.userAvatar.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.userAvatar.frame.height / 2)
        cell.userAvatar.kf.setImage(with: URL(string: profileUrl), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        if(self.viewedPosts[indexPath.row].likes == 1){
            cell.likeButton.image = UIImage(named: "heartred")
        }else{
            cell.likeButton.isHidden = true
        }
        return  cell
        }
}
