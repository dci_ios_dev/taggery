//
//  ContentViewController.swift
//  Taggery
//
//  Created by Gowsika on 19/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FontAwesome_swift
import SwiftyJSON

import GooglePlaces
import GooglePlacePicker
var ContentViewControllerVC = ContentViewController()

protocol firstresponds:class {
    func responds(status:Bool)
}

class ContentViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate,UITextViewDelegate,KeyboardTextFieldDelegate,updatetext,customInputViewDelegate {
    var pageViewController : UIPageViewController?
    var pages  = [UserDetailsStory]()
    var currentIndex : Int = 0
    var keyboardTextField : KeyboardTextField!

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bottomViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var commentViewConstant: NSLayoutConstraint!
    var protcols : firstresponds?
    @IBOutlet weak var testView: UIView!
    
    @IBOutlet weak var postDescLabel: UILabel!
    @IBOutlet weak var postStatusLabel: UILabel!
    var selectedlocation = "Select Location"

    var likesCount = 0
    var likedStatus = 2
    
    var placePicker = GMSPlacePickerViewController()

    @IBOutlet weak var commentView: UITextView!{
        didSet{
            commentView.layer.cornerRadius = 10.0
            commentView.layer.borderColor = UIColor.white.cgColor
            commentView.layer.borderWidth = 2.0
        }
    }
    
    var postDetails = [RelatedPostsID]()
    
    var currentPostid = 0
    
    var arrayposition = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
             ContentViewControllerVC = self
        
      

        commentView.delegate = self
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "PageViewController") as? UIPageViewController
        pageViewController!.dataSource = self
        pageViewController!.delegate = self
        locationClicked.isUserInteractionEnabled = true
        locationClicked.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.locationTapped)))
        let startingViewController: PreViewViewController = viewControllerAtIndex(index: currentIndex)!
        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: false, completion: nil)
        pageViewController!.view.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: self.mainView.frame.height)
        
        addChildViewController(pageViewController!)
        self.mainView.addSubview(pageViewController!.view)
        
         self.configuregoogle()
        
        //self.mainView.addSubview(testView)
       
       // self.view.addSubview(keyboardTextField)
        
//        let keyboardInputView = customKeyboardView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 40, width: UIScreen.main.bounds.width, height: 40))
//        self.mainView.addSubview(keyboardInputView)
        self.mainView.sendSubview(toBack: pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(postTextUpdate(notification:)), name: NSNotification.Name("updatePostText"), object: nil)
        
        self.commentView.translatesAutoresizingMaskIntoConstraints = false
      
      
//        keyboardInputView.delegate = self

        
        let kScreenWidth = UIScreen.main.bounds.size.width
        let toolBar: UIToolbar = {
            let tmpToolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 70))
            tmpToolBar.backgroundColor = UIColor.gray
            return tmpToolBar
        }()
        
       let textView = UITextView(frame: CGRect(x: 0, y: 5, width: kScreenWidth/4*3, height: 60))
        textView.backgroundColor = UIColor.lightGray
        textView.delegate = self
        textView.tag = 2
        let inputItem = UIBarButtonItem(customView: textView)
        let sendBtn = UIButton(frame: CGRect(x: 0, y: 5, width: kScreenWidth/4-40, height: 50))
        sendBtn.contentMode = .center
        sendBtn.setTitle("", for: .normal)
        sendBtn.setTitleColor(UIColor.blue, for: .normal)
        sendBtn.addTarget(self, action: #selector(handleSend(event:)), for: .touchUpInside)
        let sendItem = UIBarButtonItem(customView: sendBtn)
        toolBar.items = [inputItem,sendItem]
        
        self.postStatusLabel.isUserInteractionEnabled = true
        self.postStatusLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.addLike)))
       
        
    }
    @IBOutlet weak var locationClicked: UIImageView!
    
    
    func configuregoogle()
    {
        let center = CLLocationCoordinate2D(latitude:Constants.UserLatitude, longitude: Constants.UserLongitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        var config = GMSPlacePickerConfig(viewport: viewport)
        placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
    }
    
    
    
    @objc func locationTapped(){
        
        //let mapviewController = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        let paramsNew = ["textFieldFocussed":true]
        NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
        present(placePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func upButtonPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewPostViewController") as! ViewPostViewController
        vc.postId = self.currentPostid
        self.present(vc, animated: true){}
        
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        guard let text = self.commentView.text,text.count > 0 else{return}
        
        let params = ["CommentUserID":"\(Constants.userId)","CommentPostID":"\(self.currentPostid)","CommentText":"\(text)","CommentFor":""] as [String:String]
        
        let url = "\(Constants.k_Webservice_URL)/addcomments"
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            Utilities.hideLoading()
            self.commentView.text = ""
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                // Utilities.showAlertView("\(Message)", onView: self)
            }
            if(StatusCode == 500)
            {
                 Utilities.showAlertView("\(Message)", onView: self)
            }
            print("HANDLER \(JSON)")
            
        }
        
    }
    
    
    
    
    func sendButtonPressedWith(_ str: String) {
        print("CAlled")
    }
    
    @objc func handleSend(event: UIButton) -> () {
        
        view.endEditing(true)
    }

    
    
    @objc func postTextUpdate(notification:NSNotification){
    
    let arrayposition = notification.userInfo?["postPosition"] as! Int
        
        self.arrayposition = arrayposition
        self.currentPostid = postDetails[arrayposition].postID!
        self.postDescLabel.text = postDetails[arrayposition].postText
        
        
        let viewsCount = postDetails[arrayposition].viewscount
        let likesCount = postDetails[arrayposition].likestcount
        let likesStat = postDetails[arrayposition].likes
       self.likesCount = postDetails[arrayposition].likestcount!
        
        if(likesStat == 1){
            likedStatus = 2
        }
        else
        {
            likedStatus = 1
        }
        
        
        
      
        
        let postTime = postDetails[arrayposition].feedtime
        var likesString = NSAttributedString()
        let likesAttributedString = NSMutableAttributedString()
        var seenString = NSAttributedString()
        var seenAttributedString = NSMutableAttributedString()
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white,.font : UIFont(name:"FontAwesome5FreeSolid", size:14)]
            likesString = NSAttributedString(string: "\u{f004} \(likesCount!)", attributes: myAttribute)
            likesAttributedString.append(likesString)
        
            seenString = NSAttributedString(string: "\u{f06e} \(viewsCount!)", attributes: myAttribute)
        let emptyString = NSAttributedString(string: " ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
            likesAttributedString.append(emptyString)
            likesAttributedString.append(seenString)
             self.postStatusLabel.attributedText = likesAttributedString
        
       //  self.postStatusLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.addLike)))
    }
    
    
    @objc func addLike(){
        let paramsNew = ["textFieldFocussed":true]
        NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
        let params = ["LikeUserID":"\(Constants.userId)","LikePostID":"\(self.currentPostid)","LikeStatus":"\(likedStatus)"]
        let url = "\(Constants.k_Webservice_URL)/addlike"
        // Utilities.showLoading()
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            
            //   Utilities.hideLoading()
            
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                self.currentPostid = self.postDetails[self.arrayposition].postID!
                self.postDescLabel.text = self.postDetails[self.arrayposition].postText
                let viewsCount = self.postDetails[self.arrayposition].viewscount
                let likesStat = self.postDetails[self.arrayposition].likes
                
                if(self.likedStatus == 1){
                    if(self.likesCount != 0){
                        self.likesCount = self.likesCount - 1
                    }
                    
                    self.likedStatus = 2
                }
                else
                {
                    self.likesCount = self.likesCount + 1
                    self.likedStatus = 1
                }
                var likesCount = self.likesCount

                let postTime = self.postDetails[self.arrayposition].feedtime
                var likesString = NSAttributedString()
                let likesAttributedString = NSMutableAttributedString()
                var seenString = NSAttributedString()
                var seenAttributedString = NSMutableAttributedString()
                let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white,.font : UIFont(name:"FontAwesome5FreeSolid", size:14)]
                
                if(self.likedStatus == 1){
                     let myAttributes12 = [ NSAttributedStringKey.foregroundColor: UIColor.red,.font : UIFont(name:"FontAwesome5FreeSolid", size:14)]
                    likesString = NSAttributedString(string: "\u{f004} \(likesCount)", attributes: myAttributes12)
                    likesAttributedString.append(likesString)
                }
                else{
                    likesString = NSAttributedString(string: "\u{f004} \(likesCount)", attributes: myAttribute)
                    likesAttributedString.append(likesString)
                }
               
                
                seenString = NSAttributedString(string: "\u{f06e} \(viewsCount!)", attributes: myAttribute)
                let emptyString = NSAttributedString(string: " ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                likesAttributedString.append(emptyString)
                likesAttributedString.append(seenString)
                self.postStatusLabel.attributedText = likesAttributedString
            }
            if(StatusCode == 500)
            {
                
            }
        }
    }
    
    
    
   
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func keyboardTextFieldPressReturnButton(_ keyboardTextField: KeyboardTextField) {
        UIAlertView(title: "", message: "Action", delegate: nil, cancelButtonTitle: "OK").show()

    }
    
    func updateTextLabels(labelValue: String, viewsCOunt: String, likesCount: String) {
        
        self.postDescLabel.text = labelValue
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        let paramsNew = ["textFieldFocussed":true]
        NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
        
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if(textView.becomeFirstResponder()){
            let paramsNew = ["textFieldFocussed":true]
            NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        
        let paramsNew = ["textFieldFocussed":true]
        NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
    
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - UIPageViewControllerDataSource
    //1
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! PreViewViewController).pageIndex
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        index -= 1
        return viewControllerAtIndex(index: index)
    }
    
    //2
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! PreViewViewController).pageIndex
        if index == NSNotFound {
            return nil
        }
        index += 1
        if (index == postDetails.count) {
            return nil
        }
        return viewControllerAtIndex(index: index)
    }
    
    //3
    func viewControllerAtIndex(index: Int) -> PreViewViewController? {
        if postDetails.count == 0 || index >= postDetails.count {
            return nil
        }
        print("Called")
        // Create a new view controller and pass suitable data.
        let vc = storyboard?.instantiateViewController(withIdentifier: "PreViewViewController") as! PreViewViewController
        vc.pageIndex = index
        vc.items = pages
        currentIndex = index
        vc.postDetails = postDetails
        
        vc.view.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        return vc
    }
    
    // Navigate to next page
    func goNextPage(fowardTo position: Int) {
        let startingViewController: PreViewViewController = viewControllerAtIndex(index: position)!
        let viewControllers = [startingViewController]
        pageViewController!.setViewControllers(viewControllers , direction: .forward, animated: true, completion: nil)
    }
    
    // MARK: - Button Actions
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
class DNKeyboardTextField : KeyboardTextField {
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        self.clearTestColor()
        //TextView
        self.textViewBackground.layer.borderColor = UIColor(rgb: (191,191,191)).cgColor
        self.textViewBackground.backgroundColor = UIColor.white
        self.textViewBackground.layer.cornerRadius = 18
        self.textViewBackground.layer.masksToBounds = true
        self.keyboardView.backgroundColor = UIColor(rgb: (238,238,238))
        self.placeholderLabel.textAlignment = .center
        self.placeholderLabel.text = " "
        self.placeholderLabel.textColor = UIColor(rgb: (153,153,153))
        
        self.leftRightDistance = 15.0
        self.middleDistance = 5.0
        self.buttonMinWidth = 60
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

extension ContentViewController:GMSPlacePickerViewControllerDelegate{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        print("Place is \(place)")
        self.selectedlocation = place.name
      //  self.selectedlatitude = "\(place.coordinate.latitude)"
       // self.selectedlongitude = "\(place.coordinate.longitude)"
        
        viewController.dismiss(animated: true) {
           // self.TableViews.reloadData()
            let paramsNew = ["textFieldFocussed":true]
            NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
            
            //let vieController = self.storyboard?.instantiateViewController(withIdentifier: "") as!
            self.commentView.text = "\(self.selectedlocation)"
        }
        
    }
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true) {
            let paramsNew = ["textFieldFocussed":true]
            NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
        }
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        
        viewController.dismiss(animated: true) {
            let paramsNew = ["textFieldFocussed":true]
            NotificationCenter.default.post(name: Notification.Name("textFieldFocussed"), object: nil, userInfo: paramsNew)
        }
    }
    
    
}


extension UIColor {
    
    convenience init(rgb: (r: CGFloat, g: CGFloat, b: CGFloat)) {
        self.init(red: rgb.r/255, green: rgb.g/255, blue: rgb.b/255, alpha: 1)
    }
    convenience init(rgba: (r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat)) {
        self.init(red: rgba.r/255, green: rgba.g/255, blue: rgba.b/255, alpha: rgba.a)
    }
}


