//
//  PreViewViewController.swift
//  Taggery
//
//  Created by Gowsika on 19/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import CoreMedia
import IQKeyboardManagerSwift
import Kingfisher
import SDWebImage

protocol updatetext {
    
    func updateTextLabels(labelValue:String,viewsCOunt:String,likesCount:String)
}

class PreViewViewController: UIViewController,SegmentedProgressBarDelegate,firstresponds{

    
    @IBOutlet weak var flagButon: UIButton!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    var reportstatus = 2
    var pageIndex : Int = 0
    var items: [UserDetailsStory] = []
    var item: [Content] = []
    var SPB: SegmentedProgressBar!
    var player: AVPlayer!
    let loader = ImageLoader()
    
    var postDetails = [RelatedPostsID]()
    var delegates : updatetext?
    var playerItem: AVPlayerItem!
    var postData : Post!

    private var playerItemContext = 0
    let requiredAssetKeys = [
        "playable",
        "hasProtectedContent"
    ]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.height / 2;
        
        print("Page Index \(pageIndex)")
        
         let postImaeUrl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(postDetails[pageIndex].postUserProfileImage!)"
       // userProfileImage.imageFromServerURL(postImaeUrl)
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: userProfileImage.frame.width, height: userProfileImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius: userProfileImage.frame.height / 2)
       userProfileImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        
        hoursLabel.text = postDetails[pageIndex].feedtime!
        lblUserName.text = postDetails[pageIndex].postUserFName
       // item = postDetails[pageIndex].content
        
        //pageIndex = postDetails.count
        
        SPB = SegmentedProgressBar(numberOfSegments: postDetails.count, duration: 5)
        if #available(iOS 11.0, *) {
            SPB.frame = CGRect(x: 18, y: UIApplication.shared.statusBarFrame.height + 5, width: view.frame.width - 35, height: 3)
        } else {
            // Fallback on earlier versions
            SPB.frame = CGRect(x: 18, y: 15, width: view.frame.width - 35, height: 3)
        }
        
        SPB.delegate = self
        SPB.topColor = UIColor.white
        SPB.bottomColor = UIColor.white.withAlphaComponent(0.25)
        SPB.padding = 2
        SPB.isPaused = true
        SPB.currentAnimationIndex = 0
        SPB.duration = getDuration(at: 0)
        view.addSubview(SPB)
        view.bringSubview(toFront: SPB)
        
        let tapGestureImage = UITapGestureRecognizer(target: self, action: #selector(tapOn(_:)))
        
        tapGestureImage.numberOfTapsRequired = 1
        tapGestureImage.numberOfTouchesRequired = 1
        imagePreview.addGestureRecognizer(tapGestureImage)
        
        let tapGestureVideo = UITapGestureRecognizer(target: self, action: #selector(tapOn(_:)))
        tapGestureVideo.numberOfTapsRequired = 1
        tapGestureVideo.numberOfTouchesRequired = 1
        videoView.addGestureRecognizer(tapGestureVideo)
       
        
        NotificationCenter.default.addObserver(self, selector: #selector(postkeyboardUpdate(notification:)), name: NSNotification.Name("textFieldFocussed"), object: nil)
     //   NotificationCenter.default.addObserver(self, selector: #selector(postgoogleUpdate(notification:)), name: NSNotification.Name("textFieldFocussed"), object: nil)
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
    }
    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    @objc func postkeyboardUpdate(notification:NSNotification){
         let arrayposition = notification.userInfo?["textFieldFocussed"] as! Bool
        
        if(arrayposition){
            SPB.isPaused = true
        }
        else
        {
           // SPB.isPaused = false
        }
    }
    @objc func postgoogleUpdate(notification:NSNotification){
        let arrayposition = notification.userInfo?["textFieldFocussed"] as! Bool
        
        if(arrayposition){
            SPB.isPaused = true
        }
        else
        {
            //SPB.isPaused = false
        }
    }
    
    
    func stopApp(){
        SPB.isPaused = true
    }
    func startApp(){
        SPB.isPaused = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.8) {
            self.view.transform = .identity
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            self.SPB.startAnimation()
            self.playVideoOrLoadImage(index: 0)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.main.async {
            self.SPB.currentAnimationIndex = 1
            self.SPB.cancel()
            self.SPB.isPaused = true
            self.resetPlayer()
        }
    }
    
    func responds(status: Bool) {
        if(status){
            self.SPB.isPaused = true
        }
        else
        {
            self.SPB.isPaused = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - SegmentedProgressBarDelegate
    //1
    func segmentedProgressBarChangedIndex(index: Int) {
        
        //self.SPB.isPaused = false
        playVideoOrLoadImage(index: index)
    }
    
    //2
    func segmentedProgressBarFinished() {
        if pageIndex == (self.postDetails.count - 1) {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            self.dismiss(animated: true, completion: nil)
           //  _ = ContentViewControllerVC.goNextPage(fowardTo: pageIndex + 1)
            //_ = ContentViewController.goNextPage(fowardTo: pageIndex + 1)
        }
    }
    
    @objc func tapOn(_ sender: UITapGestureRecognizer) {
        SPB.isPaused = false
        SPB.skip()
    }
    
    //MARK: - Play or show image
    func playVideoOrLoadImage(index: NSInteger) {
        
        print("Index is \(index)")
        
        
       
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.imagePreview.isUserInteractionEnabled = true
        self.imagePreview.addGestureRecognizer(swipeRight)
        
        
        let params  = ["postPosition":index]
        NotificationCenter.default.post(name: Notification.Name("updatePostText"), object: nil, userInfo: params)
        let paramsPost = ["user_id":"\(Constants.userId)","PostID":"\(postDetails[index].postID!)"]
        let url = "\(Constants.k_Webservice_URL)/getpostdetails"
        self.SPB.isPaused = true
         WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: paramsPost) { Data in
            
           
            
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Json4Swift_Base.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                    self.postData = datas.post!
                    if(datas.post!.reportStatus! == 2){
                          self.reportstatus = 1
                        self.flagButon.setBackgroundImage(UIImage(named: "flagred.png"), for: .normal)
                    }
                    else
                    {
                        self.reportstatus = 2
                        self.flagButon.setBackgroundImage(UIImage(named: "flagwhite.png"), for: .normal)
                    }
                }
            }catch{
                
            }
        }
        
        
        
        
        if postDetails[index].postMediaType! == 1 || postDetails[index].postMediaType! == 0{
           // self.SPB.duration = 5
            self.imagePreview.isHidden = false
            self.videoView.isHidden = true
              let postImaeUrl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(postDetails[index].postImage!)"
            //print("URL is \(postImaeUrl)")
            let imageUrl = URL(string: "\(postImaeUrl)")
           // self.imagePreview.imageFromServerURL(postImaeUrl)
            self.imagePreview.contentMode = .scaleAspectFill
            self.imagePreview.clipsToBounds = true

            
            if let url = URL(string: "\(postImaeUrl)") {
                //self.SPB.isPaused = true
                LoadingIndicatorView.show()
                self.imagePreview.image = UIImage(named: "placeHolder.jpg")
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                 
                    if error != nil {
                        print("ERROR LOADING IMAGES FROM URL: \(String(describing: error))")
                        return
                    }
                    DispatchQueue.main.async {
                        LoadingIndicatorView.hide()
                        self.SPB.isPaused = false
                        self.SPB.duration = 5.0
                        if let data = data {
                            
                            if let downloadedImage = UIImage(data: data) {
                               self.imagePreview.image = downloadedImage
                                //self.image = downloadedImage
                            }
                        }
                    }
                }).resume()
            }
            
            
        } else {
            self.imagePreview.isHidden = true
            self.videoView.isHidden = false
           
            
            resetPlayer()
            
            var postImaeUrl = "\(Constants.taggeryVideoUrl)\(postDetails[index].postImage!)"
            postImaeUrl = postImaeUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
            let url = URL(string: postImaeUrl)
            print("URL is \(url!)")
            let asset = AVAsset(url: url!)
            playerItemContext = 0
            //self.SPB.isPaused = true
            playerItem = AVPlayerItem(asset: asset,
                                      automaticallyLoadedAssetKeys: requiredAssetKeys)
            playerItem.addObserver(self,
                                   forKeyPath: #keyPath(AVPlayerItem.status),
                                   options: [.old, .new],
                                   context: &playerItemContext)
            
            self.player = AVPlayer(playerItem: playerItem)
            
            
            
           // self.player.addObserver(self, forKeyPath: #keyPath(self.player.status), options: [.old,.new], context: playerItemContext)
            
            let videoLayer = AVPlayerLayer(player: self.player)
            videoLayer.frame = view.bounds
            videoLayer.videoGravity = .resizeAspect
            self.videoView.layer.addSublayer(videoLayer)
            
          
            let duration = asset.duration
            let durationTime = CMTimeGetSeconds(duration)
            
            self.SPB.duration = durationTime
            self.player.play()
            
            
        }
    }
    
    // MARK: Private func
    private func getDuration(at index: Int) -> TimeInterval {
        var retVal: TimeInterval = 5.0
        if postDetails[index].postType == "1" {
            retVal = 5.0
        } else {
//            guard let url = NSURL(string: item[index].url) as URL? else { return retVal }
//            let asset = AVAsset(url: url)
           // let duration = asset.duration
            retVal = 5.0
        }
        return retVal
    }
    
    private func resetPlayer() {
        if player != nil {
            player.pause()
            player.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    //MARK: - Button actions
    @IBAction func close(_ sender: Any) {
        self.SPB.isPaused = true
        let params = ["user_id":"\(Constants.userId)","PostID":"\(self.postData.postID!)","ReportStatus":"\(reportstatus)"] as [String:String]
        let url = "\(Constants.k_Webservice_URL)/report"

        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            self.SPB.isPaused = false
            self.SPB.duration = 3.0
            
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            Utilities.showAlertView(Message, onView: self)
            if(StatusCode == 200)
            {
                if(self.reportstatus == 1){
                    self.flagButon.setBackgroundImage(UIImage(named: "flagwhite.png"), for: .normal)
                }else{
                    self.flagButon.setBackgroundImage(UIImage(named: "flagred.png"), for: .normal)
                }
                
                //self.getDatafromApi()
            }
            if(StatusCode == 500)
            {
                
            }
        }
        
//        self.dismiss(animated: true, completion: nil)
//        resetPlayer()
    }
}
