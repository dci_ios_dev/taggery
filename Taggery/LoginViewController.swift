//
//  LoginViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 13/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import CoreTelephony
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var phoneNumberTxtFieldOulet: UITextField!
    @IBOutlet weak var passwordTxtFieldOutlet: UITextField!
    @IBOutlet weak var logInBtnOutlet: UIButton!
    @IBOutlet weak var countryBtnOutlet: UIButton!
    
    var countryCode = String()
    var isEyeClicked: Bool!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryBtnOutlet.setTitle("Country", for: UIControlState.normal)
       // self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        
        logInBtnOutlet.layer.cornerRadius = 5;
        logInBtnOutlet.clipsToBounds = true;
        
        phoneNumberTxtFieldOulet.text = ""
        passwordTxtFieldOutlet.text = ""
        
        phoneNumberTxtFieldOulet.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        passwordTxtFieldOutlet.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
//        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(Locale.current)
//            print(countryCode)
//
//        }
        
        let networkInfo = CTTelephonyNetworkInfo()
        
        if let carrier = networkInfo.subscriberCellularProvider,carrier.isoCountryCode != nil {
//            print("country code is: " + carrier.mobileCountryCode!);
//            //will return the actual country code
            print("ISO country code is: " + carrier.isoCountryCode!);
            let flagDetails = IsoCountryCodes.searchByCountry(name: carrier.isoCountryCode!.uppercased())
        
            let flagString = IsoCountries.flag(countryCode: flagDetails.alpha2 )

            let countryText = "\(flagString) \(String(describing: flagDetails.calling))"
            self.countryBtnOutlet.setTitle(countryText, for: .normal)
            
            var myString = "\(String(describing: flagDetails.calling))"
            countryCode = String(myString.dropFirst())
            
        }
        else
        {
            Utilities.showAlertView("No sim card inserted in your current device", onView: self)
            return
        }


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let countryValuesCount = SharedVariables.sharedInstance.selectedCountryDict.values
        
        if countryValuesCount.count > 0 {
            
            let flagString = IsoCountries.flag(countryCode: SharedVariables.sharedInstance.selectedCountryDict["iso"] as! String)
            
            
            let countryText = "\(flagString) +\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
            
             self.countryBtnOutlet.setTitle(countryText, for: .normal)
            
            countryCode = "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func visibleEyeBtnAction(_ sender: Any) {
        if isEyeClicked == false {
            
            passwordTxtFieldOutlet.isSecureTextEntry = false
            isEyeClicked = true
        }
        else{
            
            passwordTxtFieldOutlet.isSecureTextEntry = true
            isEyeClicked = false
        }
        
        
    }
    func didReceiveUserNameCheck(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 500 {
//            userNameValidImageView.isHidden = false
//            isUserNameValid = true
            print("Entered number \(phoneNumberTxtFieldOulet.text!)")
            
            let phones = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
            
            WebServiceHandler.sharedInstance.forgototp(viewController: self, mobile: phones)
        }else{
            Utilities.showAlertView("Enter Valid Number", onView: self)
//            userNameValidImageView.isHidden = true
//            isUserNameValid = false
        }
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
       // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
//        let phoneNumParam = "\(countryCode)9677877096"
//        WebServiceHandler.sharedInstance.phoneNumberCheck(phoneNumber: phoneNumParam, viewController: self)
        
        
        
        let phonenumber = phoneNumberTxtFieldOulet.text!
        
        if(phonenumber.count < 10)
        {
            
            Utilities.showAlertView("Enter Valid Phone Number", onView: self)

        }
        else{
            WebServiceHandler.sharedInstance.userNameCheck(name: " ", CountryCode: "+\(countryCode)", Phonenumber: phoneNumberTxtFieldOulet.text!, Type: "2", viewController: self)
            
            
//            let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
//            vc.mobileNumber = phoneNumParam
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
       
    }
    
    func makeLogin(password:String,mobileNumber:String,countruyCode:String,fcmToken:String){
      
        if(mobileNumber.count < 9 || mobileNumber.count > 15){
            Utilities.showAlertView("Enter Valid Mobile Number between 9 to 10 digits", onView: self)
            return
        }
        if(password.count == 0){
            Utilities.showAlertView("Password cannot Be empty", onView: self)
            return
        }
        
      let parameters = ["PhoneNumber": mobileNumber, "Password": password,"CountryCode":countruyCode,"FCMKey":fcmToken]
      let url = "\(Constants.k_Webservice_URL)login"
        
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: parameters) { JSON in
            print("\(JSON)")
        let statuscode = JSON["StatusCode"].int
            if(statuscode == 200){
                let userDict = JSON["UserDetails"]
                UserDefaults.standard.set(userDict.rawString(), forKey: "userDetail")
                Constants.userId = "\(userDict["id"])"
                UserDefaults.standard.set(true, forKey: "isLogin")
                UserDefaults.standard.set("\(userDict["id"])", forKey: "userId")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if(statuscode == 500){
                let message = JSON["Message"].string
                Utilities.showAlertView(message!, onView: self)
                
            }
        }

    }
    
    
    func didReceiveforgotResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        let status = responseDict["type"] as! String
        
        if status == "success" {
            // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
            let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
            vc.mobileNumber = phoneNumParam
            vc.countrycode = countryCode
            vc.numberNormal = "\(phoneNumberTxtFieldOulet.text!)"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
        }
    }
    
    
    
    
    @IBAction func countryBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectCountryViewController") as! SelectCountryViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @available(iOS 10.0, *)
    @IBAction func logInBtnAction(_ sender: Any) {
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let phoneNumParam = "\(phoneNumberTxtFieldOulet.text!)"
         let fcmToken = appDelegate.fcmToken
       
        self.makeLogin(password: passwordTxtFieldOutlet.text!, mobileNumber: phoneNumParam, countruyCode: "+\(countryCode)", fcmToken: fcmToken)
        //WebServiceHandler.sharedInstance.logInApiCall(password: passwordTxtFieldOutlet.text!, mobileNumber: phoneNumParam, fcmToken: fcmToken, countryCode: "+\(countryCode)", viewController: self)
        
    }

    @IBAction func signUpBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
       // _ = self.navigationController?.popViewController(animated: true)
    }
    // MARK:- Api Response Methods
    
    func didReceivePhoneNumberCheck(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
       // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
            
            let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
            
        WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: phoneNumParam)
            
        }else{
            
        }
    }
    func didReceiveLogInResponse(responseDict:[String:Any]) {
        
//print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
            
            // we need to Convert data to standard NSObject context
            
            let userDict = responseDict["UserDetails"] as! [String:Any]
            
            let jsondata = JSON(userDict)
            
            print("JSON DATA is \(jsondata)")
            
            UserDefaults.standard.set(jsondata.rawString(), forKey: "userDetail")
            Constants.userId = "\(jsondata["id"])"
            UserDefaults.standard.set(true, forKey: "isLogin")
            
            
           
            
//           let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
//          self.navigationController?.pushViewController(vc, animated: true)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            Utilities.showAlertView("Login Failed", onView: self)
        }
    }
    func didReceiveOtpResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        let status = responseDict["type"] as! String
        
        if status == "success" {
           // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
            let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
            vc.mobileNumber = phoneNumParam
            vc.countrycode = countryCode
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
        }
    }

}
