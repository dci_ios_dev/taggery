//
//  CreatPostViewController.swift
//  Taggery
//
//  Created by Aravind on 27/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

import ParallaxHeader

import SwiftIcons

class locationcells : UICollectionViewCell{
    
    @IBOutlet weak var locationlabel: UILabel!
    
    
}

class CreatPostViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   
    

    @IBOutlet weak var postImageViewOutlet: UIImageView!
    @IBOutlet weak var bottomViewOutlet: UIView!
    @IBOutlet weak var tagedLblOutlet: UILabel!
    @IBOutlet weak var textViewOutlet: UITextView!
    @IBOutlet weak var postBtnOutlet: UIButton!
    
    @IBOutlet weak var tagpeoples: UILabel!
    @IBOutlet weak var AddLocation: UILabel!
    
    @IBOutlet weak var bottomScrollView: UIScrollView!
    @IBOutlet var scrollview: UIView!
    
    @IBOutlet weak var locationcollectionview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  self.locationcollectionview.delegate = self
      //  self.locationcollectionview.dataSource = self
        
       
        Utilities.setBackgroungImage(self.view)
       // self.Tagpeoplelable.setIcon(prefixText:"Tag people",icon: .fontAwesome(.tag), iconSize: 30,color:.white)
       
       // self.tagpeoples.set(image: UIImage(named: "usertag")!, with: "Tag People")
       
     
      //  self.AddLocation.set(image: UIImage(named: "location")!, with: "Add Location")
        
        
    //    let imageView = UIImageView()
     //   imageView.image = UIImage(named: "image1.jpg")
      //  imageView.contentMode = .scaleAspectFill
        
    //    self.scrollview.backgroundColor  = UIColor(patternImage: UIImage(named: "BG.png")!)
    //    self.bottomScrollView.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "locationcells", for: indexPath) as! locationcells
        cells.locationlabel.text = "HEllo"
        
        return cells
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func postBtnAction(_ sender: Any) {
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UILabel {
    func set(image: UIImage, with text: String) {
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: -5.0, width: 25, height: 25)
        let attachmentStr = NSAttributedString(attachment: attachment)
        
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentStr)
        
      let spacsstring = NSAttributedString(string: "   ")
        
        mutableAttributedString.append(spacsstring)
        
        let textString = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 20.0),.foregroundColor:UIColor.white])
        mutableAttributedString.append(textString)
        
        self.attributedText = mutableAttributedString
    }
}
extension UILabel {
    func setBottomBorder(label:UILabel) {
       
        let bottomLayer = CALayer()
        bottomLayer.frame = CGRect(x: 0, y: frame.height - 0.8, width: frame.width , height: 1)
        bottomLayer.backgroundColor = UIColor.black.cgColor
        label.clipsToBounds = true
        label.layer.addSublayer(bottomLayer)
        
        
    }
}
