//
//  Utilities.swift
//  MagmaChamberCalendar
//
//  Created by Balajibabu S.G. on 09/09/17.
//  Copyright © 2017 Balajibabu S.G. All rights reserved.
//

import UIKit

import SwiftyJSON
import ANLoader
import Alamofire
// variables profile 1->requested, 2->following, 3->follow 

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}


struct GoogleMaps{
    
    var latitude : Double
    var longitude : Double
    var placename : String
    
}
struct timeDuration {
    var timename : String
    var duration : String
}

@IBDesignable
class LeftAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        contentHorizontalAlignment = .left
        let availableSpace = UIEdgeInsetsInsetRect(bounds, contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.right - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: availableWidth / 2, bottom: 0, right: 0)
    }
}
struct MessageFIR : Encodable {
    var chatWithID: Int?
    var imageText: Any?
    var imageUrl: Any?
    var name, photoUrl: String?
    var readStatus: Int?
    var receiverFcmToken, receiverPhotoUrl, receiverRoleName, receiverUniqueID: String?
    var roleName: String?
    var senderUserID: Int?
    var sentTime, toName, uniqueID, userFCMKey, text: String?
    
    var dictionaryRepresentation: NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(chatWithID!, forKey: "chatWithID")
        dictionary.setValue(imageText!, forKey: "imageText")
        dictionary.setValue(imageUrl!, forKey: "imageUrl")
        dictionary.setValue(name!, forKey: "name")
        dictionary.setValue(photoUrl!, forKey: "photoUrl")
        dictionary.setValue(readStatus!, forKey: "readStatus")
        dictionary.setValue(receiverFcmToken!, forKey: "receiverFcmToken")
        dictionary.setValue(receiverPhotoUrl!, forKey: "receiverPhotoUrl")
        dictionary.setValue(receiverRoleName!, forKey: "receiverRoleName")
        dictionary.setValue(receiverUniqueID!, forKey: "receiverUniqueID")
        dictionary.setValue(roleName!, forKey: "roleName")
        dictionary.setValue(senderUserID!, forKey: "senderUserID")
        dictionary.setValue(sentTime!, forKey: "sentTime")
        dictionary.setValue(toName!, forKey: "toName")
        dictionary.setValue(uniqueID!, forKey: "uniqueID")
        dictionary.setValue(userFCMKey!, forKey: "userFCMKey")
        dictionary.setValue(text!, forKey: "text")
        return dictionary
        //        return [
        //            "chatWithID" : chatWithID as Any,
        //            "imageText" : imageText as Any,
        //            "imageURL" : imageURL as Any,
        //            "name" : name as Any,
        //            "photoURL" : photoURL as Any,
        //            "readStatus" : readStatus as Any,
        //            "receiverFcmToken" : receiverFcmToken as Any,
        //            "receiverPhotoURL" : receiverPhotoURL as Any,
        //            "receiverRoleName" : receiverRoleName as Any,
        //            "receiverUniqueID" : receiverUniqueID as Any,
        //            "roleName" : roleName as Any,
        //            "senderUserID" : senderUserID as Any,
        //            "sentTime" : sentTime as Any,
        //            "toName" : toName as Any,
        //            "uniqueID" : uniqueID as Any,
        //            "userFCMKey" : userFCMKey as Any,
        //            "text" : text as Any
        //        ]
    }
    
    var receiverDictionaryRepresentation: NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(chatWithID!, forKey: "senderUserID")
        dictionary.setValue(imageText!, forKey: "imageText")
        dictionary.setValue(imageUrl!, forKey: "imageUrl")
        dictionary.setValue(name!, forKey: "toName")
        dictionary.setValue(receiverPhotoUrl!, forKey: "photoUrl")
        dictionary.setValue(readStatus!, forKey: "readStatus")
        dictionary.setValue(receiverFcmToken!, forKey: "receiverFcmToken")
        dictionary.setValue(photoUrl!, forKey: "receiverPhotoUrl")
        dictionary.setValue(receiverRoleName!, forKey: "receiverRoleName")
        dictionary.setValue(receiverUniqueID!, forKey: "receiverUniqueID")
        dictionary.setValue(roleName!, forKey: "roleName")
        dictionary.setValue(senderUserID!, forKey: "chatWithID")
        dictionary.setValue(sentTime!, forKey: "sentTime")
        dictionary.setValue(toName!, forKey: "name")
        dictionary.setValue(uniqueID!, forKey: "uniqueID")
        dictionary.setValue(userFCMKey!, forKey: "userFCMKey")
        dictionary.setValue(text!, forKey: "text")
        return dictionary
        //        return [
        //            "chatWithID" : chatWithID as Any,
        //            "imageText" : imageText as Any,
        //            "imageURL" : imageURL as Any,
        //            "name" : name as Any,
        //            "photoURL" : photoURL as Any,
        //            "readStatus" : readStatus as Any,
        //            "receiverFcmToken" : receiverFcmToken as Any,
        //            "receiverPhotoURL" : receiverPhotoURL as Any,
        //            "receiverRoleName" : receiverRoleName as Any,
        //            "receiverUniqueID" : receiverUniqueID as Any,
        //            "roleName" : roleName as Any,
        //            "senderUserID" : senderUserID as Any,
        //            "sentTime" : sentTime as Any,
        //            "toName" : toName as Any,
        //            "uniqueID" : uniqueID as Any,
        //            "userFCMKey" : userFCMKey as Any,
        //            "text" : text as Any
        //        ]
    }
    
    
    init(chatWithID: Int, imageText: Any, imageURL: Any, name: String, photoURL: String, readStatus: Int, receiverFcmToken: String, receiverPhotoURL: String, receiverRoleName: String, receiverUniqueID: String, roleName: String, senderUserID: Int, sentTime: String, toName: String, uniqueID: String, userFCMKey: String, text: String) {
        self.chatWithID = chatWithID
        self.imageText = imageText
        self.imageUrl = imageURL
        self.name = name
        self.photoUrl = photoURL
        self.readStatus = readStatus
        self.receiverFcmToken = receiverFcmToken
        self.receiverPhotoUrl = receiverPhotoURL
        self.receiverRoleName = receiverRoleName
        self.receiverUniqueID = receiverUniqueID
        self.roleName = roleName
        self.senderUserID = senderUserID
        self.sentTime = sentTime
        self.toName = toName
        self.uniqueID = uniqueID
        self.userFCMKey = userFCMKey
        self.text = text
    }
    func encode(to encoder: Encoder) throws {
        
    }
}
struct Registration: Codable {
    let firstName, lastName, userName, phoneNumber: String
    let gender, relationShip, password, fcmKey: String
    let deviceType, emailID, countryCode, profilePic: String
    
    enum CodingKeys: String, CodingKey {
        case firstName, lastName, userName, phoneNumber, gender, relationShip, password, fcmKey, deviceType
        case emailID = "emailId"
        case countryCode, profilePic
    }
    
    init(firstName: String, lastName: String, userName: String, phoneNumber: String, gender: String, relationShip: String, password: String, fcmKey: String, deviceType: String, emailID: String, countryCode: String, profilePic: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.phoneNumber = phoneNumber
        self.gender = gender
        self.relationShip = relationShip
        self.password = password
        self.fcmKey = fcmKey
        self.deviceType = deviceType
        self.emailID = emailID
        self.countryCode = countryCode
        self.profilePic = profilePic
    }
}


class UILabelPadding: UILabel {
    
    let Pad = UIEdgeInsets(top: 2, left: 8, bottom: 2, right: 8)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, Pad))
    }
    
    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + Pad.left + Pad.right
        let heigth = superContentSize.height + Pad.top + Pad.bottom
        return CGSize(width: width, height: heigth)
    }
}

class Utilities: NSObject {
    
    static var memberColorArray = [UIColor.red, UIColor.green, UIColor.blue, UIColor.brown, UIColor.darkGray, UIColor.magenta, UIColor.orange, UIColor.purple, UIColor.cyan]
    
    
    class func showLoading(){
        ANLoader.pulseAnimation = true //It will animate your Loading
        ANLoader.activityColor = .white
        ANLoader.activityBackgroundColor = .clear
        ANLoader.activityTextColor = .white
        
        ANLoader.showLoading("Loading", disableUI: true)
        
    }
    class func hideLoading(){
        
        ANLoader.hide()
    }
    
    class func currentDateTime()->String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let timeintreval = "\(date.timeIntervalSince1970)"
        return timeintreval
    }
    
   class func getJSON(_ key: String)->JSON {
        var p = ""
        if let buildNumber = UserDefaults.standard.value(forKey: key) as? String {
            p = buildNumber
        }else {
            p = ""
        }
        if  p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                return try! JSON(data: json)
            } else {
                return JSON("nil")
            }
        } else {
            return JSON("nil")
        }
    }
    
    class  func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        print("TEST EMAIl \(testStr)")
        return emailTest.evaluate(with: testStr)
    }
    
    class func attributedPlaceholder(text:String) -> NSAttributedString{
        
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.lightGray,
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14.0)
        ]
       return NSAttributedString(string: "\(text)", attributes:attributes)

    }
    
    class func postattributedPlaceholder(text:String) -> NSAttributedString{
        
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14.0)
        ]
        return NSAttributedString(string: "\(text)", attributes:attributes)
        
    }
    
    class func showAlertView(_ message: String, onView view: UIViewController)  {
        
        let alert = UIAlertController(title: Constants.k_ApplicationName, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        view.present(alert, animated: true) {
            
        }
        
        //UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    class func retunUrlfromString(){
        
    }
   class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    
    class func setBackgroungImage(_ view: UIView) {
        
        let imageView = UIImageView(image: UIImage(named: "BG.png")?.withRenderingMode(.alwaysOriginal))
        imageView.contentMode = .scaleAspectFill
        imageView.tag = 1000
        imageView.frame = view.bounds
        view.insertSubview(imageView, at: 0)
    }
    
    class func firstAndLastNameVadidation(_ Input:String, CharactersInRange range: NSRange, textField: UITextField) -> Bool {
        if (range.location == 0){
            let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~\\/:;?-=\'`£•¢")
            
            if Input.rangeOfCharacter(from: validString) != nil
            {
                // print(range)
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + Input.count - range.length
            return newLength <= 16
        }
        
    }
    class func userNameVadidation(_ Input:String, CharactersInRange range: NSRange, textField: UITextField) -> Bool {
        
            guard let text = textField.text else { return true }
            
            let newLength = text.count + Input.count - range.length
            return newLength <= 16
        
        
    }
    
    class func phoneNumberValidation(_ Input:String, CharactersInRange range: NSRange, textField: UITextField) -> Bool {
        
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + Input.characters.count - range.length
        return newLength <= 10
    }
    
    class func mandatoryText(stringToAdd:String) -> NSAttributedString {
       
        let text = stringToAdd + " *"
        let range = (text as NSString).range(of: "*")
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range)
        return attributedString
    
    }
    
    class func border(for textField: UITextField) {
        let borderColorCodeForTF: CGFloat = 235.0 / 255.0
        textField.layer.borderColor = UIColor(red: borderColorCodeForTF, green: borderColorCodeForTF, blue: borderColorCodeForTF, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.5
        textField.layer.cornerRadius = 8.0
        textField.clipsToBounds = true
        textField.layer.masksToBounds = true
    }
    
    class func setBorderColor(borderForView:Any) {
        
        (borderForView as AnyObject).layer.borderWidth = 2.0
        //        (borderForView as AnyObject).clipsToBounds = true
        (borderForView as AnyObject).layer.cornerRadius = 2.0
        (borderForView as AnyObject).layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
   class func addGradient(View:UIView){
        
        let gradient:CAGradientLayer = CAGradientLayer()
        gradient.frame.size = View.frame.size
        gradient.colors = [UIColor.white.cgColor,UIColor.white.withAlphaComponent(0).cgColor] //Or any colors
        View.layer.addSublayer(gradient)
        
    }
    
    class func jsonConvertor(arrayToConvert:Array<Any>) -> String {
        
        var dataProduct = Data()
        
        do{
            dataProduct = try JSONSerialization.data(withJSONObject: arrayToConvert, options: .prettyPrinted)
        }
        catch {
            print(error.localizedDescription)
        }
        return NSString(data: dataProduct, encoding: String.Encoding.utf8.rawValue)! as String
        
    }
    
    class func showActivityIndicator(navigationItem:UINavigationItem, msgText:String) {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicatorView.frame = CGRect(x:0, y:0, width:14, height:14)
        activityIndicatorView.color = UIColor.black
        activityIndicatorView.startAnimating()
        
        let titleLabel = UILabel()
        titleLabel.text = msgText
        titleLabel.font = UIFont.italicSystemFont(ofSize: 14)
        
        let fittingSize = titleLabel.sizeThatFits(CGSize(width:200.0, height:activityIndicatorView.frame.size.height))
        titleLabel.frame = CGRect(x:activityIndicatorView.frame.origin.x + activityIndicatorView.frame.size.width + 8, y:activityIndicatorView.frame.origin.y, width:fittingSize.width, height:fittingSize.height)
        
        let titleView = UIView(frame: CGRect(x:((activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width) / 2), y:((activityIndicatorView.frame.size.height) / 2), width:(activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width), height:(activityIndicatorView.frame.size.height)))
        titleView.addSubview(activityIndicatorView)
        titleView.addSubview(titleLabel)
        
        navigationItem.titleView = titleView
    }
    
    class func hideActivityIndicator(navigationItem:UINavigationItem) {
        navigationItem.titleView = nil
    }

    
//    class func keyboardWillShow(notification: NSNotification) {
//        
//        let wrappedDict = notification.object as! [String:Any]
//        let scrollView = wrappedDict["ScrollView"] as! UIScrollView
//        let vcView = wrappedDict["VC"] as! UIViewController
//        let keyboardRectInWindow = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
//        let keyboardSize = vcView.view.convert(keyboardRectInWindow!, from: nil).size
//        var scrollInsets = scrollView.contentInset
//        scrollInsets.bottom = keyboardSize.height + 20
//        scrollView.contentInset = scrollInsets
//        scrollView.scrollIndicatorInsets = scrollInsets
//    }
//    
//    class func keyboardWillHide(notification: NSNotification) {
//       
//        let wrappedDict = notification.object as! [String:Any]
//        let scrollView = wrappedDict["ScrollView"] as! UIScrollView
//        
//        var scrollInsets = scrollView.contentInset
//        scrollInsets.bottom = 0.0
//        scrollView.contentInset = scrollInsets
//        scrollView.scrollIndicatorInsets = scrollInsets
//    }

    
    
    
}
extension UITextView {
    
    /**
     Calculates if new textview height (based on content) is larger than a base height
     
     - parameter baseHeight: The base or minimum height
     
     - returns: The new height
     */
    func newHeight(withBaseHeight baseHeight: CGFloat) -> CGFloat {
        
        // Calculate the required size of the textview
        let fixedWidth = frame.size.width
        let newSize = sizeThatFits(CGSize(width: fixedWidth, height: .greatestFiniteMagnitude))
        var newFrame = frame
        
        // Height is always >= the base height, so calculate the possible new height
        let height: CGFloat = newSize.height > baseHeight ? newSize.height : baseHeight
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: height)
        
        return newFrame.height
    }
}
extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        } else {
            self.drawText(in: rect)
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        guard let text = self.text else { return super.intrinsicContentSize }
        
        var contentSize = super.intrinsicContentSize
        var textWidth: CGFloat = frame.size.width
        var insetsHeight: CGFloat = 0.0
        
        if let insets = padding {
            textWidth -= insets.left + insets.right
            insetsHeight += insets.top + insets.bottom
        }
        
        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                        attributes: [NSAttributedStringKey.font: self.font], context: nil)
        
        contentSize.height = ceil(newSize.size.height) + insetsHeight
        
        return contentSize
    }
}
extension UICollectionViewFlowLayout {
    
    @available(iOS 9.0, *)
    open override func invalidationContext(forInteractivelyMovingItems targetIndexPaths: [IndexPath], withTargetPosition targetPosition: CGPoint, previousIndexPaths: [IndexPath], previousPosition: CGPoint) -> UICollectionViewLayoutInvalidationContext {
        
        let context = super.invalidationContext(forInteractivelyMovingItems: targetIndexPaths, withTargetPosition: targetPosition, previousIndexPaths: previousIndexPaths, previousPosition: previousPosition)
        
        //Check that the movement has actually happeneds
        if previousIndexPaths.first!.item != targetIndexPaths.first!.item {
            collectionView?.dataSource?.collectionView?(collectionView!, moveItemAt: previousIndexPaths.first!, to: targetIndexPaths.last!)
        }
        
        return context
    }
    
    open override func invalidationContextForEndingInteractiveMovementOfItems(toFinalIndexPaths indexPaths: [IndexPath], previousIndexPaths: [IndexPath], movementCancelled: Bool) -> UICollectionViewLayoutInvalidationContext {
        return super.invalidationContextForEndingInteractiveMovementOfItems(toFinalIndexPaths: indexPaths, previousIndexPaths: previousIndexPaths, movementCancelled: movementCancelled)
    }
    
    @available(iOS 9.0, *) //If you'd like to apply some formatting as the dimming of the movable cell
    open override func layoutAttributesForInteractivelyMovingItem(at indexPath: IndexPath, withTargetPosition position: CGPoint) -> UICollectionViewLayoutAttributes {
        let attributes = super.layoutAttributesForInteractivelyMovingItem(at: indexPath, withTargetPosition: position)
        attributes.alpha = 0.8
        return attributes
}
}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
extension UIView {
    func toFullyBottom() {
        self.bottom = superview!.bounds.size.height
        self.autoresizingMask = [UIViewAutoresizing.flexibleTopMargin, UIViewAutoresizing.flexibleWidth]
    }
    
    public var bottom: CGFloat{
        get {
            return self.frame.origin.y + self.frame.size.height
        }
        set {
            var frame = self.frame;
            frame.origin.y = newValue - frame.size.height;
            self.frame = frame;
        }
    }
}
extension UICollectionView {
    func scrollToLast() {
        guard numberOfSections > 0 else {
            return
        }
        
        let lastSection = numberOfSections - 1
        
        guard numberOfItems(inSection: lastSection) > 0 else {
            return
        }
        
        let lastItemIndexPath = IndexPath(item: numberOfItems(inSection: lastSection) - 1,
                                          section: lastSection)
        scrollToItem(at: lastItemIndexPath, at: .right, animated: true)
    }
}
extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
extension UICollectionView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func restore() {
        self.backgroundView = nil

    }
}





