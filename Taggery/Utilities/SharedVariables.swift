//
//  SharedVariables.swift
//  MagmaChamberCalendar
//
//  Created by Balajibabu S.G. on 07/11/17.
//  Copyright © 2017 Balajibabu S.G. All rights reserved.
//

import UIKit

import CoreLocation

protocol UserLocationDelegate {
    func UserLocation(latitude:Double,longitude:Double)
}

class SharedVariables: NSObject {
    
    static let sharedInstance = SharedVariables()
    
    var userDetails = [String:Any]()
    
    var countryListArray = NSArray()
    var selectedCountryDict = [String:Any]()
    var otpString = String()
    var currentLatitude = String()
    var currentLongitude = String()
    var homePage = "0"
    var selectedlocationname = String()
    var selecteddurationname = String()
    
}
