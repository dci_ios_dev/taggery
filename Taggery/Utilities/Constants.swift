//
//  Constants.swift
//  MagmaChamberCalendar
//
//  Created by Balajibabu S.G. on 08/09/17.
//  Copyright © 2017 Balajibabu S.G. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static var k_ApplicationName = "Taggery"
    static var k_ThemeColor = UIColor(red: 246/255, green: 142/255, blue: 18/255, alpha: 1.0)
    static var primaryColor = UIColor(red: 80/255, green: 44/255, blue: 150/255, alpha: 1.0)
    static var k_NVCTitleFont = [ NSAttributedStringKey.font: UIFont(name: "Ubuntu-Medium", size: 18)!, NSAttributedStringKey.foregroundColor: UIColor.white]
    //static var k_Webservice_Header = "Secret:mciosapp|Password:#$71ppj8B9nUs"
    static var k_Webservice_Header = "Secret:7Gz65CQZ8+h&_sZGxXr@FtM3kjDcFRea|Password:TaggeryAPI#"
    static let k_Webservice_URL = "http://api.taggery.dci.in/"
    static let k_Mgs91_AuthKey = "247409Ab3Tajhf5515bec58b0"
    
    static var tagFriends = [friendsContacts]()
    static let k_CountryCode = "91"
    static var userId = String()
    
    static let taggeryProfileUrl = "https://res.cloudinary.com/cloudtaggery18/Profile/"
    static let taggeryPostUrl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/"
    static let taggeryVideoUrl = "https://res.cloudinary.com/cloudtaggery18/video/upload/Add_Post/"
    static var UserLatitude = Double()
    static var UserLongitude = Double()
    static var userFcm = ""
    static var fromFCm = false
    static var fcmKey = 0
//    static let k_CountryCode = "61"
    
    //MSG91
    // MY OWN
}
