/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct AnalyticalList : Codable {
	let notification_created_UserID : Int?
	let notification_created_UserPicture : String?
	let notification_text : String?
	let notification_caption : Int?
	let notification_postImage : String?
	let userTotalLikesCount : Int?
	let notification_postImageType : Int?
	let notification_created_at : String?

	enum CodingKeys: String, CodingKey {

		case notification_created_UserID = "notification_created_UserID"
        case notification_created_UserPicture = "notification_created_UserPicture"
		case notification_text = "notification_text"
		case notification_caption = "notification_caption"
		case notification_postImage = "notification_postImage"
		case userTotalLikesCount = "UserTotalLikesCount"
		case notification_postImageType = "notification_postImageType"
		case notification_created_at = "notification_created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        
        print("value \(values.allKeys)")
		notification_created_UserID = try values.decodeIfPresent(Int.self, forKey: .notification_created_UserID)
		notification_created_UserPicture = try values.decodeIfPresent(String.self, forKey: .notification_created_UserPicture)
		notification_text = try values.decodeIfPresent(String.self, forKey: .notification_text)
		notification_caption = try values.decodeIfPresent(Int.self, forKey: .notification_caption)
		notification_postImage = try values.decodeIfPresent(String.self, forKey: .notification_postImage)
        userTotalLikesCount = try values.decodeIfPresent(Int.self, forKey: .userTotalLikesCount)
		notification_postImageType = try values.decodeIfPresent(Int.self, forKey: .notification_postImageType)
		notification_created_at = try values.decodeIfPresent(String.self, forKey: .notification_created_at)
	}

}
