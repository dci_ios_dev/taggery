//
//  MyAnalyticsViewController.swift
//  Taggery
//
//  Created by Gowsika on 27/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher
import ParallaxHeader

class analyticsCell:UITableViewCell{
    
    @IBOutlet weak var userAvatar: UIImageView!{
        didSet{
            userAvatar.layer.borderWidth = 1
            userAvatar.layer.masksToBounds = false
            userAvatar.layer.borderColor = UIColor.clear.cgColor
            userAvatar.layer.cornerRadius = userAvatar.frame.height/2
            userAvatar.clipsToBounds = true
        }
    }
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var userDetails: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    
    
}

class MyAnalyticsViewController: UIViewController {

    @IBOutlet var headerView: UIView!
    @IBOutlet weak var userAvatar: UIImageView!{
        didSet{
            userAvatar.layer.borderWidth = 1
            userAvatar.layer.masksToBounds = false
            userAvatar.layer.borderColor = UIColor.clear.cgColor
            userAvatar.layer.cornerRadius = userAvatar.frame.height/2
            userAvatar.clipsToBounds = true
        }
    }
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var likesTotalLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var analyticals : Analytical!
    var analyticsList = [AnalyticalList]()
    var pageNumber : Int = 0
    @IBOutlet weak var progressBar: UIProgressView!{
        didSet{
            
           // progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 10)
           progressBar.layer.borderWidth = 1.0
           progressBar.layer.cornerRadius = 10
            progressBar.backgroundColor = UIColor.clear
            progressBar.layer.borderColor = UIColor.white.cgColor
            progressBar.clipsToBounds = true
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Utilities.setBackgroungImage(self.view)
        self.headerView.frame = CGRect(x: 0, y: 10, width: self.view.frame.height, height: 80)
       // self.view.addSubview(self.headerView)
        self.progressBar.backgroundColor = UIColor.clear
        self.progressBar.progressTintColor = UIColor.white
        self.progressBar.progress = 0.0
        self.progressBar.progressViewStyle = .bar
        
        
        self.tableView.parallaxHeader.view = self.headerView
        self.tableView.parallaxHeader.height = 80
        self.tableView.parallaxHeader.minimumHeight = 0
        self.tableView.parallaxHeader.mode = .topFill
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        self.getDataFromApi(pageno: self.pageNumber)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        // Do any additional setup after loading the view.
    }
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
   
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
     
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    
    
    func getDataFromApi(pageno:Int){
        print("Page Number \(pageno)")
        
       Constants.userId = UserDefaults.standard.string(forKey: "userId")!
        let params = ["user_id":"\(Constants.userId)","page":"\(pageno)"]
        let url = "\(Constants.k_Webservice_URL)/myanalyticalpage"
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            let decoder = JSONDecoder()
            do{
                //decoder.keyDecodingStrategy = .
                let datas = try decoder.decode(AnalyticalBase.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                    self.analyticals = datas.analytical!
                  let listData = datas.analytical?.analyticalList!
                    for list in listData!{
                        self.analyticsList.append(list)
                    }
                  //self.analyticsList = datas.analytical!.analyticalList!
                    self.updateui()
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    Utilities.hideLoading()
                }
            }
            catch{
                print("ERROR \(error)")
            }
        }
    }
    
    func updateui(){
        let profileImage = "\(Constants.taggeryProfileUrl)\(self.analyticals.userdetails!.userPicture!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: userAvatar.frame.width, height: userAvatar.frame.height)) >> RoundCornerImageProcessor(cornerRadius: userAvatar.frame.height / 2)
        userAvatar.kf.setImage(with: URL(string: profileImage), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        let actualValue = Double(self.analyticals!.userdetails!.actuallike!)
        let TotalValue = Double(self.analyticals!.userdetails!.nearBigValue!)
        let calculated =  Float(actualValue/TotalValue)
        self.progressBar.progress = calculated
        self.likesTotalLabel.text = "\(self.analyticals!.userdetails!.actuallike!)/\(self.analyticals!.userdetails!.nearBigValue!)"
    }

}
extension MyAnalyticsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return analyticsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "analyticsCell", for: indexPath) as! analyticsCell
        if(self.analyticsList.count > 0){
        
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.lightGray,.font :UIFont.systemFont(ofSize: 14.0)]
            
        var minText = self.analyticsList[indexPath.row].notification_created_at!
          
        let wordstoRemove = "before"
        if let range = minText.range(of: wordstoRemove) {
                minText.removeSubrange(range)
            }
        
        let minutes = NSAttributedString(string: "\(minText)", attributes: myAttribute)
        let textString = NSAttributedString(string: "\(self.analyticsList[indexPath.row].notification_text!) \n")
        let attributes = NSMutableAttributedString()
        attributes.append(textString)
        attributes.append(minutes)
            
        cell.userDetails.attributedText = attributes
        var postImage = ""
        let profileImage = "\(Constants.taggeryProfileUrl)\(self.analyticsList[indexPath.row].notification_created_UserPicture!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.userAvatar.frame.width, height: cell.userAvatar.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.userAvatar.frame.height / 2)
            cell.userAvatar.kf.setImage(with: URL(string: profileImage), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
       
            if(self.analyticsList[indexPath.row].notification_postImageType! == 1){
               postImage = "\(Constants.taggeryPostUrl)\(self.analyticsList[indexPath.row].notification_postImage!)"
            }
            else
            {
                postImage = "\(Constants.taggeryVideoUrl)\(self.analyticsList[indexPath.row].notification_postImage!).jpg"
            }
            cell.postImage.kf.setImage(with: URL(string: postImage), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
        }
      
        return cell
    }
}
extension MyAnalyticsViewController : UIScrollViewDelegate{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(scrollView == tableView){
            if(tableView.contentOffset.y+tableView.bounds.height >= tableView.contentSize.height){
               self.pageNumber = self.pageNumber + 1
              
                self.getDataFromApi(pageno:self.pageNumber)
            }
        }
    }
}
