/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct RelatedPostsID : Codable {
	let postID : Int?
	let postUserID : Int?
	let postMediaType : Int?
	let postUserFName : String?
	let postUserLName : String?
	let postUserProfileImage : String?
	let postType : String?
	let postText : String?
	let postImage : String?
	let postLocation : String?
	let postLatitude : String?
	let postLongitude : String?
	let postTimeFrame : Int?
	let postStatus : Int?
	let feedtime : String?
	let postTagList : [String]?
	let viewscount : Int?
	let likestcount : Int?
	let likes : Int?
	let views : Int?

	enum CodingKeys: String, CodingKey {

		case postID = "PostID"
		case postUserID = "PostUserID"
		case postMediaType = "PostMediaType"
		case postUserFName = "PostUserFName"
		case postUserLName = "PostUserLName"
		case postUserProfileImage = "PostUserProfileImage"
		case postType = "PostType"
		case postText = "PostText"
		case postImage = "PostImage"
		case postLocation = "PostLocation"
		case postLatitude = "PostLatitude"
		case postLongitude = "PostLongitude"
		case postTimeFrame = "PostTimeFrame"
		case postStatus = "PostStatus"
		case feedtime = "Feedtime"
		case postTagList = "PostTagList"
		case viewscount = "viewscount"
		case likestcount = "likestcount"
		case likes = "Likes"
		case views = "Views"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        postID = try values.decodeIfPresent(Int.self, forKey: .postID)
        postUserID = try values.decodeIfPresent(Int.self, forKey: .postUserID)
        postMediaType = try values.decodeIfPresent(Int.self, forKey: .postMediaType)
        postUserFName = try values.decodeIfPresent(String.self, forKey: .postUserFName)
        postUserLName = try values.decodeIfPresent(String.self, forKey: .postUserLName)
        postUserProfileImage = try values.decodeIfPresent(String.self, forKey: .postUserProfileImage)
        postType = try values.decodeIfPresent(String.self, forKey: .postType)
        postText = try values.decodeIfPresent(String.self, forKey: .postText)
        postImage = try values.decodeIfPresent(String.self, forKey: .postImage)
        postLocation = try values.decodeIfPresent(String.self, forKey: .postLocation)
		postLatitude = try values.decodeIfPresent(String.self, forKey: .postLatitude)
		postLongitude = try values.decodeIfPresent(String.self, forKey: .postLongitude)
		postTimeFrame = try values.decodeIfPresent(Int.self, forKey: .postTimeFrame)
		postStatus = try values.decodeIfPresent(Int.self, forKey: .postStatus)
        feedtime = try values.decodeIfPresent(String.self, forKey: .feedtime)
		postTagList = try values.decodeIfPresent([String].self, forKey: .postTagList)
		viewscount = try values.decodeIfPresent(Int.self, forKey: .viewscount)
		likestcount = try values.decodeIfPresent(Int.self, forKey: .likestcount)
		likes = try values.decodeIfPresent(Int.self, forKey: .likes)
		views = try values.decodeIfPresent(Int.self, forKey: .views)
	}

}
