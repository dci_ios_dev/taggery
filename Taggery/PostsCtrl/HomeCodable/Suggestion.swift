

import Foundation
struct Suggestion : Codable {
	let snapID : Int?
	let id : Int?
	let userFirstName : String?
	let userLastName : String?
	let userName : String?
	let userPicture : String?
	let status : String?

	enum CodingKeys: String, CodingKey {

		case snapID = "SnapID"
		case id = "id"
		case userFirstName = "UserFirstName"
		case userLastName = "UserLastName"
		case userName = "UserName"
		case userPicture = "UserPicture"
		case status = "Status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		snapID = try values.decodeIfPresent(Int.self, forKey: .snapID)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		userFirstName = try values.decodeIfPresent(String.self, forKey: .userFirstName)
		userLastName = try values.decodeIfPresent(String.self, forKey: .userLastName)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		userPicture = try values.decodeIfPresent(String.self, forKey: .userPicture)
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}
