//
//  Header.swift
//  Taggery
//
//  Created by Gowsika on 14/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class HeaderVI: UIView {

    @IBOutlet weak var messageButton: UIButton!{
        
        didSet{
            messageButton.imageView?.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet var contentViews: UIView!
    @IBOutlet weak var profileImage: UIImageView!{
        didSet{
            profileImage.layer.borderWidth = 1
            profileImage.layer.masksToBounds = false
            profileImage.layer.borderColor = UIColor.black.cgColor
            profileImage.layer.cornerRadius = profileImage.frame.height/2
            profileImage.clipsToBounds = true
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
    Bundle.main.loadNibNamed("HeaderVI", owner: self, options: nil)
    addSubview(contentViews)
    contentViews.frame = self.bounds
        contentViews.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    }
    
}
