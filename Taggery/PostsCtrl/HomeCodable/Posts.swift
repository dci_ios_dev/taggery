/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Posts : Codable {
	let snapID : Int?
	let postID : Int?
	let postImage : String?
	let postMediaType : Int?
	let userFirstName : String?
	let userLastName : String?
	let postUserID : Int?
	let userPicture : String?
	let likeCount : Int?
	let viewCount : Int?
	let feedtime : String?
	let likes : Int?
	let views : Int?
	let seenStatus : Int?
	let relatedPostsID : [RelatedPostsID]?

	enum CodingKeys: String, CodingKey {

		case snapID = "SnapID"
		case postID = "PostID"
		case postImage = "PostImage"
		case postMediaType = "PostMediaType"
		case userFirstName = "UserFirstName"
		case userLastName = "UserLastName"
		case postUserID = "PostUserID"
		case userPicture = "UserPicture"
		case likeCount = "LikeCount"
		case viewCount = "ViewCount"
		case feedtime = "Feedtime"
		case likes = "Likes"
		case views = "Views"
		case seenStatus = "SeenStatus"
		case relatedPostsID = "RelatedPostsID"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        snapID = try values.decodeIfPresent(Int.self, forKey: .snapID)
        postID = try values.decodeIfPresent(Int.self, forKey: .postID)
        postImage = try values.decodeIfPresent(String.self, forKey: .postImage)
        postMediaType = try values.decodeIfPresent(Int.self, forKey: .postMediaType)
        userFirstName = try values.decodeIfPresent(String.self, forKey: .userFirstName)
        userLastName = try values.decodeIfPresent(String.self, forKey: .userLastName)
        postUserID = try values.decodeIfPresent(Int.self, forKey: .postUserID)
        userPicture = try values.decodeIfPresent(String.self, forKey: .userPicture)
        likeCount = try values.decodeIfPresent(Int.self, forKey: .likeCount)
        viewCount = try values.decodeIfPresent(Int.self, forKey: .viewCount)
		feedtime = try values.decodeIfPresent(String.self, forKey: .feedtime)
		likes = try values.decodeIfPresent(Int.self, forKey: .likes)
		views = try values.decodeIfPresent(Int.self, forKey: .views)
		seenStatus = try values.decodeIfPresent(Int.self, forKey: .seenStatus)
		relatedPostsID = try values.decodeIfPresent([RelatedPostsID].self, forKey: .relatedPostsID)
	}

}
