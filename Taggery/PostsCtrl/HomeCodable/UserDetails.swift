/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserDetails : Codable {
	let id : Int?
	let userName : String?
	let userFirstName : String?
	let userLastName : String?
	let userCountryCode : String?
	let userMobileNumber : String?
	let userGender : String?
	let userRelationshipStatus : String?
	let userPicture : String?
	let userEmail : String?
	let userCoverPhoto : String?
	let userDescription : String?
	let created_at : String?
	let updated_at : String?
	let userStatus : Int?
	let password : String?
	let password_confirmation : String?
	let userSalt : String?
	let userDeviceID : String?
	let userFCMKey : String?
	let userDeviceType : String?
	let userLastLogin : String?
	let userLastOnlineTime : String?
	let userOTPFpDate : String?
	let userOTPFpCount : Int?
	let userOTP : Int?
	let userOTPRegDate : String?
	let userOTPRegCount : Int?
	let userPrivateProfile : Int?
	let usershowLike_Rank : Int?
	let userCountryId : Int?
	let userProfileType : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case userName = "UserName"
		case userFirstName = "UserFirstName"
		case userLastName = "UserLastName"
		case userCountryCode = "UserCountryCode"
		case userMobileNumber = "UserMobileNumber"
		case userGender = "UserGender"
		case userRelationshipStatus = "UserRelationshipStatus"
		case userPicture = "UserPicture"
		case userEmail = "UserEmail"
		case userCoverPhoto = "UserCoverPhoto"
		case userDescription = "UserDescription"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case userStatus = "UserStatus"
		case password = "password"
		case password_confirmation = "password_confirmation"
		case userSalt = "UserSalt"
		case userDeviceID = "UserDeviceID"
		case userFCMKey = "UserFCMKey"
		case userDeviceType = "UserDeviceType"
		case userLastLogin = "UserLastLogin"
		case userLastOnlineTime = "UserLastOnlineTime"
		case userOTPFpDate = "UserOTPFpDate"
		case userOTPFpCount = "UserOTPFpCount"
		case userOTP = "UserOTP"
		case userOTPRegDate = "UserOTPRegDate"
		case userOTPRegCount = "UserOTPRegCount"
		case userPrivateProfile = "UserPrivateProfile"
		case usershowLike_Rank = "UsershowLike_Rank"
		case userCountryId = "UserCountryId"
		case userProfileType = "UserProfileType"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        userFirstName = try values.decodeIfPresent(String.self, forKey: .userFirstName)
        userLastName = try values.decodeIfPresent(String.self, forKey: .userLastName)
        userCountryCode = try values.decodeIfPresent(String.self, forKey: .userCountryCode)
        userMobileNumber = try values.decodeIfPresent(String.self, forKey: .userMobileNumber)
        userGender = try values.decodeIfPresent(String.self, forKey: .userGender)
        userRelationshipStatus = try values.decodeIfPresent(String.self, forKey: .userRelationshipStatus)
        userPicture = try values.decodeIfPresent(String.self, forKey: .userPicture)
        userEmail = try values.decodeIfPresent(String.self, forKey: .userEmail)
        userCoverPhoto = try values.decodeIfPresent(String.self, forKey: .userCoverPhoto)
		userDescription = try values.decodeIfPresent(String.self, forKey: .userDescription)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		userStatus = try values.decodeIfPresent(Int.self, forKey: .userStatus)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		password_confirmation = try values.decodeIfPresent(String.self, forKey: .password_confirmation)
		userSalt = try values.decodeIfPresent(String.self, forKey: .userSalt)
		userDeviceID = try values.decodeIfPresent(String.self, forKey: .userDeviceID)
		userFCMKey = try values.decodeIfPresent(String.self, forKey: .userFCMKey)
		userDeviceType = try values.decodeIfPresent(String.self, forKey: .userDeviceType)
		userLastLogin = try values.decodeIfPresent(String.self, forKey: .userLastLogin)
		userLastOnlineTime = try values.decodeIfPresent(String.self, forKey: .userLastOnlineTime)
		userOTPFpDate = try values.decodeIfPresent(String.self, forKey: .userOTPFpDate)
		userOTPFpCount = try values.decodeIfPresent(Int.self, forKey: .userOTPFpCount)
		userOTP = try values.decodeIfPresent(Int.self, forKey: .userOTP)
		userOTPRegDate = try values.decodeIfPresent(String.self, forKey: .userOTPRegDate)
		userOTPRegCount = try values.decodeIfPresent(Int.self, forKey: .userOTPRegCount)
		userPrivateProfile = try values.decodeIfPresent(Int.self, forKey: .userPrivateProfile)
		usershowLike_Rank = try values.decodeIfPresent(Int.self, forKey: .usershowLike_Rank)
		userCountryId = try values.decodeIfPresent(Int.self, forKey: .userCountryId)
		userProfileType = try values.decodeIfPresent(Int.self, forKey: .userProfileType)
	}

}
