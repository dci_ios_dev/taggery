/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Base : Codable {
	let status : String?
	let statusCode : Int?
	let profilePic : String?
	let currentPage : String?
	let suggestion : [Suggestion]?
	let location : [Location]?
	let posts : [Posts]?
	let totalPage : Int?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case status = "Status"
		case statusCode = "StatusCode"
		case profilePic = "ProfilePic"
		case currentPage = "CurrentPage"
		case suggestion = "suggestion"
		case location = "location"
		case posts = "Posts"
		case totalPage = "TotalPage"
		case message = "Message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        profilePic = try values.decodeIfPresent(String.self, forKey: .profilePic)
        currentPage = try values.decodeIfPresent(String.self, forKey: .currentPage)
		suggestion = try values.decodeIfPresent([Suggestion].self, forKey: .suggestion)
		location = try values.decodeIfPresent([Location].self, forKey: .location)
        posts = try values.decodeIfPresent([Posts].self, forKey: .posts)
        totalPage = try values.decodeIfPresent(Int.self, forKey: .totalPage)
        message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}
