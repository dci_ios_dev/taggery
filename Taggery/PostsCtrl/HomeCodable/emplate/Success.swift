//
//  Success.swift
//  Taggery
//
//  Created by Gowsika on 14/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import Foundation
struct Success : Codable {
    let success : String?
    let statusCode:Int?
    
    enum CodingKeys: String, CodingKey {
        case success = "Status"
        case statusCode = "StatusCode"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
    }

}
