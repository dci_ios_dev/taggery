//
//  PostsViewController.swift
//  Taggery
//
//  Created by Gowsika on 12/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

import FontAwesome_swift
import ParallaxHeader
import SwiftyJSON
import SDWebImage
import Cloudinary

import Kingfisher
import IQKeyboardManagerSwift


class CustomsViewCollec:UICollectionViewCell{
    @IBOutlet weak var profileImage: UIImageView!{
        didSet{
//            profileImage.layer.masksToBounds = false
//            profileImage.layer.borderColor = UIColor.black.cgColor
//            profileImage.layer.cornerRadius = profileImage.frame.height/2
//            profileImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var requestButton: UIButton!
    var suggestionCellArray = [Suggestion]()

    override func awakeFromNib() {
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
//
    }
    
    @IBAction func loadRequest(_ sender: UIButton) {
        let params  = ["position":sender.tag]
        NotificationCenter.default.post(name: Notification.Name("requestSelected"), object: nil, userInfo: params)
    }
        override func prepareForReuse() {
        super.prepareForReuse()
        //userName.text = ""
        userName.textAlignment = .center
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.profileImage.layoutIfNeeded()       // add this
//        self.profileImage.layer.cornerRadius = profileImage.frame.width/2
//        self.profileImage.clipsToBounds = true

    }
}

class collectionViewFriendsSuggestions:UITableViewCell{
    
    @IBOutlet weak var collectionViewsFriends : UICollectionView!
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionViewsFriends.delegate = dataSourceDelegate
        collectionViewsFriends.dataSource = dataSourceDelegate
        collectionViewsFriends.tag = row
        collectionViewsFriends.reloadData()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if let flowlayout = collectionViewsFriends.collectionViewLayout as? UICollectionViewFlowLayout{
            //flowlayout.estimatedItemSize = CGSize(width:1,height:1)
            if #available(iOS 10.0, *) {
                   //flowlayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
            } else {
                // Fallback on earlier versions
            }
            
        }
    }
}

class suggestionsColletions:UITableViewCell{
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var playImage: UIImageView!
    override func awakeFromNib() {
        backgroundImage.layer.cornerRadius = 10
        backgroundImage.layer.masksToBounds = true
    }
}
class friendsColletions:UITableViewCell{

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        //self.collectionSuggestionView.delegate = self
        // self.collectionSuggestionView.dataSource = self
        backgroundImage.layer.cornerRadius = 10
        backgroundImage.layer.masksToBounds = true
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.clear.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        locationText.font = UIFont.fontAwesome(ofSize: 16, style: .solid)
     //   locationText.text = String.fontAwesomeIcon(name: .heart) + " 23" + " "+String.fontAwesomeIcon(name: .eye)+" 5"
    }
}




class PostsViewController: UIViewController {
    var sectionsCount = [Int]()
    var baseArray = [Base]()
    var suggestionsarray = [Suggestion]()
    var locationsArray = [Location]()
    var postsArray = [Posts]()
    var seenPostsArray = [Posts]()
    var customViews = HeaderVI()
    var pageNumber = 1
    
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var tabelViewPosts: UITableView!{
        didSet{
          
          
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.tintColor = UIColor.white
        //refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        //tabelViewPosts.addSubview(refreshControl) // not required when using UITableViewController
        
        self.tabelViewPosts.delegate = self
        self.tabelViewPosts.dataSource = self
        // Do any additional setup after loading the view.
        
         customViews.frame = CGRect(x:0,y:10,width:UIScreen.main.bounds.size.width,height:self.view.frame.height / 4)
        customViews.messageButton.addTarget(self, action: #selector(messageTapped), for: .touchUpInside)
        customViews.messageButton.isHidden = true
        tabelViewPosts.parallaxHeader.view = customViews
        tabelViewPosts.parallaxHeader.height = self.view.frame.height / 5
        tabelViewPosts.parallaxHeader.minimumHeight = 0
        tabelViewPosts.parallaxHeader.mode = .top
      //  self.getDataFromApi(pageNumber:1)
        
        self.tabelViewPosts.backgroundColor = UIColor.clear
        //  self.tabelViewPosts.separatorStyle = .none
        self.tabelViewPosts.estimatedRowHeight = 280
        self.tabelViewPosts.rowHeight = UITableViewAutomaticDimension
        self.tabelViewPosts.backgroundColor = UIColor.clear
        self.tabelViewPosts.tableHeaderView?.backgroundColor = UIColor.clear
        self.tabelViewPosts.reloadData()
        
        self.initializeSwipe()
         NotificationCenter.default.addObserver(self, selector: #selector(followSelected(notification:)), name: NSNotification.Name("requestSelected"), object: nil)
        
        
        
    }
    
    @objc func showProfile(sender:UITapGestureRecognizer){
        let tappedImage = sender.view as! UIImageView
        let tag = tappedImage.tag
        let user_id = suggestionsarray[tag].id!
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewController.followingUserId = "\(user_id)"
        self.navigationController?.pushViewController(viewController, animated: false)
        print("User_id \(user_id)")
        
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.postsArray.removeAll()
        self.suggestionsarray.removeAll()
        self.locationsArray.removeAll()
        self.seenPostsArray.removeAll()
        
        self.getDataFromApi(pageNumber:1)
    }
    
    @objc func messageTapped() {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        let transition = CATransition()
        transition.duration = 0.40
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(leftViewController, animated: false)
    }
    
    func initializeSwipe(){
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
                swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
            if gesture.direction == UISwipeGestureRecognizerDirection.right {
                print("Swipe Right Pick")
        let rightViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                let transition = CATransition()
                transition.duration = 0.40
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                navigationController?.view.layer.add(transition, forKey: kCATransition)
                navigationController?.pushViewController(rightViewController, animated: false)
        
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.left {
                
                let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                let transition = CATransition()
                transition.duration = 0.40
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                navigationController?.view.layer.add(transition, forKey: kCATransition)
                navigationController?.pushViewController(leftViewController, animated: false)
                
            }
        }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.postsArray.removeAll()
        self.suggestionsarray.removeAll()
        self.locationsArray.removeAll()
        self.seenPostsArray.removeAll()
        
         self.getDataFromApi(pageNumber:pageNumber)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func getDataFromApi(pageNumber:Int){
        
       //refreshControl.endRefreshing()
        let params = ["user_id":"\(Constants.userId)","page":"\(pageNumber)","lat":"\(Constants.UserLatitude)","lng":"\(Constants.UserLongitude)"] as [String:String]
        let url = "\(Constants.k_Webservice_URL)homepage"
        
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            
            let decoder = JSONDecoder()
             do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Base.self, from: Data)
               
                if(datas.statusCode! == 200){
                    let suggestRaw = datas.suggestion!
                    
                   let profileurl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(datas.profilePic!)"
                    self.customViews.profileImage.backgroundColor = UIColor.clear
                    let processor = ResizingImageProcessor(referenceSize: CGSize(width: self.customViews.profileImage.frame.width, height: self.customViews.profileImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius: self.customViews.profileImage.frame.height / 2)
                    self.customViews.profileImage.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
                    self.suggestionsarray.removeAll()
                    for suggests in suggestRaw{
                        
                        if(suggests.status! == "2"){
                        }
                        else
                        {
                            self.suggestionsarray.append(suggests)
                        }
                    }
                    
                    self.locationsArray = datas.location!
                    
                    let unmodifiedPostsArray = datas.posts!
                    if(unmodifiedPostsArray.count > 0){
                       
                        for posts in unmodifiedPostsArray{
                            if(posts.seenStatus! == 0){
                                 self.postsArray.append(posts)
                            }else{
                                self.seenPostsArray.append(posts)
                            }
                        }
                    }
                }
                else
                {
                   self.tabelViewPosts.setEmptyMessage("No posts Available")
                }
                DispatchQueue.main.async {
                    Utilities.hideLoading()
                    self.tabelViewPosts.reloadData()
                    
                }
               
            }
            catch{
                print("ERROR \(error)")
                self.tabelViewPosts.setEmptyMessage("No posts Available")
        }
        }
    }
    
    @objc func followSelected(notification:NSNotification){
        
        let arrayPosition = notification.userInfo?["position"] as! Int
        let followUrl = "\(Constants.k_Webservice_URL)/follow"
        let FollowerUserID = self.suggestionsarray[arrayPosition].id
        let FollowingUserID = Constants.userId
        let params = ["FollowerUserID":"\(Constants.userId)","FollowingUserID":"\(FollowerUserID)"] as [String:String]

        Utilities.showLoading()
        WebServiceHandler.sharedInstance.commonPostRequest(url: followUrl, params: params) { JSON in
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                self.tabelViewPosts.reloadData()
               // Utilities.showAlertView("\(Message)", onView: self)
            }
            if(StatusCode == 500)
            {
               // Utilities.showAlertView("\(Message)", onView: self)
            }
            
            Utilities.hideLoading()
        }
    }
}

extension PostsViewController: UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return suggestionsarray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
////        if(section == 0){
////            return 0.1
////        }
//        return 10
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
//        if(indexPath.section == 0){
//           // return 0.1
//        }
         if(indexPath.section == 1)
        {
            return 200
        }
        else {
             return UITableViewAutomaticDimension
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "collections", for: indexPath) as! CustomsViewCollec
        //cells.userName.sizeToFit()
        cells.userName.text = "\(suggestionsarray[indexPath.row].userFirstName!)"
        cells.userName.textAlignment = .center
        cells.userName.textColor = UIColor.white
        cells.profileImage.isUserInteractionEnabled = true
        cells.profileImage.tag = indexPath.row
        cells.profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showProfile(sender:))))
        
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
        
        if(suggestionsarray[indexPath.row].userPicture! != "")
        {
            let imageUrl = suggestionsarray[indexPath.row].userPicture!.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            print("IMGS \(imageUrl)")
           let profileurl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(imageUrl[0])"
            cells.profileImage.backgroundColor = UIColor.clear
            let processor = ResizingImageProcessor(referenceSize: CGSize(width: cells.profileImage.frame.width, height: cells.profileImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cells.profileImage.frame.height / 2)
            cells.profileImage.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
            
            cells.profileImage.contentMode = .scaleAspectFill
            cells.profileImage.clipsToBounds = true

        }else{
            cells.profileImage.contentMode = .scaleAspectFill
            cells.profileImage.clipsToBounds = true
            cells.profileImage.image = UIImage(named: "defaultAvatar")
        }
        cells.requestButton.layer.cornerRadius = 10.0
        switch suggestionsarray[indexPath.row].status {
        case "1":
               cells.requestButton.setTitle("Requested", for: UIControlState.normal)
               cells.requestButton.backgroundColor = UIColor.white
               cells.requestButton.setTitleColor(UIColor.init(hexString: "#502C96"), for: UIControlState.normal)
            break
        case "2":
            cells.requestButton.setTitle("Following", for: UIControlState.normal)
            break
        case "3":
            cells.requestButton.setTitle("Follow", for: UIControlState.normal)
            cells.requestButton.setTitleColor(UIColor.init(hexString: "#ffffff"), for: UIControlState.normal)

            cells.requestButton.backgroundColor = UIColor.init(hexString: "#502C96")
            break
        default:
                break
        }
        cells.requestButton.tag = indexPath.row
        
        //        cloudinary.createDownloader().fetchImage("Profile/\(suggestionsarray[indexPath.row].userPicture!)", { progress in
//            // Handle progress
//        }) { responseImage, error in
//            DispatchQueue.main.async {
//                cells.profileImage.image = responseImage
//
//            }
//        }
        
        
        return cells
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(section == 0){
//            return 1
//        }
       
        if(section == 1){
            if(suggestionsarray.count > 0){
                return 1
            }
        }
        
        if(section == 2){
            return locationsArray.count
        }
        if(section == 0){
            
            print("POST COUNT FINAL IS \(postsArray.count)")
            return postsArray.count
        }
        if(section == 3){
            return seenPostsArray.count
        }

        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if(indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "friendsCollections", for:indexPath) as! friendsColletions
            cell.backgroundColor = UIColor.clear
            cell.layer.cornerRadius = 10
            cell.layer.masksToBounds = true
            
            let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(postsArray[indexPath.row].userPicture!)"
            let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.profileImage.frame.width, height: cell.profileImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.profileImage.frame.height / 2)
            cell.profileImage.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
            
            
            
            if(postsArray.count > 0){
                cell.userName.text = "\(postsArray[indexPath.row].userFirstName!)"
                
                let viewsCount = postsArray[indexPath.row].viewCount
                let likesCount = postsArray[indexPath.row].likeCount
                let postTime = postsArray[indexPath.row].feedtime
                var likesString = NSAttributedString()
                let likesAttributedString = NSMutableAttributedString()
                var seenString = NSAttributedString()
                var seenAttributedString = NSMutableAttributedString()
                if(likesCount! > 0){
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.red ]
                    likesString = NSAttributedString(string: String.fontAwesomeIcon(name: .heart), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(likesString)
                    likesAttributedString.append(valueString)
                }
                else{
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ]
                    likesString = NSAttributedString(string: String.fontAwesomeIcon(name: .heart), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(likesString)
                    likesAttributedString.append(valueString)
                }
                if(viewsCount! > 0){
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ]
                    seenString = NSAttributedString(string: String.fontAwesomeIcon(name: .eye), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    let emptyString = NSAttributedString(string: "   ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(emptyString)
                    likesAttributedString.append(seenString)
                    likesAttributedString.append(valueString)
                }
                else{
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ]
                    likesString = NSAttributedString(string: String.fontAwesomeIcon(name: .eye), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    let emptyString = NSAttributedString(string: "   ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(emptyString)
                    likesAttributedString.append(seenString)
                    likesAttributedString.append(valueString)
                }
                
                print("POST TIMES \(postTime!)")
                cell.timeLabel.text = "\(postTime!)"
                cell.locationText.attributedText = likesAttributedString
                //  cell.locationText.text = "\(likesString)" + " \(viewsCount!)" + " "+String.fontAwesomeIcon(name: .eye)+" \(likesCount!)"
                if(postsArray[indexPath.row].postMediaType! == 0 || postsArray[indexPath.row].postMediaType! == 1){
                    if(postsArray[indexPath.row].postImage != ""){
                        let imageurl = postsArray[indexPath.row].postImage?.components(separatedBy: CharacterSet.whitespacesAndNewlines)[0]
                        let postImaeUrl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(imageurl!)"
                        cell.backgroundImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                        cell.backgroundImage.contentMode = .scaleAspectFill
                        cell.backgroundImage.clipsToBounds = true

                        cell.locationText.font = UIFont.fontAwesome(ofSize: 16, style: .solid)
                        
                    }
                }
                else{
                    if(postsArray[indexPath.row].postMediaType! == 2 ){
                        if(postsArray[indexPath.row].postImage != ""){
                            let imageurl = postsArray[indexPath.row].postImage?.components(separatedBy: CharacterSet.whitespacesAndNewlines)[0]
                            var str = imageurl!
                            
                            if let dotRange = str.range(of: ".") {
                                str.removeSubrange(dotRange.lowerBound..<str.endIndex)
                            }
                            let postImaeUrl = "\(Constants.taggeryVideoUrl)\(str).jpg"
                            print("PPSt \(postImaeUrl)")
                            cell.backgroundImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                            cell.backgroundImage.contentMode = .scaleAspectFill
                            cell.backgroundImage.clipsToBounds = true

                            cell.locationText.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
                            
                        }
                    }
                }
                return cell
            }
            else
            {
                // cell.isHidden = true
                return cell
            }
            
        }
        if(indexPath.section == 2)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "locationColletions", for:indexPath) as! suggestionsColletions
            cell.backgroundColor = UIColor.clear
            cell.layer.cornerRadius = 10
            cell.layer.masksToBounds = true
            if(locationsArray[indexPath.row].postMediaType! == 0 || locationsArray[indexPath.row].postMediaType! == 1){
            if(locationsArray[indexPath.row].postImage != ""){
              let imageurl = locationsArray[indexPath.row].postImage?.components(separatedBy: CharacterSet.whitespacesAndNewlines)[0]
              let postImaeUrl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(imageurl!)"
              cell.backgroundImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
              cell.backgroundImage.contentMode = .scaleAspectFill
              cell.backgroundImage.clipsToBounds = true
              cell.locationText.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
              cell.locationText.text = String.fontAwesomeIcon(name: .mapMarkerAlt) + "  \(locationsArray[indexPath.row].postText!)"
              cell.playImage.isHidden = true
            }
            }
            else{
                if(locationsArray[indexPath.row].postMediaType! == 2 ){
                    if(locationsArray[indexPath.row].postImage != ""){
                        let imageurl = locationsArray[indexPath.row].postImage?.components(separatedBy: CharacterSet.whitespacesAndNewlines)[0]
                        var str = imageurl!
                        
                        if let dotRange = str.range(of: ".") {
                            str.removeSubrange(dotRange.lowerBound..<str.endIndex)
                        }
                        let postImaeUrl = "\(Constants.taggeryVideoUrl)\(str).jpg"
                        print("Video image \(postImaeUrl)")
                        cell.backgroundImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                        cell.backgroundImage.contentMode = .scaleAspectFill
                        cell.backgroundImage.clipsToBounds = true

                        cell.locationText.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
                       cell.locationText.text = String.fontAwesomeIcon(name: .mapMarkerAlt) + "  \(locationsArray[indexPath.row].postText!)"
                        cell.playImage.isHidden = false
                    }
                }
            }
            return cell
        }
        if(indexPath.section == 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "collections", for:indexPath) as! collectionViewFriendsSuggestions
            cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            cell.collectionViewsFriends.reloadData()
            return cell
        }
        if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "friendsCollections", for:indexPath) as! friendsColletions
            cell.backgroundColor = UIColor.clear
            cell.layer.cornerRadius = 10
            cell.layer.masksToBounds = true
            
            let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(seenPostsArray[indexPath.row].userPicture!)"
            let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.profileImage.frame.width, height: cell.profileImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.profileImage.frame.height / 2)
            cell.profileImage.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
            
            
            
            if(seenPostsArray.count > 0){
                cell.userName.text = "\(seenPostsArray[indexPath.row].userFirstName!)"
                
                let viewsCount = seenPostsArray[indexPath.row].viewCount
                let likesCount = seenPostsArray[indexPath.row].likeCount
                let postTime = seenPostsArray[indexPath.row].feedtime
                var likesString = NSAttributedString()
                let likesAttributedString = NSMutableAttributedString()
                var seenString = NSAttributedString()
                var seenAttributedString = NSMutableAttributedString()
                if(likesCount! > 0){
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.red ]
                    likesString = NSAttributedString(string: String.fontAwesomeIcon(name: .heart), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(likesString)
                    likesAttributedString.append(valueString)
                }
                else{
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ]
                    likesString = NSAttributedString(string: String.fontAwesomeIcon(name: .heart), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(likesString)
                    likesAttributedString.append(valueString)
                }
                if(viewsCount! > 0){
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ]
                    seenString = NSAttributedString(string: String.fontAwesomeIcon(name: .eye), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    let emptyString = NSAttributedString(string: "   ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(emptyString)
                    likesAttributedString.append(seenString)
                    likesAttributedString.append(valueString)
                }
                else{
                    let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.white ]
                    likesString = NSAttributedString(string: String.fontAwesomeIcon(name: .eye), attributes: myAttribute)
                    let valueString = NSAttributedString(string: " \(viewsCount!)", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    let emptyString = NSAttributedString(string: "   ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
                    likesAttributedString.append(emptyString)
                    likesAttributedString.append(seenString)
                    likesAttributedString.append(valueString)
                }
                
                print("POST TIMES \(postTime!)")
                cell.timeLabel.text = "\(postTime!)"
                cell.locationText.attributedText = likesAttributedString
                //  cell.locationText.text = "\(likesString)" + " \(viewsCount!)" + " "+String.fontAwesomeIcon(name: .eye)+" \(likesCount!)"
                if(seenPostsArray[indexPath.row].postMediaType! == 0 || seenPostsArray[indexPath.row].postMediaType! == 1){
                    if(seenPostsArray[indexPath.row].postImage != ""){
                        let imageurl = seenPostsArray[indexPath.row].postImage?.components(separatedBy: CharacterSet.whitespacesAndNewlines)[0]
                        let postImaeUrl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(imageurl!)"
                        cell.backgroundImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                        cell.backgroundImage.contentMode = .scaleAspectFill
                        cell.backgroundImage.clipsToBounds = true

                        cell.locationText.font = UIFont.fontAwesome(ofSize: 16, style: .solid)
                        
                    }
                }
                else{
                    if(seenPostsArray[indexPath.row].postMediaType! == 2 ){
                        if(seenPostsArray[indexPath.row].postImage != ""){
                            let imageurl = seenPostsArray[indexPath.row].postImage?.components(separatedBy: CharacterSet.whitespacesAndNewlines)[0]
                            let postImaeUrl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(imageurl!)"
                            print("PPSt \(postImaeUrl)")
                            cell.backgroundImage.kf.setImage(with: URL(string: postImaeUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                            cell.backgroundImage.contentMode = .scaleAspectFill
                            cell.backgroundImage.clipsToBounds = true

                            cell.locationText.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
                            
                        }
                    }
                }
                return cell
            }
            else
            {
                // cell.isHidden = true
                return cell
            }
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        
        if(indexPath.section == 1){
            print("CAlled")
        }
        
        if(indexPath.section == 0){
            let url = "\(Constants.k_Webservice_URL)/viewpost"
            let params = ["ViewUserID":"\(Constants.userId)","ViewPostID":"\(postsArray[indexPath.row].postID!)"] as [String:String]
            
            
            WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
                guard let StatusCode = JSON["StatusCode"].int else{
                    return
                }
                print("CALLED \(JSON)")
                guard let Message = JSON["Message"].string else{
                    return
                }
                if(StatusCode == 200)
                {
                }
                if(StatusCode == 500)
                {
                }
            }
            
            let postDetails = self.postsArray[indexPath.row].relatedPostsID
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
            viewController.postDetails = postDetails!
            self.present(viewController, animated: true, completion: nil)
                }
        else if (indexPath.section == 3){
            let url = "\(Constants.k_Webservice_URL)/viewpost"
            let params = ["ViewUserID":"\(Constants.userId)","ViewPostID":"\(seenPostsArray[indexPath.row].postID!)"] as [String:String]
            
            WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
                guard let StatusCode = JSON["StatusCode"].int else{
                    return
                }
              
                guard let Message = JSON["Message"].string else{
                    return
                }
                if(StatusCode == 200)
                {
                }
                if(StatusCode == 500)
                {
                }
            }
            
            let postDetails = self.seenPostsArray[indexPath.row].relatedPostsID
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
            viewController.postDetails = postDetails!
            self.present(viewController, animated: true, completion: nil)
        }
        else if(indexPath.section == 2)
        {
            let url = "\(Constants.k_Webservice_URL)/viewpost"
            let params = ["ViewUserID":"\(Constants.userId)","ViewPostID":"\(locationsArray[indexPath.row].postID!)"] as [String:String]
            
            
            WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            print("CALLED \(JSON)")
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
            }
            if(StatusCode == 500)
            {
            }
            }
            
            let postDetails = self.locationsArray[indexPath.row].relatedPostsID
            let viewController = storyboard?.instantiateViewController(withIdentifier: "MapGoogleViewController") as! MapGoogleViewController
            //viewController.postDetails = postDetails!
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 20, height: 20)
//    }
    
}
extension PostsViewController:UIScrollViewDelegate{
    
func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    if(scrollView == tabelViewPosts){
        if(tabelViewPosts.contentOffset.y+tabelViewPosts.bounds.height >= tabelViewPosts.contentSize.height){
           // self.pageNumber = self.pageNumber + 1
           // self.getDataFromApi(pageNumber:self.pageNumber)
        }
    }
    }
    
}

extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
