/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct friendsContacts : Codable {
	let id : Int?
	let userName : String?
	let userFirstName : String?
	let userLastName : String?
	let userPicture : String?
	let status : String?
    let userFcmKey : String?
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case userName = "UserName"
		case userFirstName = "UserFirstName"
		case userLastName = "UserLastName"
		case userPicture = "UserPicture"
		case status = "Status"
        case userFcmKey = "UserFCMKey"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		userFirstName = try values.decodeIfPresent(String.self, forKey: .userFirstName)
        userLastName = try values.decodeIfPresent(String.self, forKey: .userLastName)
        userPicture = try values.decodeIfPresent(String.self, forKey: .userPicture)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        userFcmKey = try values.decodeIfPresent(String.self, forKey: .userFcmKey)
	}

}
