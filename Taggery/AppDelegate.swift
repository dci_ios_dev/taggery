//
//  AppDelegate.swift
//  Taggery
//
//  Created by Aravind Kumar on 06/03/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Cloudinary
import UserNotifications
import IQKeyboardManagerSwift
import Firebase
import GooglePlaces
import GoogleMaps

@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var fcmToken = String()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let imageView = UIImageView(image: UIImage(named: "BG.png")?.withRenderingMode(.alwaysOriginal))
        imageView.contentMode = .scaleAspectFill
        imageView.tag = 1000
        imageView.frame = (self.window?.bounds)!
        self.window?.addSubview(imageView)
        
        GMSPlacesClient.provideAPIKey("AIzaSyDr1I5pZDQP5FW7R_-Xnhh4y61LzfhTruU")
        GMSServices.provideAPIKey("AIzaSyDr1I5pZDQP5FW7R_-Xnhh4y61LzfhTruU")
        
//        let config = CLDConfiguration(cloudName: "divjnpbqd", secure: true)
//        var cloudinary = CLDCloudinary(configuration: config)
//
//        let url = cloudinary.createUrl().generate("image1.jpg")
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        FirebaseApp.configure()
        
        // [START add_token_refresh_observer]
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name(rawValue: "tokenRefresh"),
                                               object: nil)
        // [END add_token_refresh_observer]
        
        IQKeyboardManager.sharedManager().enable = true
        
        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
            print("from push")
        }
        
     
       
     
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let presentViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        self.window?.rootViewController = presentViewController
//        self.window?.makeKeyAndVisible()

        if (launchOptions != nil) {
            
            // For local Notification
            if let localNotificationInfo = launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as? UILocalNotification {
                
                if let something = localNotificationInfo.userInfo!["yourKey"] as? String {
                    
                }
                
                
            } else
                
                // For remote Notification
                if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as! [NSObject : AnyObject]? {
                    
            }
            
        }
        
        
        // Override point for customization after application launch.
        return true
    }
    
    //////FCM
    
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            let id = InstanceID.instanceID().token()
            print("InstanceID token: \(refreshedToken)")
            print("InstanceID token: \(String(describing: id))")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
      
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID2 : \(messageID)")
        }
        
        if let dict = userInfo as NSDictionary? as! [String:Any]? {
           
            if (dict["typeID"] as? String) != nil{
                let typeID = dict["typeID"] as? String
                if(typeID == "2"){
                    Constants.fromFCm = true
                    Constants.fcmKey = 2


                    
                }else if (typeID == "1"){
                    Constants.fromFCm = true
                     Constants.fcmKey = 1


                }
            }
        }

        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("FCM:",fcmToken)
        let defaults = UserDefaults.standard
        defaults.set(fcmToken, forKey: "FCMToken")
        Constants.userFcm = fcmToken

    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken as Data

        if let refreshedToken = InstanceID.instanceID().token() {
            fcmToken = "\(refreshedToken)"
            print("FCM:",fcmToken)
            let defaults = UserDefaults.standard
            defaults.set(refreshedToken, forKey: "FCMToken")
            
        }
        Constants.userFcm = fcmToken
        InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: InstanceIDAPNSTokenType.sandbox)
        let DeviceToken = UIDevice.current.identifierForVendor
        print(DeviceToken!)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
    }
    
    // This method will be called when app received push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    private func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        //Handle the notification
        
        completionHandler([.alert, .badge, .sound])
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

