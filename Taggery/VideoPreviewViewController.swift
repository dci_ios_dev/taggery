//
//  VideoPreviewViewController.swift
//  Taggery
//
//  Created by Gowsika on 10/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import AVKit
import Cloudinary

class VideoPreviewViewController: ViewController{

    @IBOutlet weak var mainview: UIView!
    
    public var mainurl : URL!
    
    var player: AVPlayer?
    var playerController : AVPlayerViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        player = AVPlayer(url: mainurl)
        playerController = AVPlayerViewController()
        
       // Utilities.setBackgroungImage(self.view)
        
        guard player != nil && playerController != nil else {
            return
        }
        playerController!.showsPlaybackControls = true
        
        playerController!.player = player!
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoPreviewViewController.finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)


        
       // self.mainview.addSubview(playerController!.view)
        playerController!.view.frame = self.mainview.bounds
        self.addChildViewController(playerController!)
        self.mainview.addSubview(playerController!.view)

       playerController?.didMove(toParentViewController: self)
        player?.play()
        
        // Do any additional setup after loading the view.
    }

    @objc func finishVideo(){
        
      //  self.player?.play()
        
    }
    @IBAction func dismissPreview(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func uploadButton(_ sender: Any) {
        LoadingIndicatorView.show()
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
        //    let request = cloudinary.createUploader().upload(url: imageUrl, uploadPreset: "Sample_preset")
        let uploader = cloudinary.createUploader()
        
        var publicName = "Vid-\(Date().toMillis()!)"
        print("IMG \(publicName)")
        let paramss = CLDUploadRequestParams().setFolder("Add_Post").setPublicId(publicName)
        paramss.setResourceType(.video)
        uploader.upload(url: self.mainurl, uploadPreset: "Sample_preset", params: paramss, progress: { progress in
            print("pogrsss \(progress)")
            
        }) { result,error in
                    LoadingIndicatorView.hide()
            if(error == nil)
            {
               let createpostController = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostController") as! CreatePostController
                createpostController.isimage = false
                
                let url = URL(string:(result?.url)!)
                createpostController.isvideourl = url
                self.navigationController?.pushViewController(createpostController, animated: true)
                
                print("MNE \(result?.url!)")
                
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
