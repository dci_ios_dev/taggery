//
//  FollowingViewController.swift
//  Taggery
//
//  Created by Gowsika on 27/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher
import ParallaxHeader
class followersCell:UITableViewCell{
    
    @IBOutlet weak var avatarImage: UIImageView!{
        didSet{
            avatarImage.layer.masksToBounds = false
            avatarImage.layer.borderColor = UIColor.black.cgColor
            avatarImage.layer.cornerRadius = avatarImage.frame.height/2
            avatarImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var followButton: UIButton!{
        didSet{
          
            followButton.layer.borderColor = UIColor.clear.cgColor
            followButton.layer.cornerRadius = 5
            
        }
    }
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    
}
// 1 -> Requested,2 -> following 3 -> follow

class FollowingViewController: UIViewController {

    @IBOutlet var searchView: UIView!
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var isFollowingslists = true
    var isSearchenable = false
    var followingsArray = [Followings]()
    var filteredArray = [Followings]()
    
    var followersArray = [Followers]()
    var filterFollowers = [Followers]()
    
    var pagenumber = 1
    var refreshControl = UIRefreshControl()

    
    @IBOutlet weak var followTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.setBackgroungImage(self.view)
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        //refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController

        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        searchField.delegate = self
        
       searchView.frame = CGRect(x:0,y:0,width:self.tableView.frame.width,height:50)
        
        self.tableView.parallaxHeader.view = searchView
        self.tableView.parallaxHeader.height = 0
        self.tableView.parallaxHeader.minimumHeight = 0
        self.tableView.parallaxHeader.mode = .topFill
        
        if(isFollowingslists){
            self.followTitle.text = "Following"
        }
        else{
            self.followTitle.text = "Followers"
        }
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        
      searchField.clearButtonMode = .always
        self.tableView.reloadData()
        self.getdataFromApi(page: pagenumber)
        // Do any additional setup after loading the view.
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.filteredArray.removeAll()
        self.filterFollowers.removeAll()
         self.getdataFromApi(page: 1)
    }

    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
            if gesture.direction == UISwipeGestureRecognizerDirection.right {
                print("Swipe Right Pick")
                self.navigationController?.popViewController(animated: true)

    
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.left {
                print("Swipe Left Orange")
    
    
    
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.up {
                print("Swipe Up")
    
    
    
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.down {
                print("Swipe Down")
            }
        }
    
    
    
    @IBAction func toggleSearch(_ sender: Any) {
        
        if(!isSearchenable){
            isSearchenable = true
            self.tableView.parallaxHeader.height = 50
            self.tableView.parallaxHeader.minimumHeight = 50
        }
        else
        {
            isSearchenable = false
            self.tableView.parallaxHeader.height = 0
            self.tableView.parallaxHeader.minimumHeight = 0
            
        }
        self.tableView.reloadData()
    }
    
    func getdataFromApi(page:Int){
        
        if(isFollowingslists){
            let params = ["user_id":"\(Constants.userId)","page":"\(page)"]
            let url = "\(Constants.k_Webservice_URL)/followings"
            Utilities.showLoading()
            WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
                let decoder = JSONDecoder()
                do{
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    print("DATA us \(Data)")
                    let datas = try decoder.decode(FollowingBase.self, from: Data)
                    
                    if(datas.statusCode! == 200){
                        self.followingsArray = datas.followings!
                        //self.filteredArray = datas.followings!
                        let follows = datas.followings!
                        if(follows.count > 0){
                            for follow in follows{
                                self.filteredArray.append(follow)
                            }
                        }
                        else{
                            if(self.filteredArray.count == 0){
                                self.tableView.setEmptyMessage("You Dont have Any followers")
                                
                            }
                        }
                        
                        DispatchQueue.main.async {
                            Utilities.hideLoading()
                            if(self.filteredArray.isEmpty){
                                self.tableView.setEmptyMessage("You Are not Following Anyone")
                            }
                            self.tableView.reloadData()
                            
                        }
                    }
                }
                catch{
                    Utilities.hideLoading()
                    print("ERROR \(error)")
                }
            }
        }
        else
        {
            let params = ["user_id":"\(Constants.userId)","page":"\(page)"]
            let url = "\(Constants.k_Webservice_URL)/followers"
            Utilities.showLoading()
            WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
                let decoder = JSONDecoder()
                do{
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    print("DATA us \(Data)")
                    let datas = try decoder.decode(FollowersBase.self, from: Data)
                    
                    if(datas.statusCode! == 200){
                        self.followersArray = datas.followers!
                        //self.filterFollowers = datas.followers!
                        let follows = datas.followers!
                        if(follows.count > 0){
                        for follow in follows{
                            self.filterFollowers.append(follow)
                            }
                        }
                        else{
                            if(self.filterFollowers.count == 0){
                                self.tableView.setEmptyMessage("You Dont have Any followers")

                            }
                        }
                        DispatchQueue.main.async {
                            Utilities.hideLoading()
                            self.tableView.reloadData()
                            
                        }
                    }
                }
                catch{
                    Utilities.hideLoading()
                    self.tableView.setEmptyMessage("You Dont have Any followers")
                    print("ERROR \(error)")
                }
            }
        }

        
    }
    
    @objc func showAllComments(_ sender: UITapGestureRecognizer){
        let tappedImage = sender.view as! UIImageView
        print("ME F \(tappedImage.tag)")
        
        var user_id = 0
        if(isFollowingslists){
            user_id = self.filteredArray[tappedImage.tag].id!
            var viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            viewController.followingUserId = "\(user_id)"
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }else{
            user_id = self.filterFollowers[tappedImage.tag].id!
            var viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
             viewController.followingUserId = "\(user_id)"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    @objc func follow(_ sender: UIButton?){
       
        if(isFollowingslists){
            let userid = self.filteredArray[sender!.tag].id!
            let followUrl = "\(Constants.k_Webservice_URL)/unfollow"
            let params = ["FollowerUserID":"\(userid)","FollowingUserID":"\(Constants.userId)"]
            
            
            let alert = UIAlertController(title: Constants.k_ApplicationName, message: "Unfollow", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Undo", style: .default, handler: { action in
                WebServiceHandler.sharedInstance.commonPostRequest(url: followUrl, params: params) { JSON in
                    guard let StatusCode = JSON["StatusCode"].int else{
                        return
                    }
                    guard let Message = JSON["Message"].string else{
                        return
                    }
                    if(StatusCode == 200)
                    {
                        self.filteredArray.removeAll()
                        Utilities.showAlertView("\(Message)", onView: self)
                        self.getdataFromApi(page: self.pagenumber)
                    }
                    if(StatusCode == 500)
                    {
                        // Utilities.showAlertView("\(Message)", onView: self)
                    }
                    
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action  in
                print("Cancel ")
            }))
            
            self.present(alert, animated: true) {
                
            }

            
//            WebServiceHandler.sharedInstance.commonPostRequest(url: followUrl, params: params) { JSON in
//                    guard let StatusCode = JSON["StatusCode"].int else{
//                        return
//                    }
//                    guard let Message = JSON["Message"].string else{
//                        return
//                    }
//                    if(StatusCode == 200)
//                    {
//                        Utilities.showAlertView("\(Message)", onView: self)
//                        self.getdataFromApi(page: self.pagenumber)
//                    }
//                    if(StatusCode == 500)
//                    {
//                        // Utilities.showAlertView("\(Message)", onView: self)
//                    }
            
          
                
            
        }else{
            let userid = self.filterFollowers[sender!.tag].id!
            print("user id \(userid)")
           
            let status = self.filterFollowers[sender!.tag].following!
            if(status == 2){
                 let followUrl = "\(Constants.k_Webservice_URL)/reqresponce"
                let params = ["FollowerUserID":"\(userid)","FollowingUserID":"\(Constants.userId)","RequestAcceptStatus":1] as [String : Any]
                WebServiceHandler.sharedInstance.commonPostRequest(url: followUrl, params: params) { JSON in
                    guard let StatusCode = JSON["StatusCode"].int else{
                        return
                    }
                    guard let Message = JSON["Message"].string else{
                        return
                    }
                    if(StatusCode == 200)
                    {
                          self.filterFollowers.removeAll()
                        Utilities.showAlertView("\(Message)", onView: self)
                        self.getdataFromApi(page: self.pagenumber)
                    }
                    if(StatusCode == 500)
                    {
                        // Utilities.showAlertView("\(Message)", onView: self)
                    }
                    
                   
                }
                
            }else if(status == 3){
                let alert = UIAlertController(title: Constants.k_ApplicationName, message: "Undo Your Request", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Undo", style: .default, handler: { action in
                    
                    let followUrl = "\(Constants.k_Webservice_URL)/follow"
                    let params = ["FollowerUserID":"\(userid)","FollowingUserID":"\(Constants.userId)"] as [String : Any]
                    WebServiceHandler.sharedInstance.commonPostRequest(url: followUrl, params: params) { JSON in
                        guard let StatusCode = JSON["StatusCode"].int else{
                            return
                        }
                        guard let Message = JSON["Message"].string else{
                            return
                        }
                        if(StatusCode == 200)
                        {
                            self.filterFollowers.removeAll()
                            Utilities.showAlertView("\(Message)", onView: self)
                            self.getdataFromApi(page: self.pagenumber)
                        }
                        if(StatusCode == 500)
                        {
                            // Utilities.showAlertView("\(Message)", onView: self)
                        }
                        
                        
                    }
                    
                    
                    
                    
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action  in
                     print("Cancel ")
                }))
                
                self.present(alert, animated: true) {
                    
                }
            }
        }
        
    }
    
    
    
   
}
extension FollowingViewController:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isFollowingslists){
            if(filteredArray.count == 0){
                self.tableView.setEmptyMessage("No Following")
            }
            else{
                self.tableView.setEmptyMessage("")
            }
           return filteredArray.count
        }
        else
        {
            if(filterFollowers.isEmpty){
                self.tableView.setEmptyMessage("No Followers")
            }
            else{
                self.tableView.setEmptyMessage("")
            }
            return filterFollowers.count
        }
        
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(tableView.contentOffset.y+tableView.bounds.height >= tableView.contentSize.height){
           // self.getdataFromApi(page:self.pagenumber+1)
        }
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let profileviewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//      //  self.navigationController?.pushViewController(profileviewController, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "followersCell", for: indexPath) as! followersCell
        cell.followButton.tag = indexPath.row
        cell.followButton.addTarget(self, action: #selector(follow(_:)), for: .touchUpInside)
        cell.avatarImage.isUserInteractionEnabled = true
        //cell.avatarImage.addGestureRecognizer(UITapGestureRecognizer)
        cell.avatarImage.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showAllComments(_:)))
        cell.avatarImage.addGestureRecognizer(tap)
        if(isFollowingslists){
            
            
            cell.userNameLabel.text = "\(self.filteredArray[indexPath.row].userFirstName ?? "")"
            cell.followButton.titleLabel?.text = "Following"
            cell.followButton.setTitle("Following", for: UIControlState.normal)
            cell.followButton.backgroundColor = UIColor.white
            cell.followButton.setTitleColor(Constants.primaryColor, for: UIControlState.normal)
            
            let usernewimage = "https://res.cloudinary.com/cloudtaggery18/Profile/\(self.filteredArray[indexPath.row].userPicture ?? "")"
            
            let processor = ResizingImageProcessor(referenceSize: CGSize(width:  cell.avatarImage.frame.width, height:  cell.avatarImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius:  cell.avatarImage.frame.height / 2)
            cell.avatarImage.kf.setImage(with: URL(string: usernewimage), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
            if(cell.userNameLabel.text == ""){
                cell.isHidden = true
            }
        }
        else
        
        {
            //            1->requested, 2->following, 3->follow

            cell.userNameLabel.text = "\(self.filterFollowers[indexPath.row].userFirstName ?? "")"
            print("TET \(self.filterFollowers[indexPath.row].following!)")
            if(self.filterFollowers[indexPath.row].following! == 2){
                 cell.followButton.titleLabel?.text = "follow"
                cell.followButton.setTitle("follow", for: UIControlState.normal)
                
                cell.followButton.backgroundColor = Constants.primaryColor
                cell.followButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            }
            else if(self.filterFollowers[indexPath.row].following! == 1){
               cell.followButton.titleLabel?.text = "Following"
                cell.followButton.setTitle("Following", for: UIControlState.normal)
                cell.followButton.backgroundColor = Constants.primaryColor
                cell.followButton.setTitleColor(UIColor.white, for: UIControlState.normal)

            }
            else
            {
                 cell.followButton.titleLabel?.text = "Requested"
                cell.followButton.setTitle("Requested", for: UIControlState.normal)
                cell.followButton.backgroundColor = UIColor.white
                cell.followButton.setTitleColor(Constants.primaryColor, for: UIControlState.normal)
            }
            let usernewimage = "https://res.cloudinary.com/cloudtaggery18/Profile/\(self.filterFollowers[indexPath.row].userPicture ?? "")"
            
            let processor = ResizingImageProcessor(referenceSize: CGSize(width:  cell.avatarImage.frame.width, height:  cell.avatarImage.frame.height)) >> RoundCornerImageProcessor(cornerRadius:  cell.avatarImage.frame.height / 2)
            cell.avatarImage.kf.setImage(with: URL(string: usernewimage), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
            
        }
        
       
        
        
        return cell
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)

        if((newString.count) > 2){
            
            if(isFollowingslists){
                
                self.filteredArray = self.followingsArray.filter {($0.userFirstName?.contains(newString))!}
                print("TEST \(newString)")
                print("Filtered Array count \(self.filteredArray.count)")
                self.tableView.reloadData()
            }
            else
            {
                self.filterFollowers = self.followersArray.filter {($0.userFirstName?.contains(newString))!}
                self.tableView.reloadData()
            }
           
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(isFollowingslists){
            self.filteredArray = self.followingsArray
            self.tableView.parallaxHeader.height = 0
            self.tableView.reloadData()
        }
        else
        {
            self.filterFollowers = self.followersArray
            self.tableView.parallaxHeader.height = 0
            self.tableView.reloadData()
        }
        
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(isFollowingslists){
            self.filteredArray = self.followingsArray
            self.tableView.parallaxHeader.height = 0
            self.tableView.reloadData()
        }
        else
        {
            self.filterFollowers = self.followersArray
            self.tableView.parallaxHeader.height = 0
            self.tableView.reloadData()
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if(isFollowingslists){
            self.filteredArray = self.followingsArray
            self.tableView.parallaxHeader.height = 0
            self.tableView.reloadData()
        }
        else
        {
            self.filterFollowers = self.followersArray
            self.tableView.parallaxHeader.height = 0
            self.tableView.reloadData()
        }
        return true
    }
    
    
}
