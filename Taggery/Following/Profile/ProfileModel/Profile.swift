/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Profile : Codable {
	let profileOwner : Int?
	let thosetwofriends : Int?
	let ourfriendrequestscount : Int?
	let userCoverPhoto : String?
	let userPicture : String?
	let userDescription : String?
	let userName : String?
	let userFirstName : String?
	let userLastName : String?
	let userGender : String?
	let userRelationshipStatus : String?
	let userProfileType : Int?
	let likes : Int?
	let followers : Int?
	let followings : Int?
	let posts : Int?
	let relatedPostsID : [RelatedPostsID]?

	enum CodingKeys: String, CodingKey {

		case profileOwner = "ProfileOwner"
		case thosetwofriends = "thosetwofriends"
		case ourfriendrequestscount = "ourfriendrequestscount"
		case userCoverPhoto = "UserCoverPhoto"
		case userPicture = "UserPicture"
		case userDescription = "UserDescription"
		case userName = "UserName"
		case userFirstName = "UserFirstName"
		case userLastName = "UserLastName"
		case userGender = "UserGender"
		case userRelationshipStatus = "UserRelationshipStatus"
		case userProfileType = "UserProfileType"
		case likes = "Likes"
		case followers = "Followers"
		case followings = "Followings"
		case posts = "Posts"
		case relatedPostsID = "RelatedPostsID"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        profileOwner = try values.decodeIfPresent(Int.self, forKey: .profileOwner)
		thosetwofriends = try values.decodeIfPresent(Int.self, forKey: .thosetwofriends)
		ourfriendrequestscount = try values.decodeIfPresent(Int.self, forKey: .ourfriendrequestscount)
        userCoverPhoto = try values.decodeIfPresent(String.self, forKey: .userCoverPhoto)
        userPicture = try values.decodeIfPresent(String.self, forKey: .userPicture)
        userDescription = try values.decodeIfPresent(String.self, forKey: .userDescription)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        userFirstName = try values.decodeIfPresent(String.self, forKey: .userFirstName)
        userLastName = try values.decodeIfPresent(String.self, forKey: .userLastName)
        userGender = try values.decodeIfPresent(String.self, forKey: .userGender)
        userRelationshipStatus = try values.decodeIfPresent(String.self, forKey: .userRelationshipStatus)
        userProfileType = try values.decodeIfPresent(Int.self, forKey: .userProfileType)
		likes = try values.decodeIfPresent(Int.self, forKey: .likes)
		followers = try values.decodeIfPresent(Int.self, forKey: .followers)
		followings = try values.decodeIfPresent(Int.self, forKey: .followings)
		posts = try values.decodeIfPresent(Int.self, forKey: .posts)
		relatedPostsID = try values.decodeIfPresent([RelatedPostsID].self, forKey: .relatedPostsID)
	}

}
