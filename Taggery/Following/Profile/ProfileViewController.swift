//
//  ProfileViewController.swift
//  Taggery
//
//  Created by Gowsika on 24/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher
import ParallaxHeader

class squareTableviewCell:UICollectionViewCell{
    
    @IBOutlet weak var squareImageView: UIImageView!
    @IBOutlet var playImage:UIImageView!

}
class roundedCollectionViewCell:UICollectionViewCell{
    
    @IBOutlet weak var roundImageView: UIImageView!
    
}

class ProfileViewController: UIViewController {



    @IBOutlet weak var analyticsClicked: UIButton!
    
    @IBOutlet weak var pencilLeftButton: UIButton!
    @IBOutlet weak var pencilRight: UIButton!
    
    @IBOutlet weak var sideMenuClick: UIButton!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var postsLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var aboutYouButon: UIButton!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var latestPostsCollectionView: UICollectionView!
    @IBOutlet weak var oldPostsCollectionView: UICollectionView!
    @IBOutlet weak var Scrollviews: UIScrollView!
    @IBOutlet weak var mainContent: UIView!
    @IBOutlet weak var userCoverPhoto: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var relationShipLabel: UILabel!
    
    var followingUserId = Constants.userId
    
    var isLoading = false
    
    
    @IBOutlet weak var userProfile: UIImageView!{
        didSet{
            userProfile.layer.borderWidth = 0
            userProfile.layer.masksToBounds = false
            userProfile.layer.borderColor = UIColor.black.cgColor
            userProfile.layer.cornerRadius = userProfile.frame.height/2
            userProfile.clipsToBounds = true
        }
    }
    @IBOutlet weak var imageViewHeightConstant: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    
    var after12HoursTop3array = [After12HrsTop3]()
    var after12HoursRemainingArray = [After12HrsRemaining]()
    var gridFlowLayout = GridCollectionLayout()

    var pageNumber : Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.latestPostsCollectionView.tag = 0
        self.oldPostsCollectionView.tag = 1
        self.latestPostsCollectionView.delegate = self
        self.latestPostsCollectionView.dataSource = self
        
        self.oldPostsCollectionView.delegate = self
        self.oldPostsCollectionView.dataSource = self
        self.oldPostsCollectionView.collectionViewLayout = gridFlowLayout
        
        let tapfollowing = UITapGestureRecognizer(target: self, action: #selector(self.showFollowers(_:)))
        let tapfollowers = UITapGestureRecognizer(target: self, action: #selector(self.showFollowing(_:)))

        self.followersLabel.isUserInteractionEnabled = true
        self.followersLabel.addGestureRecognizer(tapfollowers)
        
        
        self.followingLabel.isUserInteractionEnabled = true
        self.followingLabel.addGestureRecognizer(tapfollowing)
       
        if(Constants.userId != self.followingUserId){
            self.followingLabel.isUserInteractionEnabled = false
            self.followersLabel.isUserInteractionEnabled = false
            self.aboutYouButon.isUserInteractionEnabled = false
            self.pencilRight.isHidden = true
            self.pencilLeftButton.isHidden = true
            // self.aboutYouButon.bac
            self.analyticsClicked.isHidden = true
            self.sideMenuClick.isHidden = true
        }
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        
        
        Utilities.setBackgroungImage(self.view)
        
        self.headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.bounds.height / 1.5)
        self.imageViewHeightConstant.constant = self.headerView.frame.height / 2.5
        //self.mainContent.addSubview(headerView)
       // Utilities.setBackgroungImage(self.Scrollviews)
        self.Scrollviews.parallaxHeader.view = self.headerView
        self.Scrollviews.parallaxHeader.height = self.view.bounds.height / 2.0
        
        self.getDataofNewPosts(page:pageNumber)
        self.getRemainingData(page:pageNumber)
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    
    @objc func showFollowers(_ sender: UITapGestureRecognizer){
        
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "FollowingViewController") as! FollowingViewController
        leftViewController.isFollowingslists = true
        navigationController?.pushViewController(leftViewController, animated: false)
        
    }
    
    
    @objc func showFollowing(_ sender: UITapGestureRecognizer){
        
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "FollowingViewController") as! FollowingViewController
         leftViewController.isFollowingslists = false
        navigationController?.pushViewController(leftViewController, animated: false)
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func getRemainingData(page:Int){
        let params = ["viewer_id":"\(followingUserId)","page":"\(page)","owner_id":"\(Constants.userId)"] as [String:String]
        let url = "\(Constants.k_Webservice_URL)/profilepaginate"
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            self.isLoading = false
            
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(profileMain.self, from: Data)
                
                if(datas.statusCode! == 200){
                    // self.after12HoursTop3array = datas.after12HrsTop3!
                   // self.after12HoursRemainingArray = datas.after12HrsRemaining!
                    
                   
                    
                    if(datas.after12HrsRemaining!.count == 0){
                        self.latestPostsCollectionView.setEmptyMessage("No more posts Available")
                    }else{
                        for datas in datas.after12HrsRemaining!{
                            self.after12HoursRemainingArray.append(datas)
                        }
                        print("COUNt NOW iS \(self.after12HoursRemainingArray.count)")
                        self.latestPostsCollectionView.setEmptyMessage("")
                    }
                    DispatchQueue.main.async {
                        self.latestPostsCollectionView.reloadData()
                        self.oldPostsCollectionView.reloadData()
                    }
                   
                }
                else
                {
                    
                }
            }
            catch{
                print("ERROR \(error)")
            }
        }
        
    }
    
    
    
    func getDataofNewPosts(page:Int){
        let params = ["viewer_id":"\(followingUserId)","page":"\(page)","owner_id":"\(Constants.userId)"] as [String:String]
        let url = "\(Constants.k_Webservice_URL)/profile"
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            self.isLoading = false
            
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(profileMain.self, from: Data)
                 Utilities.hideLoading()
                if(datas.statusCode! == 200){
                   // self.after12HoursTop3array = datas.after12HrsTop3!
                    //self.after12HoursRemainingArray = datas.after12HrsRemaining!
                    self.after12HoursTop3array = datas.after12HrsTop3!
                    for datas in datas.after12HrsTop3!{
                        //self.after12HoursTop3array.append(datas)
                    }
                    let profiles = datas.profile!
                    DispatchQueue.main.async {
                       
                        self.likesLabel.text = "\(profiles.likes!)\nLikes"
                        self.followersLabel.text = "\(profiles.followers!)\nFollowers"
                        
                        
                        
                        self.followingLabel.text = "\(profiles.followings!)\nFollowing"
                        
                        self.postsLabel.text = "\(profiles.posts!)\nPost"
                        self.relationShipLabel.text = "\(profiles.userRelationshipStatus!)"
                        self.genderLabel.text = "\(profiles.userGender!)"
                        self.firstNameLabel.text = "\(profiles.userFirstName!)"
                        
                        //self.detailsLabel.text = "\(profiles.userFirstName!)\n\(profiles.userRelationshipStatus!)\n\(profiles.userGender!)"
                        self.aboutYouButon.titleLabel?.text = "\(profiles.userDescription!) "
                        if(profiles.userDescription == ""){
                           self.aboutYouButon.setTitle("Add about your Description", for: UIControlState.normal)
                        }
                        let myAttributewhite = [ NSAttributedStringKey.foregroundColor: UIColor.white]
                        let likesString = NSAttributedString(string: "\(profiles.userDescription! as? String ?? "" )", attributes: myAttributewhite)
                        self.aboutYouButon.setAttributedTitle(likesString, for: UIControlState.normal)
                        
                        
                        self.nameLabel.text = "\(profiles.userName!)"
                        
                        let profileurl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(profiles.userCoverPhoto as? String ?? "")"
                        self.userCoverPhoto.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                        let usernewimage = "https://res.cloudinary.com/cloudtaggery18/Profile/\(profiles.userPicture! as? String ?? "" )"
                        let processor = ResizingImageProcessor(referenceSize: CGSize(width: self.userProfile.frame.width, height: self.userProfile.frame.height)) >> RoundCornerImageProcessor(cornerRadius: self.userProfile.frame.height / 2)
                        self.userProfile.kf.setImage(with: URL(string: usernewimage), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
                        self.latestPostsCollectionView.reloadData()
                        self.oldPostsCollectionView.reloadData()
                        
                        if(self.after12HoursTop3array.isEmpty){
                            self.oldPostsCollectionView.setEmptyMessage("No Posts Available")
                        }else{
                            self.oldPostsCollectionView.setEmptyMessage("")
                        }
                    }
                }
                else
                {
                  
                }
                
                
            }
            catch{
                print("ERROR \(error)")
            }
        }
    }
    
    
    
    @IBAction func analyticsClicked(_ sender: Any) {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyAnalyticsViewController") as! MyAnalyticsViewController
        
        navigationController?.pushViewController(leftViewController, animated: false)
        
        
    }
    @IBAction func menuClicked(_ sender: Any) {
        
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        navigationController?.pushViewController(leftViewController, animated: false)
        
    }
    
    @IBAction func aboutYouCLicked(_ sender: Any) {
      
        let editProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        navigationController?.pushViewController(editProfileViewController, animated: false)
        
    }
}

extension ProfileViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(scrollView.tag == 1){
            
            if(self.isLoading == false){
                self.isLoading = true
                if(latestPostsCollectionView.contentOffset.y+latestPostsCollectionView.bounds.height >= latestPostsCollectionView.contentSize.height){
                    self.pageNumber = self.pageNumber + 1
                    self.getRemainingData(page: self.pageNumber)
                    print("Called \(self.pageNumber)")
                }
            }
            
    }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView.tag == 0){
            
            let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewProfilePostsController") as! ViewProfilePostsController
            leftViewController.selectedPostId = "\(self.after12HoursTop3array[indexPath.row].postID!)"
            navigationController?.pushViewController(leftViewController, animated: false)
            
        }
        else
        {
            let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewProfilePostsController") as! ViewProfilePostsController
            leftViewController.selectedPostId = "\(self.after12HoursRemainingArray[indexPath.row].postID!)"
           
            navigationController?.pushViewController(leftViewController, animated: false)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == 0){
            return self.after12HoursTop3array.count
        }
        else
        {
            print("Remaining \(self.after12HoursRemainingArray.count)")
            return self.after12HoursRemainingArray.count
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if(collectionView.tag == 0){
         let collectionView = collectionView.dequeueReusableCell(withReuseIdentifier: "roundedCollectionViewCell", for: indexPath) as! roundedCollectionViewCell
            collectionView.layer.borderWidth = 1
            collectionView.layer.masksToBounds = false
            collectionView.layer.borderColor = UIColor.black.cgColor
            collectionView.layer.cornerRadius = collectionView.frame.height/2
            collectionView.roundImageView.layer.borderWidth = 0
            collectionView.roundImageView.layer.masksToBounds = false
            collectionView.roundImageView.layer.borderColor = UIColor.black.cgColor
            collectionView.roundImageView.layer.cornerRadius = collectionView.frame.height/2
            collectionView.clipsToBounds = true
            
            let profileurl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(self.after12HoursTop3array[indexPath.row].postImage!)"
            collectionView.roundImageView.backgroundColor = UIColor.clear
            let processor = ResizingImageProcessor(referenceSize: CGSize(width: collectionView.roundImageView.frame.width, height: collectionView.roundImageView.frame.height)) >> RoundCornerImageProcessor(cornerRadius: collectionView.roundImageView.frame.height / 2)
            collectionView.roundImageView.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "placeHolder.jpg"), options: [.processor(processor)])
            collectionView.roundImageView.contentMode = .scaleAspectFill
            collectionView.roundImageView.clipsToBounds = true
            
            
            return collectionView
        }
        if(collectionView.tag == 1){
            let collectionView = collectionView.dequeueReusableCell(withReuseIdentifier: "squareTableviewCell", for: indexPath) as! squareTableviewCell
            
//            if(self.after12HoursRemainingArray[indexPath.row].postImage! == ""){
//               collectionView.squareImageView.image = UIImage(named: "placeHolder.jpg")
//
//            } else {
            
           
            if(self.after12HoursRemainingArray[indexPath.row].postMediaType! == 1 || self.after12HoursRemainingArray[indexPath.row].postMediaType! == 0){
                let profileurl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(self.after12HoursRemainingArray[indexPath.row].postImage!)"
                collectionView.squareImageView.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                collectionView.playImage.isHidden = true
            }else{
                let profileurl = "\(Constants.taggeryVideoUrl)\(self.after12HoursRemainingArray[indexPath.row].postImage!).jpg"
                print("Video \(profileurl)")
                collectionView.squareImageView.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                collectionView.playImage.isHidden = false
            }
            
//            }
            
            collectionView.squareImageView.contentMode = .scaleAspectFill
            collectionView.clipsToBounds = true
            return collectionView
        }

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if(collectionView.tag == 0){
            return ( self.view.frame.width - 240 ) / 3
        } else {
            return 1
        }
    }
    
    
}
