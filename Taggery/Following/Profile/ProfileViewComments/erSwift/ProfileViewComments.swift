//
//  ViewController.swift
//  Taggery
//
//  Created by Gowsika on 26/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher

class customViewCommentsCell:UITableViewCell{
    
    @IBOutlet weak var userAvatar: UIImageView!{
        didSet{
            userAvatar.layer.masksToBounds = false
            userAvatar.layer.borderColor = UIColor.black.cgColor
            userAvatar.layer.cornerRadius = userAvatar.frame.height/2
            userAvatar.clipsToBounds = true
        }
    }
    @IBOutlet weak var userText: UILabel!{
        didSet{
        
        }
    }
    @IBOutlet weak var borderView: UIView!{
        didSet{
            borderView.layer.borderColor = UIColor.white.cgColor
            borderView.layer.cornerRadius = 10
            borderView.layer.borderWidth = 1.0
        }
    }
}

class ProfileViewComments: UIViewController {

    @IBOutlet weak var commentsTableView: UITableView!
    
    @IBOutlet weak var shareCommentsTextView: UITextView!{
        didSet{
            shareCommentsTextView.layer.borderColor = UIColor.white.cgColor
            shareCommentsTextView.layer.cornerRadius = 10
            shareCommentsTextView.layer.borderWidth = 1.0
        }
    }
    var commentsArray = [Comments]()
    var selectedPostId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shareCommentsTextView.text = "Add comment.."
        shareCommentsTextView.delegate = self
        
        self.commentsTableView.delegate = self
        self.commentsTableView.dataSource = self
        commentsTableView.rowHeight = UITableViewAutomaticDimension
        commentsTableView.estimatedRowHeight = 44.0
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
       
    }
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    

    @IBAction func sendComments(_ sender: Any) {
        if(self.shareCommentsTextView.text!.count == 0 || self.shareCommentsTextView.text.contains("Add comment..")){
            
            return
        }
        Utilities.showLoading()
        let params = ["CommentUserID":"\(Constants.userId)","CommentPostID":"\(selectedPostId)","CommentText":"\(self.shareCommentsTextView.text!)"] as [String:String]
        
        
        let url = "\(Constants.k_Webservice_URL)/addcomments"
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            Utilities.hideLoading()
            print("RESP \(JSON)")
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                self.shareCommentsTextView.text = "Add comment.."
                self.getDatafromApi()
            }
            if(StatusCode == 500)
            {
                
            }
        }
    }
    
    func getDatafromApi(){
        
       
       // self.commentsArray.removeAll()
        
        let params = ["user_id":"\(Constants.userId)","PostID":"\(selectedPostId)"]
        let url = "\(Constants.k_Webservice_URL)/getpostdetails"
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Json4Swift_Base.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                self.commentsArray = datas.post!.comments!
                self.commentsTableView.reloadData()
                }
                DispatchQueue.main.async {
                    Utilities.hideLoading()
                    //     self.tabelViewPosts.reloadData()
                    
                }

            }
            catch{
                print("ERROR \(error)")
            }
        }
        
        
    }
    

}
extension ProfileViewComments:UITableViewDelegate,UITableViewDataSource,UITextViewDelegate{
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func textViewDidChange(_ textView: UITextView) {
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text.contains("Add comment..")){
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.count < 1){
            self.shareCommentsTextView.attributedText = Utilities.attributedPlaceholder(text: "Add comment..")
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let colorGray = Utilities.hexStringToUIColor(hex: "#91919191")

        let cell = tableView.dequeueReusableCell(withIdentifier: "customViewCommentsCell", for: indexPath) as! customViewCommentsCell
        let viewsAttributedString = NSMutableAttributedString()

        if(Constants.userId == "\(self.commentsArray[indexPath.row].commentUserID!)"){
            
         var timeText = " - \(self.commentsArray[indexPath.row].commentCreatedAt!)"
         var wordstoRemove = "before"
            if let range = timeText.range(of: wordstoRemove) {
                timeText.removeSubrange(range)
            }
          
           let myAttributename = [NSAttributedStringKey.foregroundColor:UIColor.white,.font : UIFont.systemFont(ofSize: 17.0, weight: .regular)]
            let nameText = NSAttributedString(string: "You", attributes: myAttributename)
            let hourTextAttrb = [ NSAttributedStringKey.foregroundColor: UIColor.lightGray,.font :UIFont.systemFont(ofSize: 15.0, weight: .regular)]
            let hourText = NSAttributedString(string: " \(timeText)", attributes: hourTextAttrb)
            
            let wordsText = NSAttributedString(string: "\n\(commentsArray[indexPath.row].commentText!)", attributes: myAttributename)
            
            viewsAttributedString.append(nameText)
            viewsAttributedString.append(hourText)
            viewsAttributedString.append(wordsText)

        
        }
        else
        {
            var timeText = " - \(self.commentsArray[indexPath.row].commentCreatedAt!)"
            var wordstoRemove = "before"
            if let range = timeText.range(of: wordstoRemove) {
                timeText.removeSubrange(range)
            }
            let myAttributename = [NSAttributedStringKey.foregroundColor:UIColor.white,.font : UIFont.systemFont(ofSize: 17.0, weight: .regular)]
            let nameText = NSAttributedString(string: "\(commentsArray[indexPath.row].commentUserName!)", attributes: myAttributename)
            let hourTextAttrb = [ NSAttributedStringKey.foregroundColor: UIColor.lightGray,.font :UIFont.systemFont(ofSize: 14.0, weight: .regular)]
            let hourText = NSAttributedString(string: "\(timeText)", attributes: hourTextAttrb)
            
            let wordsText = NSAttributedString(string: "\n\(commentsArray[indexPath.row].commentText!)", attributes: myAttributename)
            
            viewsAttributedString.append(nameText)
            viewsAttributedString.append(hourText)
            viewsAttributedString.append(wordsText)

        }
        
        cell.userText.attributedText = viewsAttributedString
        
        let profileurl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(self.commentsArray[indexPath.row].commentPicture!)"
       cell.userAvatar.backgroundColor = UIColor.clear
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.userAvatar.frame.width, height: cell.userAvatar.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.userAvatar.frame.height / 2)
        cell.userAvatar.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}
