//
//  ViewProfilePostsController.swift
//  Taggery
//
//  Created by Gowsika on 26/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Kingfisher
import SwiftyJSON
import AVKit
class ViewProfilePostsController: UIViewController {

    @IBOutlet weak var userLastEnteredComments: UILabel!
    @IBOutlet weak var commentViews: UITextView!{
        didSet{
            commentViews.layer.masksToBounds = false
            commentViews.layer.borderColor = UIColor.white.cgColor
            commentViews.layer.cornerRadius = self.commentViews.frame.height / 2
            commentViews.layer.borderWidth = 1.5
            commentViews.clipsToBounds = true
            commentViews.backgroundColor = UIColor.clear
        }
        
    }
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerVideoView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var seenLabels: UILabel!
    
    @IBOutlet weak var scrollViewContent: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var flagButton: UIButton!
    
    @IBOutlet weak var heightCOnstant: NSLayoutConstraint!
    var selectedPostId = ""
    @IBOutlet weak var postTitleLabel: UILabel!
    
    @IBOutlet weak var postDurationLabel: UILabel!
    
    @IBOutlet weak var addLikesLabel: UILabel!
    
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var postUserAvatar: UIImageView!{
        didSet{
            postUserAvatar.layer.borderWidth = 0
            postUserAvatar.layer.masksToBounds = false
            postUserAvatar.layer.borderColor = UIColor.white.cgColor
            postUserAvatar.layer.cornerRadius = postUserAvatar.frame.height/2
            postUserAvatar.clipsToBounds = true
        }
        
    }
    @IBOutlet weak var postUserNameDetails: UILabel!
    
    @IBOutlet weak var viewCommentsLabel: UILabel!
    
    var viewedArray = [Viewed]()
    var postData : Post!
    var ilikes = false
    var friendLikesid = 0
    
    var likesArray = [Viewed]()
    var commentsArray = [Comments]()
    
    var reportstatus = 2
    
    var likedStatus = 2
    
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showAllComments(_:)))

        self.viewCommentsLabel.isUserInteractionEnabled = true
        self.viewCommentsLabel.addGestureRecognizer(tap)
        Utilities.setBackgroungImage(self.view)
        self.headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 2)

        self.footerView.frame = CGRect(x:0,y:self.view.frame.height / 2,width:self.view.frame.width,height:self.view.frame.height/2)

        
        self.commentViews.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        
        self.scrollViewContent.addSubview(headerView)
        self.scrollViewContent.addSubview(footerView)
        self.commentViews.delegate = self
        self.commentViews.attributedText = Utilities.attributedPlaceholder(text: "Add comment..")
        self.getDatafromApi()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
    }
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
        
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    @IBAction func addCommentAction(_ sender: Any) {
       
       
        if(self.commentViews.text!.count == 0 || self.commentViews.text.contains("Add comment..")){
            
            return
        }
         Utilities.showLoading()
        let params = ["CommentUserID":"\(Constants.userId)","CommentPostID":"\(selectedPostId)","CommentText":"\(self.commentViews.text!)"] as [String:String]
        
        
        let url = "\(Constants.k_Webservice_URL)/addcomments"
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            Utilities.hideLoading()
            print("RESP \(JSON)")
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                //self.commentViews.attributedText = ""
                self.commentViews.text = ""
                self.getDatafromApi()
            }
            if(StatusCode == 500)
            {
                
            }
        }
        
    
    }
    
    @IBAction func flagButtonClicked(_ sender: Any) {
       
        Utilities.showLoading()
        if(postData != nil){
            
            if(postData.reportStatus! == 2){
                reportstatus = 1
            }
            else
            {
                reportstatus = 2
            }
            
        }
        let params = ["user_id":"\(Constants.userId)","PostID":"\(selectedPostId)","ReportStatus":"\(reportstatus)"] as [String:String]
        let url = "\(Constants.k_Webservice_URL)/report"
        
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            
            Utilities.hideLoading()
            
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                self.getDatafromApi()
            }
            if(StatusCode == 500)
            {
                
            }
        }
    }
    
    func getDatafromApi(){
       
        self.likesArray.removeAll()
        self.viewedArray.removeAll()
        self.commentsArray.removeAll()
        
        let params = ["user_id":"\(Constants.userId)","PostID":"\(selectedPostId)"]
        let url = "\(Constants.k_Webservice_URL)/getpostdetails"
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Json4Swift_Base.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                
                    self.postData = datas.post!
                    self.locationLabel.font = UIFont.fontAwesome(ofSize: 18.0, style: .solid)
                    let attributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 18.0, style: .solid)]
                    let fontAwesomeText = NSAttributedString(string: "\(String.fontAwesomeIcon(name: .mapMarkerAlt))", attributes: attributes)
                    let myAttributeName = [ NSAttributedStringKey.foregroundColor: UIColor.white,.font :UIFont.systemFont(ofSize: 18.0, weight: .semibold)]
                    let nameText = NSAttributedString(string: " \(datas.post!.postLocation!)", attributes: myAttributeName)
                    
                    let viewsAttributedString = NSMutableAttributedString()
                    viewsAttributedString.append(fontAwesomeText)
                    viewsAttributedString.append(nameText)
                    
                    self.locationLabel.attributedText = viewsAttributedString
                    
                    self.seenLabels.font = UIFont.fontAwesome(ofSize: 18.0, style: .solid)
                    let attributesSeen = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 18.0, style: .solid)]
                    
                    let fontAwesomeTextSeen = NSAttributedString(string: "\(String.fontAwesomeIcon(name: .eye))", attributes: attributesSeen)
                    let myAttributeNameSeen = [ NSAttributedStringKey.foregroundColor: UIColor.white,.font :UIFont.systemFont(ofSize: 18.0, weight: .semibold)]
                    let nameTextSeen = NSAttributedString(string: " \(datas.post!.viewscount!)", attributes: myAttributeNameSeen)
                    
                    let viewsAttributedStringSeen = NSMutableAttributedString()
                    viewsAttributedStringSeen.append(fontAwesomeTextSeen)
                    viewsAttributedStringSeen.append(nameTextSeen)
                    
                    self.seenLabels.attributedText = viewsAttributedStringSeen

                    
                    let profileurl = "https://res.cloudinary.com/cloudtaggery18/Add_Post/\(self.postData.postImage!)"

                   self.headerImage.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
                    //self.locationLabel.attributedText = viewsAttributedString
                    self.postTitleLabel.text = "\(datas.post!.postText!)"
                    self.postDurationLabel.text = "\(datas.post!.postcreateddate!)"
                    self.viewedArray = (datas.post!.viewed)!
                    self.commentsArray = (datas.post!.comments)!
                    
                    print("me is \(datas.post!.reportStatus!)")
                    if(datas.post!.reportStatus! == 2){

                        
                        self.flagButton.setBackgroundImage(UIImage(named: "flagwhite.png"), for: .normal)
                    }
                    else
                    {
                        
                      self.flagButton.setBackgroundImage(UIImage(named: "flagred.png"), for: .normal)
                    }
                    self.updateui()
                }
                else
                {
                     Utilities.hideLoading()
                }
                
                //  self.suggestionsarray.removeAll()
                
                //  self.locationsArray = datas.location!
                // self.postsArray = datas.posts!
                
                DispatchQueue.main.async {
                    Utilities.hideLoading()
                    //     self.tabelViewPosts.reloadData()
                    
                }
                
            }
            catch{
                print("ERROR \(error)")
            }
        }
        
        
    }
   @objc func showAllComments(_ sender: UITapGestureRecognizer){
        
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewComments") as! ProfileViewComments
        leftViewController.commentsArray = self.commentsArray
        leftViewController.selectedPostId = "\(self.selectedPostId)"
        navigationController?.pushViewController(leftViewController, animated: false)
        
    }
    
    func updateui(){

        if(self.postData.postMediaType! == 1){
             self.headerVideoView.isHidden = true
        }else{
            self.headerImage.isHidden = true
            self.headerVideoView.isHidden = false
            let videoUrl = URL(string:"\(Constants.taggeryVideoUrl)\(self.postData.postImage!)")
            
            player = AVPlayer(url: videoUrl!)
            playerController = AVPlayerViewController()
            guard player != nil && playerController != nil else {
                return
            }
            playerController!.showsPlaybackControls = true
            playerController!.player = player!
            playerController!.view.frame = self.headerVideoView.bounds
            self.addChildViewController(playerController!)
            headerVideoView.addSubview(playerController!.view)
            playerController?.didMove(toParentViewController: self)
            
        }
        
        
        if(commentsArray.count > 0){
            if(commentsArray.count == 1){
            self.viewCommentsLabel.isHidden = true
            }
            self.postUserAvatar.isHidden = false
            self.postUserNameDetails.isHidden = false
            self.viewCommentsLabel.isHidden = false
            
            self.viewCommentsLabel.text = "view all \(commentsArray.count) comments"
           // var attributesStringUsername = ""
           
            let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.lightGray,.font :UIFont.systemFont(ofSize: 14.0)]
            let minutes = NSAttributedString(string: " - \(commentsArray[commentsArray.count - 1].commentCreatedAt!)", attributes: myAttribute)
            let viewsAttributedString = NSMutableAttributedString()
            let myAttributeName = [ NSAttributedStringKey.foregroundColor: UIColor.white,.font :UIFont.systemFont(ofSize: 16.0, weight: .semibold)]
            let nameText = NSAttributedString(string: "\(commentsArray[commentsArray.count - 1].commentUserFirstName!)", attributes: myAttributeName)
            let myAttributeCommenText = [NSAttributedStringKey.foregroundColor:UIColor.white,.font : UIFont.systemFont(ofSize: 13.0, weight: .regular)]
            let commentText = NSAttributedString(string: "\n \(commentsArray[commentsArray.count - 1].commentText!)", attributes: myAttributeCommenText)
            
            
            viewsAttributedString.append(nameText)
            viewsAttributedString.append(minutes)
          //  viewsAttributedString.append(commentText)
            
            self.userLastEnteredComments.text = "\(commentsArray[commentsArray.count - 1].commentText!)"
            self.postUserNameDetails.attributedText = viewsAttributedString
            
            
            let profileurl = "https://res.cloudinary.com/cloudtaggery18/Profile/\(self.commentsArray[commentsArray.count - 1].commentPicture!)"
            
            let processor = ResizingImageProcessor(referenceSize: CGSize(width: self.postUserAvatar.frame.width, height: self.postUserAvatar.frame.height)) >> RoundCornerImageProcessor(cornerRadius: self.postUserAvatar.frame.height / 2)
            self.postUserAvatar.backgroundColor = UIColor.clear
            
            self.postUserAvatar.kf.setImage(with: URL(string: profileurl), placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])

            print("POst Image Type \("")")
//            if(self.postData.postType! == 1){
//                self.headerVideoView.isHidden = true
//            }

        }
        else
        {
           self.postUserAvatar.isHidden = true
            self.postUserNameDetails.isHidden = true
           self.viewCommentsLabel.isHidden = true
        }

        print("Viewed Array count is \(viewedArray.count)")

        if(viewedArray.count == 0){
            self.addLikesLabel.text = "Add your like"
            if(postData.likes! == 1){
                self.addLikesLabel.text = "liked by you"
                self.likesButton.setBackgroundImage(UIImage(named: "heartred.png"), for: .normal)
            }
        }
        else if(viewedArray.count > 0)
        {
            for (key,index) in viewedArray.enumerated(){
                if(index.likes == 1){
                    if(Constants.userId == "\(index.id)"){
                    }
                    else
                    {
                       likesArray.append(index)
                    }
                }
            }
        }
        if(likesArray.count > 0){
            if(postData.likes! == 1){
                self.addLikesLabel.text = "liked by you and \(likesArray.count) other(s)"
                self.likesButton.setBackgroundImage(UIImage(named: "heartred.png"), for: .normal)
            }
            else
            {
                self.addLikesLabel.text = "Add your Like and \(likesArray.count) other(s) liked"
                self.likesButton.setBackgroundImage(UIImage(named: "heartwhite.png"), for: .normal)
            }
            
            if(likesArray.count == 2){
                if(postData.likes! == 1){
                    self.addLikesLabel.text = "liked by you and \(likesArray[likesArray.count - 1].userName!)"
                    self.likesButton.setBackgroundImage(UIImage(named: "heartred.png"), for: .normal)
                }
                
            }
            if(likesArray.count > 2){
                if(postData.likes! == 1){
                    self.addLikesLabel.text = "liked by you and \(likesArray.count) others"
                    self.likesButton.setBackgroundImage(UIImage(named: "heartred.png"), for: .normal)
                }
                else
                {
                    self.addLikesLabel.text = "liked by \(likesArray.count) others"
                    self.likesButton.setBackgroundImage(UIImage(named: "heartred.png"), for: .normal)
                }
            }
        }
        else {
            if(postData.likes! == 2) {
                self.addLikesLabel.text = "Add your Like"
                self.likesButton.setBackgroundImage(UIImage(named: "heartwhite.png"), for: .normal)
            } else if(postData.likes! == 1){
                self.addLikesLabel.text = "liked by you"
                self.likesButton.setBackgroundImage(UIImage(named: "heartred.png"), for: .normal)
            }
        }
    }

    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addLike(_ sender:Any){
        if(postData.likes! == 1){
            likedStatus = 2
        }
        else
        {
            likedStatus = 1
        }
        
        let params = ["LikeUserID":"\(Constants.userId)","LikePostID":"\(selectedPostId)","LikeStatus":"\(likedStatus)"]
        let url = "\(Constants.k_Webservice_URL)/addlike"
       // Utilities.showLoading()
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            
         //   Utilities.hideLoading()
            
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                self.getDatafromApi()
            }
            if(StatusCode == 500)
            {
                
            }
        }
    }
    
}

extension ViewProfilePostsController:UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {

    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.attributedText == Utilities.attributedPlaceholder(text: "Add comment..")){
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.count < 1){
        self.commentViews.attributedText = Utilities.attributedPlaceholder(text: "Add comment..")

        }
      heightCOnstant.constant = self.viewCommentsLabel.frame.origin.y + 5
    }
    
}
