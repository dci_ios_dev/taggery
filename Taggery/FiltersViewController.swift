//
//  FiltersViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 07/03/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Cloudinary
import Kingfisher

class FiltersViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var filterImageViewOutlet: UIImageView!
    @IBOutlet weak var filtersCollectionView: UICollectionView!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    
    
    var newfield = UITextField()
    
    var textfieldorgin = CGPoint(x:0,y:0)
    
    var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CISepiaTone"
    ]
    
    var artFilters = ["art:athena","art:red_rock","art:fes","art:zorro","art:eucalyptus","art:peacock","art:ukulele","art:primavera","art:daguerre","art:audrey","art:incognito"]
    
    
   // var imageName = String()
    //var cloudImageUrl = String()
    var imageUrl:URL!
    var fetchedUrl:URL!
    var fetchedImageName = String()
    var selectedImage = UIImage()
    var selectedImageData = Data()
    var publicName = ""
    
    
    var newFilterimage = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(textFieldDrag(pan:)))
       
        self.filtersCollectionView.isHidden = false
        
        LoadingIndicatorView.show("Loading")
        
        filtersCollectionView.delegate = self
        filtersCollectionView.dataSource = self
        
        filtersCollectionView.backgroundColor = UIColor.clear
        
        Utilities.setBackgroungImage(self.view)
        
       // cloudinary.createUrl().generate("sample.jpg")
        
        // returns: http://res.cloudinary.com/demo/image/upload/sample.jpg
        self.loadCloudFilters()
        
        // TESTiNg CGRECT MAKE for DYNAMIC VIEW
        
        self.newfield.frame =  CGRect(x: 50, y: 50, width: 80, height: 80)
       // self.newfield.translatesAutoresizingMaskIntoConstraints = fa
       
        self.newfield.addGestureRecognizer(gesture)
        
        self.filterImageViewOutlet.addSubview(newfield)
         self.newfield.addGestureRecognizer(gesture)
    }
    
    @objc func textFieldDrag(pan: UIPanGestureRecognizer) {
        print("Being Dragged")
        if pan.state == .began {
            print("panIF")
            textfieldorgin = pan.location(in: newfield)
        }else {
            print("panELSE")
            let location = pan.location(in: view) // get pan location
            newfield.frame.origin = CGPoint(x: location.x - textfieldorgin.x, y: location.y - textfieldorgin.y)
        }
    }
    
    
    func loadCloudFilters() {
        
        filterImageViewOutlet.image = UIImage(data:selectedImageData,scale:1.0)
        
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
    
         publicName = "IMG-\(Date().toMillis()!)"
                print("IMG \(publicName)")
        

        let params = CLDUploadRequestParams().setFolder("Add_Post").setPublicId(publicName)
        params.setResourceType(.image)
        //    let request = cloudinary.createUploader().upload(url: imageUrl, uploadPreset: "Sample_preset")
        let uploader = cloudinary.createUploader()
       
        uploader.upload(data: selectedImageData, uploadPreset: "Sample_preset", params: params, progress: { Progress in
            
        }) { result,error in
             LoadingIndicatorView.hide()
            if(error == nil){
                print("Result \(result)")
                let fetch = result?.url
                 self.fetchedUrl = URL(string: fetch!)
                print("FETCH URL is \(self.fetchedUrl)")
                self.fetchedImageName = "Add_Post/\(self.fetchedUrl.lastPathComponent)"
                print("IMAGE NAME IS\(self.fetchedImageName)")
                self.filtersCollectionView.reloadData()
                }
            else
            {
                 print("Error \(error)")

                //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! PostsViewController
                //                self.navigationController?.pushViewController(vc, animated: true)
            }

        }
        
//
//        uploader.upload(data: selectedImageData, uploadPreset: "sample_preset")
//        { result, error in
//            print("ME \(error)")
//            LoadingIndicatorView.hide()
//            if(error == nil){
//                let fetch = result?.url
//
//                self.fetchedUrl = URL(string: fetch!)
//                self.fetchedImageName = self.fetchedUrl.lastPathComponent
//                print("\(self.fetchedImageName)")
//                self.filtersCollectionView.reloadData()
//
//            }
//            else
//            {
////                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! PostsViewController
////                self.navigationController?.pushViewController(vc, animated: true)
//            }
//
//
//
//
//        }
        
//        uploader.upload(url: imageUrl, uploadPreset: "Sample_preset") { result, error in
//
//            let fetch = result?.url
//            print(fetch!)
//            self.fetchedUrl = URL(string: fetch!)
//            self.fetchedImageName = self.fetchedUrl.lastPathComponent
//            print("\(self.fetchedImageName)")
//
//        }
        //self.filtersCollectionView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: false)
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        
        LoadingIndicatorView.show()
        newFilterimage = filterImageViewOutlet.image!
        let imageData = UIImageJPEGRepresentation(newFilterimage, 1.0)
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
        
        publicName = "IMGFilter-\(Date().toMillis()!)"
        print("IMG \(publicName)")
        
        
        let params = CLDUploadRequestParams().setFolder("Add_Post").setPublicId(publicName)
        params.setResourceType(.image)
        //    let request = cloudinary.createUploader().upload(url: imageUrl, uploadPreset: "Sample_preset")
        let uploader = cloudinary.createUploader()
        
        uploader.upload(data: imageData!, uploadPreset: "Sample_preset", params: params, progress: { Progress in
            
        }) { result,error in
            LoadingIndicatorView.hide()
            if(error == nil){
                print("Result \(result)")
                let fetch = result?.url
                self.fetchedUrl = URL(string: fetch!)
                print("FETCH URL is \(self.fetchedUrl!)")
                self.fetchedImageName = "Add_Post/\(self.fetchedUrl.lastPathComponent)"
               
                let createpostController = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostController") as! CreatePostController
                createpostController.selectedimage = self.newFilterimage
                createpostController.selectedImageData = imageData!
                createpostController.imageURL = self.fetchedUrl
                createpostController.isimage = true
                createpostController.nameFromCloudinary = "\(self.fetchedUrl!.lastPathComponent)"
                self.navigationController?.pushViewController(createpostController, animated: false)
            }
            else
            {
               print("Result \(error)")
                
            }
            
        }
   
    }
    @IBAction func editTextBtnAction(_ sender: Any) {
    }
    @IBAction func tagUserBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController") as! TagUserViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func selectLocationBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func deleteBtnAction(_ sender: Any) {
        
       self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - CollectionView Delegate Methods
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artFilters.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell:UICollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as UICollectionViewCell?)!
        
        let filterImageView = cell.viewWithTag(1) as! UIImageView
        let filterTitleLabel = cell.viewWithTag(2) as! UILabel
        
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            
             print("FETCHED URL \(self.fetchedImageName)")
            
            DispatchQueue.main.async {
                
               
                
            filterImageView.cldSetImage(cloudinary.createUrl().setTransformation(CLDTransformation().setEffect(self.artFilters[indexPath.row])).generate(self.fetchedImageName)!, cloudinary: cloudinary)
                
                var titleString = self.artFilters[indexPath.row]
                let range1 = titleString.characters.index(titleString.startIndex, offsetBy: 4)..<titleString.endIndex
                titleString = String(titleString[range1])
                filterTitleLabel.text = titleString
                //filterImageView.image = imageForButton
                //filterImageView.layer.cornerRadius = 5
                filterImageView.layer.masksToBounds = true;
                filterImageView.clipsToBounds = false
            }
        }
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
//        let ciContext = CIContext(options: nil)
//        let originalImage = UIImage(named:"Aravind.jpg")
//        let coreImage = CIImage(image: originalImage!)
//        let filter = CIFilter(name: "\(CIFilterNames[indexPath.row])" )
//        filter!.setDefaults()
//        filter!.setValue(coreImage, forKey: kCIInputImageKey)
//        let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
//        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
//        let imageForButton:UIImage = UIImage.init(cgImage: filteredImageRef!)
        
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
    filterImageViewOutlet.cldSetImage(cloudinary.createUrl().setTransformation(CLDTransformation().setEffect(artFilters[indexPath.row])).generate("\(fetchedImageName)")!, cloudinary: cloudinary)
        
        
        
        
        
        
        
        
        
        //filterImageViewOutlet.image = imageForButton
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width / 4 - 10, height: collectionView.frame.size.height)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
