//
//  TagUserViewController.swift
//  Taggery
//
//  Created by Aravind on 27/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SDWebImage
import Kingfisher
import ParallaxHeader
class TagUserViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var userSearchField: UITextField!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var serachTextField: UITextField!
    var isSearchenable = false
    
    var filterFriendList = [friendsContacts]()
    var friendsList = [friendsContacts]()
    
    var tagFriends = [friendsContacts]()
    
    var selectedFriends = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        userSearchField.delegate = self
        userSearchField.clearButtonMode = .always
        searchView.frame = CGRect(x: 0, y: 0, width: self.tableViewOutlet.frame.width, height: 50)
        
        self.tableViewOutlet.parallaxHeader.view = searchView
        self.tableViewOutlet.parallaxHeader.height = 0
        self.tableViewOutlet.parallaxHeader.minimumHeight = 0
        self.tableViewOutlet.parallaxHeader.mode = .topFill
       Constants.tagFriends.removeAll()
        print("Counts \(Constants.tagFriends.count)")
        getfriendsList()
        Utilities.setBackgroungImage(self.view)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
           self.navigationController?.popViewController(animated: true)
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            
          
            
        }
    }

    func getfriendsList(){
        
        var url = "\(Constants.k_Webservice_URL)friendslist"
        let params = ["user_id":"\(Constants.userId)"] as [String:String]
        Utilities.showLoading()

        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
             Utilities.hideLoading()
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(friendsBase.self, from: Data)
                self.filterFriendList = datas.suggestion!
                self.friendsList = datas.suggestion!
                self.tableViewOutlet.reloadData()
            }
            catch{
               
                print("ERROR \(error)")
            }
        }
        
        
    }
    
    @IBAction func searchClicked(_ sender: Any) {
        print("Toggle Clicked");
        
        if(!isSearchenable) {
            isSearchenable = true
            self.tableViewOutlet.parallaxHeader.height = 50
            self.tableViewOutlet.parallaxHeader.minimumHeight = 50
        } else {
            isSearchenable = false
            self.tableViewOutlet.parallaxHeader.height = 0
            self.tableViewOutlet.parallaxHeader.minimumHeight = 0
        }
        self.tableViewOutlet.reloadData()
    }
    
    @IBAction func tagUsersClicked(_ sender: Any) {
       
      let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostController") as! CreatePostController!
       
       Constants.tagFriends = self.tagFriends
       //viewController?.TableViews.reloadData()
        self.navigationController?.popViewController(animated: true)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return friendsList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableViewOutlet.dequeueReusableCell(withIdentifier: "Cell");
        if((cell?.isSelected)!){
            cell?.backgroundColor = UIColor.clear
            cell?.contentView.backgroundColor = UIColor.clear
        }
        let namelabel:UILabel = cell!.viewWithTag(2) as! UILabel
        let imageView:UIImageView = cell!.viewWithTag(1) as! UIImageView

        namelabel.text = friendsList[indexPath.row].userFirstName!
        imageView.layer.cornerRadius = imageView.frame.size.height / 2;
        imageView.clipsToBounds = true;

        let profilePicUrl = "\(Constants.taggeryProfileUrl)\(friendsList[indexPath.row].userPicture!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: imageView.frame.width, height: imageView.frame.height)) >> RoundCornerImageProcessor(cornerRadius: imageView.frame.height / 2)
        
        imageView.kf.setImage(with: URL(string: profilePicUrl),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])

        
        
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableViewOutlet.dequeueReusableCell(withIdentifier: "Cell");
        cell?.backgroundColor = UIColor.clear
        
        cell?.contentView.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tagFriends.append(friendsList[indexPath.row])
        
        let cell = tableViewOutlet.dequeueReusableCell(withIdentifier: "Cell");
        cell?.selectedBackgroundView?.backgroundColor = UIColor.clear
        cell?.accessoryType = .checkmark

        
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableViewOutlet.dequeueReusableCell(withIdentifier: "Cell");
        cell?.selectedBackgroundView?.backgroundColor = UIColor.clear
        cell?.accessoryType = .none
        for(index,value) in tagFriends.enumerated(){
            if(value.id! == friendsList[indexPath.row].id!){
                tagFriends.remove(at: index)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 80
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.friendsList = self.filterFriendList
        self.tableViewOutlet.parallaxHeader.height = 0
        self.tableViewOutlet.reloadData()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.friendsList = self.filterFriendList
        self.tableViewOutlet.parallaxHeader.height = 0
        self.tableViewOutlet.reloadData()
        return true
    }
    
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if((newString.count) > 2){
            self.friendsList = self.filterFriendList.filter {($0.userFirstName?.localizedCaseInsensitiveContains(newString))!}
            print("Test \(newString)")
            print("Filtered array count \(self.filterFriendList.count)")
            self.tableViewOutlet.reloadData()
        }
    
        return true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
