//
//  MapViewController.swift
//  Taggery
//
//  Created by Gowsika on 28/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import Kingfisher
class locationcell:UICollectionViewCell{
    
    @IBOutlet weak var postImage: UIImageView!
    
}


class MapGoogleViewController: UIViewController {

    @IBOutlet var mapView: UIView!
    var isPageRefreshing = false
    var page = 1
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.backgroundColor = UIColor.clear
        }
    }
    var mapViews:GMSMapView!
    var locationarrays = [Location]()
    var gridFlowLayout = GridCollectionLayout()

    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.setBackgroungImage(self.view)
        
        let camera = GMSCameraPosition.camera(withLatitude: Constants.UserLatitude, longitude: Constants.UserLongitude, zoom: 8)

        
        
        mapViews = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height / 2 - 20), camera: camera)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Constants.UserLatitude, longitude: Constants.UserLongitude)
      
        print("Lati is \(Constants.UserLatitude) Long is \(Constants.UserLongitude)")
        marker.map = mapViews
        mapViews.isMyLocationEnabled = true
        
        self.collectionView.parallaxHeader.view = mapViews
        self.collectionView.parallaxHeader.height = self.view.bounds.height / 2
        self.collectionView.parallaxHeader.minimumHeight = 0
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        gridFlowLayout.itemHeight = 80
        self.collectionView.collectionViewLayout = gridFlowLayout
       
        self.mapViews.animate(to: camera)
       
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        self.getApi(page: page)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    
    func getApi(page:Int){
        isPageRefreshing = true
        let params = ["lat":"\(Constants.UserLatitude)","lng":"\(Constants.UserLongitude)","page":"\(page)"]
        let url = "\(Constants.k_Webservice_URL)/locationlist"
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            self.isPageRefreshing = false
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Locations_Base.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                   self.locationarrays = datas.location!
                    for locationdata in datas.location!{
                        self.locationarrays.append(locationdata)
                    }
                    
                    self.collectionView.reloadData()
                }
                
                //  self.suggestionsarray.removeAll()
                
                //  self.locationsArray = datas.location!
                // self.postsArray = datas.posts!
                
                DispatchQueue.main.async {
                    //     self.tabelViewPosts.reloadData()
                    
                }
                
            }
            catch{
                print("ERROR \(error)")
            }
        }
        
        
    }
}
extension MapGoogleViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.locationarrays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "locationcell", for: indexPath) as! locationcell
        let picUrl = "\(Constants.taggeryPostUrl)\(self.locationarrays[indexPath.row].postImage!)"
        
        //cell.postImage.sd_setImage(with: URL(string: picUrl), placeholderImage: UIImage(named: "placeHolder.jpg"))
        cell.postImage.kf.setImage(with: URL(string: picUrl), placeholder: UIImage(named: "placeHolder.jpg"), options: nil)
        cell.postImage.contentMode = .scaleAspectFill
        cell.postImage.clipsToBounds = true

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewProfilePostsController") as! ViewProfilePostsController
        leftViewController.selectedPostId = "\(self.locationarrays[indexPath.row].postID!)"
        navigationController?.pushViewController(leftViewController, animated: false)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
         if(isPageRefreshing==false) {
            if(self.collectionView.contentOffset.y >= (self.collectionView.contentSize.height - self.collectionView.bounds.size.height)) {
                print("OKKK  \(page)")
                isPageRefreshing=true
                page = page + 1
                self.getApi(page: page)
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if(indexPath.row == self.locationarrays.count - 1){
           // page = page + 1
            //self.getApi(page: page)
        }
    }

   
}

    
    

