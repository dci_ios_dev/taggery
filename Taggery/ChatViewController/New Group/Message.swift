//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class Message {
    
    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var timestamp: Int
    var isRead: Bool
    var image: UIImage?
    private var toID: String?
    private var fromID: String?
    
    //MARK: Methods
    class func downloadAllMessages(forUserID: String, completion: @escaping (NSArray) -> Swift.Void) {
        
            Database.database().reference().child("ONE_TO_ONE_CHAT_MESSAGES").child(forUserID).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                 //   let data = snapshot.value as! [String: Any]
                    let data = snapshot.value as! NSDictionary
                    let arrayV = data.allValues as NSArray
                    print("Array Value of data \(arrayV)")
                    print("Data v\(data.count)")
                    completion(arrayV)
                    
//                    let location = data["location"]!
//                    Database.database().reference().child("conversations").child(location as! String).observe(.childAdded, with: { (snap) in
//                        if snap.exists() {
//                            let receivedMessage = snap.value as! [String: Any]
//                            let messageType = receivedMessage["type"] as! String
//                            var type = MessageType.text
//                            switch messageType {
//                                case "photo":
//                                type = .photo
//                                case "location":
//                                type = .location
//                            default: break
//                            }
//                            let content = receivedMessage["content"] as! String
//                            let fromID = receivedMessage["fromID"] as! String
//                            let timestamp = receivedMessage["timestamp"] as! Int
////                            if fromID == currentUserID {
////                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true)
////                                completion(message)
////                            } else {
////                                let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true)
////                                completion(message)
////                            }
//                        }
//                    })
                }
            })
        
    }
    
    func downloadImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
        if self.type == .photo {
            let imageLink = self.content as! String
            let imageURL = URL.init(string: imageLink)
            URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, response, error) in
                if error == nil {
                    self.image = UIImage.init(data: data!)
                    completion(true, indexpathRow)
                }
            }).resume()
        }
    }
    
    class func markMessagesRead(forUserID: String)  {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    Database.database().reference().child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            for item in snap.children {
                                let receivedMessage = (item as! DataSnapshot).value as! [String: Any]
                                let fromID = receivedMessage["fromID"] as! String
                                if fromID != currentUserID {
                                    Database.database().reference().child("conversations").child(location).child((item as! DataSnapshot).key).child("isRead").setValue(true)
                                }
                            }
                        }
                    })
                }
            })
        }
    }
   
    func downloadLastMessage(forLocation: String, completion: @escaping () -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("conversations").child(forLocation).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    for snap in snapshot.children {
                        let receivedMessage = (snap as! DataSnapshot).value as! [String: Any]
                        self.content = receivedMessage["content"]!
                        self.timestamp = receivedMessage["timestamp"] as! Int
                        let messageType = receivedMessage["type"] as! String
                        let fromID = receivedMessage["fromID"] as! String
                        self.isRead = receivedMessage["isRead"] as! Bool
                        var type = MessageType.text
                        switch messageType {
                        case "text":
                            type = .text
                        case "photo":
                            type = .photo
                        case "location":
                            type = .location
                        default: break
                        }
                        self.type = type
                        if currentUserID == fromID {
                            self.owner = .receiver
                        } else {
                            self.owner = .sender
                        }
                        completion()
                    }
                }
            })
        }
    }
    
    class func sendMessage(message: MessageFIR, fromID:String, toID: String, completion: @escaping (Bool) -> Swift.Void){
        var sentMessage = message
        var receiveMessage = message
        let receiverDict = receiveMessage.receiverDictionaryRepresentation
        let messageDict = sentMessage.dictionaryRepresentation
        //After uploadcurrentMessage implement the inbox update and Notification update
        // Note for receiver inbox use dict.receiverDictionaryRepresentation
        // if sender is 60 mean send value as sentMessage.dictionaryRepresentation and receiver is 67 mean send Value as sentMessage.receiverDictionaryRepresentation
        
        
        if let image = message.imageUrl as? UIImage{
            
            let currentDateTime = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyyMMddHH:mm"
            let currendDateTimeString = formatter.string(from: currentDateTime)
            
            let filePath = "messagePics/currentUserId\(currendDateTimeString)" // changeCurrenUserID as currently logged user id
            let imageRef = Storage.storage().reference().child(filePath)
            
            uploadImage(image, at: imageRef) { (downloadURL) in
                guard let downloadURL = downloadURL else {
                    print("Download url not found or error to upload")
                    return completion(false)
                }
                print("Image DownloadURL \(downloadURL.absoluteString)")
                completion(true)
                //                message.setValue(downloadURL.absoluteString, forKey: "imageUrl")
                sentMessage.imageUrl = downloadURL.absoluteString
                
                messageDict.setValue(downloadURL.absoluteString, forKey: "imageUrl")
                receiverDict.setValue(downloadURL.absoluteString, forKey: "imageUrl")
                Message.uploadCurrenMessage(withValues: messageDict , toId: "\(fromID)to\(toID)") { (status) in
                    completion(status)
                }
                Message.uploadCurrenMessage(withValues: messageDict , toId: "\(toID)to\(fromID)") { (status) in
                    completion(status)
                }
                Message.addInboxMessage(withValues: messageDict, fromId: fromID, toId: toID, completion: { (status) in
                    completion(status)
                })
                receiverDict.setValue(0, forKey: "readStatus")
                Message.addInboxMessage(withValues: receiverDict, fromId: toID, toId: fromID, completion: { (status) in
                    completion(status)
                })
                Message.sendNotification(withValues: receiverDict, fromId: fromID, toId: toID, completion: { (status) in
                    completion(status)
                })
            }
            //           let imageData = UIImageJPEGRepresentation((image ), 0.5)
            
            // print("MW \(Storage.storage().reference().)")
            
            /*  Storage.storage().reference().parent()?.putData(imageData!, metadata: nil, completion: { (metadata, error) in
             if error == nil {
             let path = metadata?.downloadURL()?.absoluteString
             print("image uploaded Path \(String(describing: path))")
             //                    let values = ["type": "photo", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false] as [String : Any]
             //                    Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
             //                        completion(status)
             //})
             } else {
             print("Image upload Error")
             }
             
             })*/
            
            //            // Create a root reference
            //            let storageRef = Storage.storage().reference()
            //
            //            // Create a reference to "mountains.jpg"
            //            let mountainsRef = storageRef.child("mountains.jpg")
            //
            //            // Create a reference to 'images/mountains.jpg'
            //            let mountainImagesRef = storageRef.child("images/mountains.jpg")
            //
            //            // While the file names are the same, the references point to different files
            //            mountainsRef.name == mountainImagesRef.name;            // true
            //            mountainsRef.fullPath == mountainImagesRef.fullPath;    // false
            //
            //            mountainsRef.putData(<#T##uploadData: Data##Data#>)
            
            
            
            //            let storageRef = Storage.storage().reference()
            //            let imageRef = storageRef.child("mount.jpg")
            //            let mountaingImageRef = storageRef.child("images/mount.jpg")
            //
            //            imageRef.name = mountaingImageRef.name
            //            imageRef.fullPath = mountaingImageRef.fullPath
            /* let met = StorageMetadata()
             met.contentType = "image/jpeg"
             Storage.storage().reference().child("messagePics/message").putData(imageData!, metadata: met, completion: { (metadata, error) in
             if error == nil {
             let path = metadata?.downloadURL()?.absoluteString
             print("image uploaded Path \(String(describing: path))")
             //                    let values = ["type": "photo", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false] as [String : Any]
             //                    Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
             //                        completion(status)
             //})
             }
             }) */
        } else {
            Message.uploadCurrenMessage(withValues: messageDict , toId: "\(fromID)to\(toID)") { (status) in
                completion(status)
            }
            Message.uploadCurrenMessage(withValues: messageDict , toId: "\(toID)to\(fromID)") { (status) in
                completion(status)
            }
            Message.addInboxMessage(withValues: messageDict, fromId: fromID, toId: toID, completion: { (status) in
                completion(status)
            })
            
            Message.addInboxMessage(withValues: receiverDict, fromId: toID, toId: fromID, completion: { (status) in
                completion(status)
            })
            receiverDict.setValue(0, forKey: "readStatus")
            Message.sendNotification(withValues: receiverDict, fromId: fromID, toId: toID, completion: { (status) in
                completion(status)
            })
            
        }
        
    }
    
    class func uploadImage(_ image: UIImage, at reference: StorageReference, completion: @escaping (URL?) -> Void){
        guard let imageData = UIImageJPEGRepresentation(image, 0.1) else {
            return completion(nil)
            
        }
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        reference.putData(imageData, metadata: metaData, completion: { (metadata, error) in
            if let error = error {
                assertionFailure(error.localizedDescription)
                print("Upload failed :: ",error.localizedDescription)
                return completion(nil)
            }
            completion(metadata?.downloadURL())
        })
    }

    class func send(message: Message, toID: String, completion: @escaping (Bool) -> Swift.Void)  {
        if let currentUserID = Auth.auth().currentUser?.uid {
            switch message.type {
            case .location:
                let values = ["type": "location", "content": message.content, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false]
                Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
                    completion(status)
                })
            case .photo:
                let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 0.5)
                
                
                let child = UUID().uuidString
                Storage.storage().reference().child("messagePics").child(child).putData(imageData!, metadata: nil, completion: { (metadata, error) in
                    if error == nil {
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["type": "photo", "content": path!, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false] as [String : Any]
                        Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
                            completion(status)
                        })
                    }
                })
            case .text:
                let values = ["type": "text", "content": message.content, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false]
                Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
                    completion(status)
                })
            }
        }
    }
    
    class func uploadCurrenMessage(withValues: NSDictionary, toId: String, completion: @escaping (Bool) -> Swift.Void){
        Database.database().reference().child("ONE_TO_ONE_CHAT_MESSAGES").child(toId).childByAutoId().setValue(withValues) { (error, _) in
            if error == nil {
                completion(true)
            } else {
                completion(false)
            }
        }
        
    }
    
    class func addInboxMessage(withValues: NSDictionary, fromId: String, toId: String, completion: @escaping (Bool) -> Swift.Void){
        Database.database().reference().child("RECENT_CHAT_MESSAGES").child("Inbox").child(fromId).child(toId).setValue(withValues) { (error, _) in
            if error == nil {
                completion(true)
            } else {
                completion(false)
            }
        }
        
    }
    class func sendNotification(withValues: NSDictionary, fromId: String, toId: String, completion: @escaping (Bool) -> Swift.Void){
        Database.database().reference().child("RECENT_CHAT_MESSAGES").child("Notifications").childByAutoId().setValue(withValues) { (error, _) in
            if error == nil {
                completion(true)
            } else {
                completion(false)
            }
        }
        
    }
    
    class func uploadMessage(withValues: [String: Any], toID: String, completion: @escaping (Bool) -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(toID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    Database.database().reference().child("conversations").child(location).childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
                        if error == nil {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                } else {
                    Database.database().reference().child("conversations").childByAutoId().childByAutoId().setValue(withValues, withCompletionBlock: { (error, reference) in
                        let data = ["location": reference.parent!.key]
                        Database.database().reference().child("users").child(currentUserID).child("conversations").child(toID).updateChildValues(data)
                        Database.database().reference().child("users").child(toID).child("conversations").child(currentUserID).updateChildValues(data)
                        completion(true)
                    })
                }
            })
        }
    }
    
    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int, isRead: Bool) {
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
    }
}
