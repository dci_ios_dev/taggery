//
//  ChatDetailViewController.swift
//  Taggery
//
//  Created by Aravind on 26/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher
import CodableFirebase


class ChatDetailViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,UITextViewDelegate {
    @IBAction func closePreview(_ sender: Any) {
        imageViewSelected.isHidden = true
        self.imageUpload = nil
    }
    var chatMessageArray = [ChatMessage]()
    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var buttonCloseImage: UIButton!
    @IBOutlet weak var imageViewSelected: UIView!
    @IBOutlet weak var labelChannelName: UILabel!
    var imagePicker = UIImagePickerController()
    var chatArray = NSArray()
    var fromDict = NSDictionary()
    var imageUpload : UIImage?
 //   let senderId = 67
    let testSenderId = Constants.userId
    var testReceiverId = 40
    @IBOutlet weak var senderImageIcon: UIImageView!
    @IBOutlet weak var textViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var commentView: UITextView!{
        didSet{
            commentView.layer.cornerRadius = 5.0
            commentView.layer.borderColor = UIColor.white.cgColor
            commentView.layer.borderWidth = 1.0
            commentView.backgroundColor = UIColor.clear
            commentView.textColor = UIColor.white
        }
    }
    let placeholderText = "Type your message"
    let height = [80,80,0,0,100,450]
    let timeStr = "Nov 23 - 01.40PM"
    let message = ["Wonder the game if you want to do it, it will clear all the credits","Wonder the game if you want to do it, it will clear all the credits","Hai","Hai","Wonder the ladnlkads ieok klwlupw went to","Wonder the ladnlkads ieok klwlupw went to,Wonder the ladnlkads ieok klwlupw went to,Wonder the ladnlkads ieok klwlupw went to,Wonder the ladnlkads ieok klwlupw went to"]
   
    
    @IBOutlet weak var tableViewChat: UITableView!{
        didSet{
            
            tableViewChat.estimatedRowHeight = 61
            tableViewChat.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imageViewSelected.isHidden = true
        testReceiverId = fromDict.value(forKey: "chatWithID") as! Int
        print("from Dict \(fromDict)")
        Utilities.setBackgroungImage(self.view)
        commentView.text = placeholderText
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
       // "\(senderId)to\(fromDict.value(forKey: "chatWithID")!)"
        Message.downloadAllMessages(forUserID: "\(testSenderId)to\(testReceiverId)", completion: {[weak weakSelf = self] (message) in
            print("message received \(message)")
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let d : Data = NSKeyedArchiver.archivedData(withRootObject: message)
                let firebaseDecoder = FirebaseDecoder()
                self.chatMessageArray.removeAll()
                for index in message{
                    let datas = try firebaseDecoder.decode(ChatMessage.self, from: index)
                    self.chatMessageArray.append(datas)
                }
             
                print("chatmessagearray \(self.chatMessageArray)")
                let sortedArrays = self.chatMessageArray.sorted(by: { $0.sentTime! < $1.sentTime!})
                print("F V \(sortedArrays)")
                self.chatMessageArray = sortedArrays
                
            }
            catch{
                print("Error 1\(error)")
            }
//            self.chatArray = message
//            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "sentTime", ascending: true)
//            let sortedResults: NSArray = self.chatArray.sortedArray(using: [descriptor]) as NSArray
//
//            self.chatArray = sortedResults
            if(self.chatMessageArray.count > 0){
                self.tableViewChat.reloadData()
                self.tableViewChat.scrollToBottom(animated: true)
            }
            
//            print("Donwload \((message.object(at: 0) as? NSDictionary)? ["name"] as! String)")
//            print("Chat detail Array \(self.chatArray)")

        })
        let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(fromDict.value(forKey: "photoUrl") as! String)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: senderImageIcon.frame.width, height: senderImageIcon.frame.height)) >> RoundCornerImageProcessor(cornerRadius: senderImageIcon.frame.height / 2)
        senderImageIcon.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        senderImageIcon.layer.cornerRadius = senderImageIcon.frame.height / 2
        senderImageIcon.clipsToBounds = true
        labelChannelName.text = fromDict.value(forKey: "toName") as? String
        // Do any additional setup after loading the view.
    }
    
     @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            let transition = CATransition()
            transition.duration = 0.40
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func downloadMessage(){
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == commentView){
            if(textView.text == placeholderText){
                textView.text = ""
            } else {
                
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let calcHeight = textView.sizeThatFits(textView.frame.size).height
        
        if(calcHeight >= 100) {
            
            textViewHeightConstant.constant = CGFloat(UILayoutPriority(rawValue: 100).rawValue)
            textViewHeightConstant.priority = UILayoutPriority(rawValue: 999)
            textView.isScrollEnabled  = true
        } else if (calcHeight <= 32 ){
            textViewHeightConstant.constant = CGFloat(UILayoutPriority(rawValue: 32).rawValue)
           // textView.sizeToFit()
            textView.isScrollEnabled  = false
        }
            else {
            textViewHeightConstant.priority = UILayoutPriority(rawValue: 250)
            textViewHeightConstant.constant = CGFloat(UILayoutPriority(rawValue: Float(calcHeight)).rawValue)
            //textView.sizeToFit()
            textView.isScrollEnabled  = false
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView == commentView) {
            if(textView.text.count == 0) {
                textView.text = placeholderText
            }else {
                
            }
        }
    }
    @IBAction func actionPostTapped(_ sender: Any) {
        if(commentView.text == placeholderText || commentView.text.count == 0) {
            print("Enter Valide Message")
        }else{
            
            print("Successfully posted")
          //  let a = UIImage()
            let currentDateTime = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
           // formatter.dateFormat = "dd-MMM-yyyy HH:mm:ssZ"
            let currendDateTimeString = formatter.string(from: currentDateTime)
            var messageData = [MessageFIR]()
            messageData.append(MessageFIR(chatWithID: fromDict.value(forKey: "chatWithID") as! Int, imageText: self.imageUpload == nil ? "" : commentView.text, imageURL: "a", name: fromDict.value(forKey: "name") as! String, photoURL: fromDict.value(forKey: "photoUrl") as! String, readStatus: 1, receiverFcmToken: fromDict.value(forKey: "receiverFcmToken") as! String, receiverPhotoURL: fromDict.value(forKey: "receiverPhotoUrl") as! String, receiverRoleName: "", receiverUniqueID: "", roleName: "", senderUserID: Int(testSenderId)!, sentTime: currendDateTimeString, toName: fromDict.value(forKey: "toName") as! String, uniqueID: "", userFCMKey: "", text: self.imageUpload == nil ? commentView.text : ""))
           // messageData[0].chatWithID = 7
            
          //  let myDict: NSDictionary = ["chat":"wonder","info":"Information"]
           
          /*  let messageDict = NSMutableDictionary()
            messageDict.setValue("h", forKey: "chatWithID")
            messageDict.setValue("h", forKey: "name")
            messageDict.setValue("h", forKey: "photoUrl")
            messageDict.setValue("h", forKey: "readStatus")
            messageDict.setValue("h", forKey: "receiverFcmToken")
            messageDict.setValue("h", forKey: "receiverPhotoUrl")
            messageDict.setValue("h", forKey: "receiverRoleName")
            messageDict.setValue("h", forKey: "receiverUniqueID")
            messageDict.setValue("h", forKey: "roleName")
            messageDict.setValue("h", forKey: "senderUserID")
            messageDict.setValue("h", forKey: "sentTime")
          //  messageDict.setValue("", forKey: "text")
            messageDict.setValue("h", forKey: "toName")
            messageDict.setValue("h", forKey: "uniqueID")
            messageDict.setValue("h", forKey: "userFCMKey")*/
            var type = ""
            
            if(self.imageUpload != nil){
                type = "1"
            }
            if(type == "1"){
                messageData[0].imageText = commentView.text
                messageData[0].imageUrl = self.imageUpload
                messageData[0].text = ""
//                messageDict.setValue("good", forKey: "imageText")
//                messageDict.setValue(self.imageUpload, forKey: "imageUrl")
            }else if(type == "2"){
               // messageDict.setValue("", forKey: "imageText")
                messageData[0].imageUrl = "h"
                //messageDict.setValue("h", forKey: "imageUrl")
            }else{
                messageData[0].imageText = ""
                messageData[0].imageUrl = ""
                messageData[0].text = commentView.text
                //messageDict.setValue("h", forKey: "text")
            }
            self.imageViewSelected.isHidden = true
            print("MessageDAta \(messageData)")
            Message.sendMessage(message: messageData[0], fromID: "\(testSenderId)", toID: "\(testReceiverId)") { (status) in
                self.imageUpload = nil
                self.commentView.text = ""
                print("Message status\(status)")
            }
        }
    }
    @IBAction func actionCameraTapped(_ sender: Any) {
        let alertcontroller = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        var cameraButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        var galleryButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        var cancelButton = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertcontroller.dismiss(animated: false, completion: nil)
        }
        
        alertcontroller.addAction(cancelActionButton)
     //   alertcontroller.addAction(cancelButton)
        alertcontroller.addAction(cameraButton)
        alertcontroller.addAction(galleryButton)
        self.navigationController!.present(alertcontroller, animated: true, completion: {
            alertcontroller.view.superview?.isUserInteractionEnabled = true
            alertcontroller.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionBackTapped(_ sender: Any) {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        let transition = CATransition()
        transition.duration = 0.40
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(leftViewController, animated: false)
    }
    
    //MARK: - Tableview Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTableViewCell", for: indexPath) as! ReceiverTableViewCell
        cell.selectionStyle = .none
        let sendCell = tableView.dequeueReusableCell(withIdentifier: "SenderTableViewCell", for: indexPath) as! SenderTableViewCell
      //  let chatDict = self.chatArray.object(at: indexPath.row) as? NSDictionary
        
       // let senderId = ((self.chatArray.object(at: 0) as? NSDictionary)? ["senderUserID"] as! Int)
        sendCell.selectionStyle = .none
        
        if(chatMessageArray[indexPath.row].senderUserID == Int(testSenderId)!) {
            if(chatMessageArray[indexPath.row].text == nil || chatMessageArray[indexPath.row].text == ""){
                sendCell.labelSendMessage.text = chatMessageArray[indexPath.row].imageText!
            }else{
                sendCell.labelSendMessage.text = chatMessageArray[indexPath.row].text!
            }
          // sendCell.labelSendMessage.text = chatDict?.value(forKey: "name") as? String
            if(chatMessageArray[indexPath.row].imageUrl == nil || chatMessageArray[indexPath.row].imageUrl! == "" ) {
                sendCell.imageViewHeight.constant = 0
                sendCell.imageVwSend.image = UIImage()
                sendCell.imageViewPlay.isHidden = true
                sendCell.labelTime.text = chatMessageArray[indexPath.row].sentTime!
                
                // sendCell.imageViewWidth.priority = UILayoutPriority(rawValue: 999)
                
            }else {
                sendCell.imageViewHeight.constant = 200
              //  let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(chatMessageArray[indexPath.row].imageUrl!)"
                let profilepic = chatMessageArray[indexPath.row].imageUrl!
                let processor = ResizingImageProcessor(referenceSize: CGSize(width: sendCell.imageVwSend.frame.width, height: sendCell.imageVwSend.frame.height)) >> RoundCornerImageProcessor(cornerRadius: 5)
                sendCell.imageVwSend.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
                //sendCell.imageVwSend.image = UIImage.init(named: "Aravind.jpg")
                // sendCell.imageViewWidth.priority = UILayoutPriority(rawValue: 250)
                // sendCell.imageViewWidth.constant = 200
                sendCell.imageViewPlay.isHidden = true // if you need make it as false
                sendCell.labelTime.text = chatMessageArray[indexPath.row].sentTime!
            }
            return sendCell
        } else {
//            let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(chatDict?.value(forKey: "photoUrl") as! String)"
//            let processor = ResizingImageProcessor(referenceSize: CGSize(width: senderImageIcon.frame.width, height: senderImageIcon.frame.height)) >> RoundCornerImageProcessor(cornerRadius: senderImageIcon.frame.height / 2)
//            senderImageIcon.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
            
            cell.imageViewMore.image = UIImage(named: "more.png")?.withRenderingMode(.alwaysTemplate)
          //  cell.imageViewMore.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.imageViewMore.tintColor = Constants.primaryColor
            if(chatMessageArray[indexPath.row].text == nil || chatMessageArray[indexPath.row].text == ""){
                cell.labelReceivedMsg.text = chatMessageArray[indexPath.row].imageText ?? ""
            }else{
                cell.labelReceivedMsg.text = chatMessageArray[indexPath.row].text!
            }
          // cell.labelReceivedMsg.text = chatDict?.value(forKey: "name") as? String
            if(chatMessageArray[indexPath.row].imageUrl == nil || chatMessageArray[indexPath.row].imageUrl! == "") {
                cell.imageViewHeight.constant = 0
                cell.imageViewReceive.image = UIImage()
                cell.imageViewPlay.isHidden = true
            }else {
                cell.imageViewHeight.constant = 200
                // let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(chatMessageArray[indexPath.row].imageUrl!)"
                let profilepic = chatMessageArray[indexPath.row].imageUrl!
                let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.imageViewReceive.frame.width, height: cell.imageViewReceive.frame.height)) >> RoundCornerImageProcessor(cornerRadius: 5)
                cell.imageViewReceive.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
               // cell.imageViewReceive.image = UIImage.init(named: "Aravind.jpg")
                cell.imageViewPlay.isHidden = true // if you need make it as false
            }
            cell.labelTime.text = chatMessageArray[indexPath.row].sentTime
            return cell
        }
        
        //return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected Index \(indexPath.row)")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChatDetailViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            
            Utilities.showAlertView("No Image Chosen", onView: self)
            return
        }
        //  self.userProfile.image = image
        dismiss(animated: true) {
            print("image data\(image)")
            self.imageUpload = image
            self.imageSelected.image = image
            UIView.transition(with: self.imageViewSelected, duration: 0.4, options: .transitionCrossDissolve, animations: {
              self.imageViewSelected.isHidden = false
            })
            
            //self.uploadimagedata(image: image)
        }
    }
    
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            self.scrollToRow(at: NSIndexPath(row: rows - 1, section: sections - 1) as IndexPath, at: .bottom, animated: true)
        }
    }
}
