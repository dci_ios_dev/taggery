/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ChatMessage : Codable {
	let chatWithID : Int?
	let name : String?
    let imageText : String?
    let imageUrl : String?
	let photoUrl : String?
	let readStatus : Int?
	let receiverFcmToken : String?
	let receiverPhotoUrl : String?
	let receiverRoleName : String?
	let receiverUniqueID : String?
	let roleName : String?
	let senderUserID : Int?
	let sentTime : String?
	let text : String?
	let toName : String?
	let uniqueID : String?
	let userFCMKey : String?

	enum CodingKeys: String, CodingKey {

		case chatWithID = "chatWithID"
        case imageText = "imageText"
        case imageUrl = "imageUrl"
		case name = "name"
		case photoUrl = "photoUrl"
		case readStatus = "readStatus"
		case receiverFcmToken = "receiverFcmToken"
		case receiverPhotoUrl = "receiverPhotoUrl"
		case receiverRoleName = "receiverRoleName"
		case receiverUniqueID = "receiverUniqueID"
		case roleName = "roleName"
		case senderUserID = "senderUserID"
		case sentTime = "sentTime"
		case text = "text"
		case toName = "toName"
		case uniqueID = "uniqueID"
		case userFCMKey = "userFCMKey"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		chatWithID = try values.decodeIfPresent(Int.self, forKey: .chatWithID)
		name = try values.decodeIfPresent(String.self, forKey: .name)
        imageText = try values.decodeIfPresent(String.self, forKey: .imageText)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
		photoUrl = try values.decodeIfPresent(String.self, forKey: .photoUrl)
		readStatus = try values.decodeIfPresent(Int.self, forKey: .readStatus)
		receiverFcmToken = try values.decodeIfPresent(String.self, forKey: .receiverFcmToken)
		receiverPhotoUrl = try values.decodeIfPresent(String.self, forKey: .receiverPhotoUrl)
		receiverRoleName = try values.decodeIfPresent(String.self, forKey: .receiverRoleName)
		receiverUniqueID = try values.decodeIfPresent(String.self, forKey: .receiverUniqueID)
		roleName = try values.decodeIfPresent(String.self, forKey: .roleName)
		senderUserID = try values.decodeIfPresent(Int.self, forKey: .senderUserID)
		sentTime = try values.decodeIfPresent(String.self, forKey: .sentTime)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		toName = try values.decodeIfPresent(String.self, forKey: .toName)
		uniqueID = try values.decodeIfPresent(String.self, forKey: .uniqueID)
		userFCMKey = try values.decodeIfPresent(String.self, forKey: .userFCMKey)
	}

}
