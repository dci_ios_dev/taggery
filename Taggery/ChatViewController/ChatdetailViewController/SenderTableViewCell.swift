//
//  SenderTableViewCell.swift
//  Taggery
//
//  Created by Aravind on 26/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class SenderTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelSendMessage: UILabel!
    @IBOutlet weak var imageVwSend: UIImageView!
    @IBOutlet weak var imageViewPlay: UIImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewChat: UIView!{
        didSet{
            viewChat.layer.cornerRadius = 5.0
            viewChat.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
