//
//  ChatViewCell.swift
//  Taggery
//
//  Created by Aravind on 24/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class ChatViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageVw: UIImageView!{
        didSet{
            avatarImageVw.layer.cornerRadius = avatarImageVw.frame.height/2
            avatarImageVw.clipsToBounds = true
            avatarImageVw.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var labelReceiver: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelChatCount: UILabel!
    @IBOutlet weak var viewRead: UIView!{
        didSet{
            viewRead.layer.cornerRadius = viewRead.frame.height / 2
            viewRead.clipsToBounds = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
