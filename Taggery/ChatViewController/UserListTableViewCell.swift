//
//  UserListTableViewCell.swift
//  Taggery
//
//  Created by Aravind on 24/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class UserListTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewUser: UIImageView!{
        didSet{
            imageViewUser.layer.cornerRadius = imageViewUser.frame.height / 2
            imageViewUser.clipsToBounds = true
            imageViewUser.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var imageViewSelect: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
