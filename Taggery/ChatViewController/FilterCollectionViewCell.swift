//
//  FilterCollectionViewCell.swift
//  Taggery
//
//  Created by Aravind on 24/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewClose: UIImageView!{
        didSet{
            imageViewClose.image = UIImage(named: "close.png")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            imageViewClose.tintColor = UIColor.black
        }
    }
    @IBOutlet weak var viewFilterBG: UIView!{
        didSet{
            viewFilterBG.layer.cornerRadius = 5.0
            viewFilterBG.backgroundColor = UIColor.white.withAlphaComponent(0.7)
            viewFilterBG.clipsToBounds = true
        }
    }
    
}
