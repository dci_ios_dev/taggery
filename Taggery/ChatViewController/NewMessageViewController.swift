//
//  NewMessageViewController.swift
//  Taggery
//
//  Created by Aravind on 24/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

class NewMessageViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let currentChildId = Constants.userId
    var nameList = [String]()
    var friendsList = [Friends]()
    var contactList = ["Vinoth","Mani","Shankar","Sathish","Manoj","Karthick","Kumar","Jerald","Nayak"]
    
    @IBOutlet weak var receipentView: UIView!
    @IBOutlet weak var receipentHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonChat: UIButton!
    @IBOutlet weak var collectionViewFilter: UICollectionView!
    @IBOutlet weak var tableViewFilter: UITableView!
    
    @IBOutlet weak var chatBtn: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.setBackgroungImage(self.view)
        receipentView.isHidden = true
        receipentHeight.constant = 0
        self.getFriendsResponse()
        buttonChat.isHidden = true
        chatBtn.constant = 0
        // Do any additional setup after loading the view.
    }
    //MARK: - Collection View Delegate & Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        for index in 0...(friendsList.count - 1) {
            if(nameList[indexPath.row] == "\(friendsList[index].id!)"){
                let size = (friendsList[index].userFirstName! as NSString).size(withAttributes: nil)
                return CGSize(width:size.width
                    + 70,height:36)
            }
        }
        return CGSize(width:0,height:36)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
        print("namelist \(nameList[indexPath.row])")
        for index in 0...(friendsList.count - 1) {
            if(nameList[indexPath.row] == "\(friendsList[index].id!)"){
                cell.labelName.text = friendsList[index].userFirstName
            }
        }
     //   cell.labelName.text = nameList[indexPath.row] as NSString as String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected Index \(indexPath.row)")
        nameList.remove(at: indexPath.row)
        collectionView.reloadData()
        tableViewFilter.reloadData()
    }
    
    //MARK: - Table View Delegate & Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as! UserListTableViewCell
        cell.selectionStyle = .none
        print("User ImageView Frame \(cell.imageViewUser.frame.width)")
        let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(friendsList[indexPath.row].userPicture!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.imageViewUser.frame.width, height: cell.imageViewUser.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.imageViewUser.frame.height / 2)
        cell.imageViewUser.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        
        cell.labelUserName.text = friendsList[indexPath.row].userFirstName
        cell.imageViewSelect.clipsToBounds = true
        if(nameList.count > 0) {
            buttonChat.isHidden = true
        } else {
            buttonChat.isHidden = true
        }
        if(nameList.contains("\(friendsList[indexPath.row].id!)")) {
            cell.imageViewSelect.image = UIImage(named: "select.png")
            
        } else {
            cell.imageViewSelect.image = UIImage(named: "unselect.png")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(nameList.contains("\(friendsList[indexPath.row].id!)")) {
            //           var strin = String(friendsList[indexPath.row].id!)
            if let index = nameList.index(of:"\(friendsList[indexPath.row].id!)") {
                nameList.remove(at: index)
            }
        } else {
            nameList.append("\(friendsList[indexPath.row].id!)")
        }
        
        collectionViewFilter.reloadData()
        collectionViewFilter.scrollToLast()
        tableViewFilter.reloadData()
        
        let userDetails = Utilities.getJSON("userDetail")
        print("Current UserDetails \(userDetails)")
        
      //  let messagDict = MessageFIR(chatWithID: friendsList[indexPath.row].id!, imageText: "", imageURL: "", name: "\(userDetails["UserLastName"])", photoURL: "\(userDetails["UserPicture"])", readStatus: 1, receiverFcmToken: friendsList[indexPath.row].userFCMKey!, receiverPhotoURL: friendsList[indexPath.row].userPicture!, receiverRoleName: "", receiverUniqueID: "", roleName: "", senderUserID: Int(currentChildId)!, sentTime: "", toName: friendsList[indexPath.row].userName!, uniqueID: "", userFCMKey: "\(userDetails["UserFCMKey"])", text: "")
        
        let messagDict = MessageFIR(chatWithID: friendsList[indexPath.row].id!, imageText: "", imageURL: "", name: "\(userDetails["UserLastName"])", photoURL: friendsList[indexPath.row].userPicture!, readStatus: 1, receiverFcmToken: friendsList[indexPath.row].userFCMKey!, receiverPhotoURL: "\(userDetails["UserPicture"])", receiverRoleName: "", receiverUniqueID: "", roleName: "", senderUserID: Int(currentChildId)!, sentTime: "", toName: friendsList[indexPath.row].userName!, uniqueID: "", userFCMKey: "\(userDetails["UserFCMKey"])", text: "")
        
        print("messageDict \(messagDict)")
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
        leftViewController.fromDict = messagDict.dictionaryRepresentation
        let transition = CATransition()
        transition.duration = 0.40
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(leftViewController, animated: false)
        
    }
    
    //MARK: - Api call
    func getFriendsResponse() {
        
        let params = ["user_id":currentChildId]
        let url = "\(Constants.k_Webservice_URL)/friendslist"
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.codabelAlamofireResponse(url: url, params: params) { Data in
            let decoder = JSONDecoder()
            do{
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(FriendsBase.self, from: Data)
                let statusCode = datas.statusCode!
                if(statusCode == 200){
                    self.friendsList.removeAll()
                    self.friendsList = datas.friends!
                    print("Friend List\(self.friendsList)")
                    print("Friend name\(String(describing: self.friendsList[0].userName))")
                    self.tableViewFilter.reloadData()
                    //                    self.viewedPosts = (datas.post?.viewed)!
                    //                    self.updateUi()
                }
                
                //  self.suggestionsarray.removeAll()
                
                //  self.locationsArray = datas.location!
                // self.postsArray = datas.posts!
                
                DispatchQueue.main.async {
                    Utilities.hideLoading()
                    //     self.tabelViewPosts.reloadData()
                    
                }
                
            }
            catch{
                print("ERROR \(error)")
            }
        }
    }
    
    //MARK: - Action
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
