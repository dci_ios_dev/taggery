//
//  ChatViewController.swift
//  Taggery
//
//  Created by Aravind on 24/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Kingfisher
import CodableFirebase

class ChatViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
   /* struct Welcome {
        let chatWithID: Int
        let imageText: String
        let imageURL: String
        let name, photoURL: String
        let readStatus: Int
        let receiverFcmToken, receiverPhotoURL, receiverRoleName, receiverUniqueID: String
        let roleName: String
        let senderUserID: Int
        let sentTime, toName, uniqueID, userFCMKey, text: String
     
        init(chatWithID: Int, imageText: String, imageURL: String, name: String, photoURL: String, readStatus: Int, receiverFcmToken: String, receiverPhotoURL: String, receiverRoleName: String, receiverUniqueID: String, roleName: String, senderUserID: Int, sentTime: String, toName: String, uniqueID: String, userFCMKey: String, text: String) {
            self.chatWithID = chatWithID
            self.imageText = imageText
            self.imageURL = imageURL
            self.name = name
            self.photoURL = photoURL
            self.readStatus = readStatus
            self.receiverFcmToken = receiverFcmToken
            self.receiverPhotoURL = receiverPhotoURL
            self.receiverRoleName = receiverRoleName
            self.receiverUniqueID = receiverUniqueID
            self.roleName = roleName
            self.senderUserID = senderUserID
            self.sentTime = sentTime
            self.toName = toName
            self.uniqueID = uniqueID
            self.userFCMKey = userFCMKey
            self.text = text
        }
    } */
    
    struct data {
        var name = ""
        var name2 = ""
        
        init(names:String) {
            self.name = names
        }
    }
    
    @IBOutlet weak var chatListTableView: UITableView!{
        didSet{
            chatListTableView.rowHeight = 70
            chatListTableView.estimatedRowHeight = UITableViewAutomaticDimension
        }
    }
    
    var structdata = [data]()
    
    var conversationArray = NSArray()
    var inboxArray = [ChatMessage]()
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var hastImageViw: UIImageView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
     var window: UIWindow?
    @IBOutlet weak var buttonNew: UIButton!{
        didSet{
            buttonNew.layer.cornerRadius = buttonNew.frame.height / 2
            buttonNew.clipsToBounds = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.userId = UserDefaults.standard.string(forKey: "userId")!
         let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
         swipeRight.direction = .left
         self.view.addGestureRecognizer(swipeRight)
        
        structdata.append(data(names: "hello"))
        
        Utilities.setBackgroungImage(self.view)
        chatListTableView.estimatedRowHeight = 70
        chatListTableView.rowHeight = UITableViewAutomaticDimension
        Conversation.showConversations { (conversations) in
            
            self.inboxArray.removeAll()
            if(conversations.count > 0) {
                do{
                    let decoder = FirebaseDecoder()
                    for inbox in conversations{
                        let datas = try decoder.decode(ChatMessage.self, from: inbox)
                        self.inboxArray.append(datas)
                    }
                    let ascendingArray = self.inboxArray.sorted(by: { $0.sentTime! > $1.sentTime! })
                    self.inboxArray = ascendingArray // For Descendig order also refer self.conversationArray
                } catch {
                    print("inbox Error \(error)")
                }
                
            }
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "sentTime", ascending: false)
            let sortedResults: NSArray = conversations.sortedArray(using: [descriptor]) as NSArray
            self.conversationArray = sortedResults // For Descendig order also refer self.inboxArray
            self.chatListTableView.reloadData()
        }
        backButton.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
            if gesture.direction == UISwipeGestureRecognizerDirection.right {
                print("Swipe Right Pick")
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.left {
                print("Swipe Left Orange")
                
                if(Constants.fromFCm == true){
                    let rightViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                    let transition = CATransition()
                    transition.duration = 0.40
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromRight
                    navigationController?.view.layer.add(transition, forKey: kCATransition)
                    navigationController?.pushViewController(rightViewController, animated: false)
                }else{
                    let rightViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                    let transition = CATransition()
                    transition.duration = 0.40
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromRight
                    navigationController?.view.layer.add(transition, forKey: kCATransition)
                    navigationController?.pushViewController(rightViewController, animated: false)
                }
                
    
    
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.up {
                print("Swipe Up")
    
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.down {
                print("Swipe Down")
            }
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inboxArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatViewCell", for: indexPath) as! ChatViewCell
        cell.selectionStyle = .none
      //  let dict = self.conversationArray[indexPath.row] as? NSDictionary
    //    var messageData = [MessageFIR]()
     //   messageData.append(MessageFIR.init(chatWithID: dict?.value(forKey:"chatWithID") as? Int ?? 0, imageText: dict?.value(forKey: "imageText") as? String ?? "", imageURL: dict?.value(forKey: "imageUrl") as? String ?? "", name: dict?.value(forKey: "name") as? String ?? "", photoURL: dict?.value(forKey: "photoUrl") as? String ?? "", readStatus: dict?.value(forKey: "readStatus") as? Int ?? 0, receiverFcmToken: dict?.value(forKey: "receiverFcmToken") as? String ?? "", receiverPhotoURL: dict?.value(forKey: "receiverPhotoUrl") as? String ?? "", receiverRoleName: dict?.value(forKey: "receiverRoleName") as? String ?? "", receiverUniqueID: dict?.value(forKey: "receiverUniqueID") as? String ?? "", roleName: dict?.value(forKey: "roleName") as? String ?? "", senderUserID: dict?.value(forKey: "senderUserID") as? Int ?? 0, sentTime: dict?.value(forKey: "sentTime") as? String ?? "", toName: dict?.value(forKey: "toName") as? String ?? "", uniqueID: dict?.value(forKey: "uniqueID") as? String ?? "", userFCMKey: dict?.value(forKey: "userFCMkey") as? String ?? "", text: dict?.value(forKey: "text") as? String ?? ""))
        
        
      //  print("Self.conversatin \(messageData[0].text!)")
       // welcomeData.append(Welcome.init(chatWithID: <#T##Int#>, imageText: <#T##String#>, imageURL: <#T##String#>, name: <#T##String#>, photoURL: <#T##String#>, readStatus: <#T##Int#>, receiverFcmToken: <#T##String#>, receiverPhotoURL: <#T##String#>, receiverRoleName: <#T##String#>, receiverUniqueID: <#T##String#>, roleName: <#T##String#>, senderUserID: <#T##Int#>, sentTime: <#T##String#>, toName: <#T##String#>, uniqueID: <#T##String#>, userFCMKey: <#T##String#>, text: <#String#>))
        
      //  print("Conversation \(indexPath.row) details \(String(describing: dict)) and User id \(dict?.value(forKey: "chatWithID") as! Int)")
        if(inboxArray[indexPath.row].senderUserID == Int(Constants.userId)){
            cell.labelReceiver.text = inboxArray[indexPath.row].toName!
        } else {
        cell.labelReceiver.text = inboxArray[indexPath.row].name!
           
        }
        if(inboxArray[indexPath.row].text == nil){
            cell.labelMessage.text = "image"
        }else{
            cell.labelMessage.text = inboxArray[indexPath.row].text!
        }
        let profilepic = "https://res.cloudinary.com/cloudtaggery18/Profile/\(inboxArray[indexPath.row].photoUrl!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.avatarImageVw.frame.width, height: cell.avatarImageVw.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.avatarImageVw.frame.height / 2)
        cell.avatarImageVw.kf.setImage(with: URL(string: profilepic),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        if(inboxArray[indexPath.row].readStatus! == 0){
            cell.viewRead.isHidden = false
        }else{
            cell.viewRead.isHidden = true
        }
        //cell.avatarImageVw
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
        let chatDict = self.conversationArray[indexPath.row] as! NSDictionary
        chatDict.setValue(1, forKey: "readStatus")
        Message.addInboxMessage(withValues: chatDict, fromId: "\(Constants.userId)", toId: "\(chatDict.value(forKey: "chatWithID")!)") { (status) in
            print("Status \(status)")
        }
//        Message.addInboxMessage(withValues: chatDict, fromId: "\(chatDict.value(forKey: "chatWithId")!)", toId:"\(Constants.userId)" ) { (status) in
//            print("Status \(status)")
//        }
        leftViewController.fromDict = chatDict
        let transition = CATransition()
        transition.duration = 0.40
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(leftViewController, animated: false)
    }
    @IBAction func actionNewTapped(_ sender: Any) {
      //  let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
         let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewMessageViewController") as! NewMessageViewController
        
        let transition = CATransition()
        transition.duration = 0.40
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(leftViewController, animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @objc func backTapped() {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! PostsViewController
        let transition = CATransition()
        transition.duration = 0.40
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(leftViewController, animated: false)
    }
}
