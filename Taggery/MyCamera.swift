//
//  MyCamera.swift
//  Taggery
//
//  Created by Gowsika on 07/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import CameraManager

class MyCamera: UIViewController {
    
    @IBOutlet weak var myshutter: UIButton!
    @IBOutlet weak var myFrame: UIView!
    
 let cameraManager = CameraManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        cameraManager.addPreviewLayerToView(self.myFrame)
        
            

        // Do any additional setup after loading the view.
    }

    @IBAction func cameraCapture(_ sender: Any) {
    
        cameraManager.capturePictureWithCompletion { (image, error) in
            
            print("Captured")
        }
        
    }
    
    @IBAction func startVideo(_ sender: Any) {
        cameraManager.startRecordingVideo()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
