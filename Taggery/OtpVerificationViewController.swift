//
//  OtpVerificationViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 18/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class OtpVerificationViewController: UIViewController {

    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var resendCodeBtnOutlet: UIButton!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    var hasEntered : Bool = false
    
    var enteredOtp = String()
    var mobileNumber = String()
    var numberNormal = String()
    var registrationDetails = [Registration]()
    
    var countrycode = ""
    
    var issignup = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setBackgroungImage(self.view)
        
        otpView.otpFieldsCount = 4
        otpView.otpFieldDefaultBorderColor = UIColor(red: 80/255.0, green: 44/255.0, blue: 150/255.0, alpha: 1.0)
        otpView.otpFieldEnteredBorderColor = UIColor.green
        otpView.otpFieldErrorBorderColor = UIColor.red
        otpView.otpFieldBorderWidth = 2
        otpView.delegate = self
        
        // Create the UI
        otpView.initalizeUI()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func didReceiveOtpVerificationResponse(responseDict:[String:Any]) {
        hasEntered = true
        print("Response:",responseDict)
        
        let status = responseDict["type"] as! String
        if status == "success" {
            
            
            if (issignup)
            {
               // registrationDetails.append(Registration.init(firstName: firstNameTxtFieldOutlet.text!, lastName:  lastNameTxtFieldOutlet.text!, userName: userNameTxtFieldOutlet.text!, phoneNumber: "\(phoneNumberTxtFieldOutlet.text!)", gender: genderTxtFieldOutlet.text!, relationShip: relationShipTxtFieldOutlet.text!, password: passwordTxtFieldOutlet.text!, fcmKey: fcmToken, deviceType: "Ios", emailID: EmailTxtFieldOutlet.text!, countryCode: countryCode, profilePic: picurl))
                
                 WebServiceHandler.sharedInstance.registration(FirstName: registrationDetails[0].firstName, LastName: registrationDetails[0].lastName, UserName: registrationDetails[0].userName, PhoneNumber: registrationDetails[0].phoneNumber, Gender: registrationDetails[0].gender, RelationShipStatus: registrationDetails[0].relationShip, Password: registrationDetails[0].password, FCMKey: registrationDetails[0].fcmKey, DeviceType: registrationDetails[0].deviceType, Emailid: registrationDetails[0].emailID,countryCode:registrationDetails[0].countryCode,ProfilePic: registrationDetails[0].profilePic,viewController: self)
                
               
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                vc.phoneNumber = self.mobileNumber
                vc.countryCode = self.countrycode
                vc.normalNumber = self.numberNormal

                self.navigationController?.pushViewController(vc, animated: true)
            }
            
         
            
        }else{
            
            Utilities.showAlertView("Verification:\(responseDict["message"] as! String)", onView: self)
        }
    }
    
    func didReceiveRegistrationResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else{
            
            Utilities.showAlertView("Registration Failed", onView: self)
        }
    }
    
    func didReceiveReSendOtpResponse(responseDict:[String:Any]) {
        
        print("ResentResponse:",responseDict)
        
        let status = responseDict["type"] as! String
        if status == "success" {
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
//            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            Utilities.showAlertView("ReSend:\(responseDict["message"] as! String)", onView: self)
        }
    }
    
    @IBAction func resendCodeBtnAction(_ sender: Any) {
        
        WebServiceHandler.sharedInstance.resendOtp(viewController: self, authkey: "", mobile: self.mobileNumber)
    }
    @IBAction func submitBtnAction(_ sender: Any) {
        
       
        if(hasEntered) {
            if(issignup){
                WebServiceHandler.sharedInstance.registration(FirstName: registrationDetails[0].firstName, LastName: registrationDetails[0].lastName, UserName: registrationDetails[0].userName, PhoneNumber: registrationDetails[0].phoneNumber, Gender: registrationDetails[0].gender, RelationShipStatus: registrationDetails[0].relationShip, Password: registrationDetails[0].password, FCMKey: registrationDetails[0].fcmKey, DeviceType: registrationDetails[0].deviceType, Emailid: registrationDetails[0].emailID,countryCode:registrationDetails[0].countryCode,ProfilePic: registrationDetails[0].profilePic,viewController: self)
            }
            else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                vc.phoneNumber = self.mobileNumber
                vc.countryCode = countrycode
                vc.normalNumber = self.numberNormal
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else {
            Utilities.showAlertView("Enter OTP", onView: self)
        }
    }
}

extension OtpVerificationViewController: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        let isEqual = (SharedVariables.sharedInstance.otpString == "12345")
        
        if isEqual {
            return true
        }
        else
        {
           return false
        }
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        
        SharedVariables.sharedInstance.otpString = otpString
        
        if(issignup){
            WebServiceHandler.sharedInstance.otpVerify(viewController: self, authkey: "", mobile: "\(countrycode)\(mobileNumber)", otp: otpString)
           
        }else{
            WebServiceHandler.sharedInstance.otpVerify(viewController: self, authkey: "", mobile: "\(mobileNumber)", otp: otpString)
           
        }
        print("OTPString: \(SharedVariables.sharedInstance.otpString)")
    }
}



