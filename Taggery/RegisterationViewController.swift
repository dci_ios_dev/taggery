//
//  RegisterationViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 05/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
import Cloudinary


class RegisterationViewController: UIViewController, UITextFieldDelegate, UIActionSheetDelegate {
    
    
    // MARK:- IB Outlets
    var imagePicker = UIImagePickerController()
    var imageUpload : UIImage?
    var picurl = ""
    var registrationDetails = [Registration]()
    
    @IBOutlet weak var EmailTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var masterScrollView: UIScrollView!
    @IBOutlet weak var firstNameTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var userNameTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var countryCodeTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var relationShipTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var genderTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var signUpBtnOutlet: UIButton!
    @IBOutlet weak var loginBtnOutlet: UIButton!
    @IBOutlet weak var userNameValidImageView: UIImageView!
    @IBOutlet weak var passwordValidImageView: UIImageView!
    @IBOutlet weak var confirmPasswordValidImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!{
        didSet{
            userImageView.layer.cornerRadius = userImageView.frame.height / 2
            userImageView.clipsToBounds = true
            userImageView.contentMode = .scaleAspectFill
        }
    }
    
    // MARK:- Local Variables
    
    
    
    var registrationDict = [String:Any]()
    var isUserNameValid: Bool!
    var isPasswordValid: Bool!
    var isConfirmPasswordValid: Bool!
    var isPhoneNumberValid: Bool!
    var countryCode = String()
    
    
    // MARK:- Default Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        imagePicker.delegate = self
        EmailTxtFieldOutlet.delegate = self
        firstNameTxtFieldOutlet.delegate = self
        lastNameTxtFieldOutlet.delegate = self
        userNameTxtFieldOutlet.delegate = self
        countryCodeTxtFieldOutlet.delegate = self
        phoneNumberTxtFieldOutlet.delegate = self
        genderTxtFieldOutlet.delegate = self
        relationShipTxtFieldOutlet.delegate = self
        passwordTxtFieldOutlet.delegate = self
        confirmPasswordTxtFieldOutlet.delegate = self
        
        signUpBtnOutlet.layer.cornerRadius = 5;
        signUpBtnOutlet.clipsToBounds = true;
        
        userNameValidImageView.isHidden = true
        passwordValidImageView.isHidden = true
        confirmPasswordValidImageView.isHidden = true
        
        isPasswordValid = false
        isConfirmPasswordValid = false
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let countryValuesCount = SharedVariables.sharedInstance.selectedCountryDict.values
        
        if countryValuesCount.count > 0 {
            
            let flagString = IsoCountries.flag(countryCode: SharedVariables.sharedInstance.selectedCountryDict["iso"] as! String)
            
            print(flagString)
            
            let countryText = "\(flagString) \(String(describing: SharedVariables.sharedInstance.selectedCountryDict["nicename"]!)) +\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
            
            countryCode = "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
            countryCodeTxtFieldOutlet.text = countryText
        }
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    // MARK:- Custom Methods
    
    func countryCodeTapped() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectCountryViewController") as! SelectCountryViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func genderTapped() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let maleButton = UIAlertAction(title: "Male", style: .default, handler: { (action) -> Void in
            print("Male")
            self.genderTxtFieldOutlet.text = "Male"
        })
        let  femaleButton = UIAlertAction(title: "Female", style: .default, handler: { (action) -> Void in
            print("Female")
            self.genderTxtFieldOutlet.text = "Female"
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertController.dismiss(animated: false, completion: nil)
        }
        
        alertController.addAction(cancelActionButton)
        alertController.addAction(maleButton)
        alertController.addAction(femaleButton)
        self.navigationController!.present(alertController, animated: true, completion: {
            
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func relationShipTapped() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let singleButton = UIAlertAction(title: "Single", style: .default, handler: { (action) -> Void in
            print("Single")
            self.relationShipTxtFieldOutlet.text = "Single"
        })
        let  complecatedButton = UIAlertAction(title: "Complicated", style: .default, handler: { (action) -> Void in
            print("Complicated")
            self.relationShipTxtFieldOutlet.text = "Complicated"
        })
        let relationShipButton = UIAlertAction(title: "In a Relationship", style: .default, handler: { (action) -> Void in
            print("In a Relationship")
            self.relationShipTxtFieldOutlet.text = "In a Relationship"
        })
        let  noneButton = UIAlertAction(title: "None", style: .default, handler: { (action) -> Void in
            print("None")
            self.relationShipTxtFieldOutlet.text = "None"
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertController.dismiss(animated: false, completion: nil)
        }
        
        alertController.addAction(cancelActionButton)
        alertController.addAction(singleButton)
        alertController.addAction(complecatedButton)
        alertController.addAction(relationShipButton)
        alertController.addAction(noneButton)
        self.navigationController!.present(alertController, animated: true, completion: {
            
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    // MARK:- Button Actions
    
    
    
    @available(iOS 10.0, *)
    @IBAction func signUpBtnAction(_ sender: Any) {
        print("Pic URL : \(self.picurl)")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fcmToken = appDelegate.fcmToken
        
        // let firstName = firstNameTxtFieldOutlet.text!
        let lastName = lastNameTxtFieldOutlet.text!
        let phoneNumber = phoneNumberTxtFieldOutlet.text!
        let gender = genderTxtFieldOutlet.text!
        let relationShip = relationShipTxtFieldOutlet.text!
        
        let email = EmailTxtFieldOutlet.text!
        // validate fName
        guard let firstName = firstNameTxtFieldOutlet.text,!firstName.isEmpty else{
            Utilities.showAlertView("Enter the Firstname", onView: self)
            return
        }
        // validate fName
        
        
        
        if firstName.count < 2 {
            Utilities.showAlertView("Enter Valid Firstname", onView: self)
        }
        if(picurl == ""){
            Utilities.showAlertView("Please add Image", onView: self)
        }
        else if(firstName.count < 2 )
        {
            Utilities.showAlertView("Enter Valid Firstname", onView: self)
        }
        else if(lastName.count < 2 )
        {
            Utilities.showAlertView("Enter Valid LastName", onView: self)
        }
        else if(isUserNameValid == false)
        {
            Utilities.showAlertView("Enter Valid Username", onView: self)
        }
        else if(isPhoneNumberValid == false){
             Utilities.showAlertView("Enter Valid PhoneNumber", onView: self)
        }
            
        else if(countryCode.count <= 0)
        {
            Utilities.showAlertView("Enter Valid Country code", onView: self)
        }
        else if(Utilities.isValidEmail(testStr: email) == false)
        {
            Utilities.showAlertView("Enter Valid Email ID", onView: self)
            
            
        }
        else if(phoneNumber.count < 9 || phoneNumber.count > 15)
        {

            Utilities.showAlertView("Enter Valid Phone Number between 9 to 15 digits", onView: self)

        }
            
        else if(gender.count <= 0)
        {
            Utilities.showAlertView("Enter Valid Gender", onView: self)
            
            return
        }
        else if(relationShip.count <= 0)
        {
            Utilities.showAlertView("Enter Valid Relationship Status", onView: self)
            
        }
        else if(isPasswordValid == false)
        {
            Utilities.showAlertView("Enter Valid Password", onView: self)
            
        }
        else if(isConfirmPasswordValid == false)
        {
            Utilities.showAlertView("Enter Valid Confirm Password", onView: self)
            
        }
            
        else{
            
            WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))\(phoneNumberTxtFieldOutlet.text!)")
            
//            WebServiceHandler.sharedInstance.registration(FirstName: firstNameTxtFieldOutlet.text!, LastName: lastNameTxtFieldOutlet.text!, UserName: userNameTxtFieldOutlet.text!, PhoneNumber: "\(phoneNumberTxtFieldOutlet.text!)", Gender: genderTxtFieldOutlet.text!, RelationShipStatus: relationShipTxtFieldOutlet.text!, Password: passwordTxtFieldOutlet.text!, FCMKey: fcmToken, DeviceType: "Ios", Emailid: EmailTxtFieldOutlet.text!,countryCode:countryCode,ProfilePic: picurl,viewController: self)
            
            registrationDetails.append(Registration.init(firstName: firstNameTxtFieldOutlet.text!, lastName:  lastNameTxtFieldOutlet.text!, userName: userNameTxtFieldOutlet.text!, phoneNumber: "\(phoneNumberTxtFieldOutlet.text!)", gender: genderTxtFieldOutlet.text!, relationShip: relationShipTxtFieldOutlet.text!, password: passwordTxtFieldOutlet.text!, fcmKey: fcmToken, deviceType: "Ios", emailID: EmailTxtFieldOutlet.text!, countryCode: "+\(countryCode)", profilePic: picurl))
            
        }
        
        
        
        //        if let firstName = firstNameTxtFieldOutlet.text,  firstName.count <= 3 {
        //
        //            if let lastName = lastNameTxtFieldOutlet.text,  lastName.count <= 3{
        //
        //                if isUserNameValid == true{
        //
        //                    if countryCode.count > 0 {
        //
        //                       if let phoneNumber = phoneNumberTxtFieldOutlet.text, phoneNumber.count >= 10 && phoneNumber.count <= 13{
        //
        //
        //                        if Utilities.isValidEmail(EmailTxtFieldOutlet.text!) == true
        //                        {
        //
        //                        if let gender = genderTxtFieldOutlet.text, gender.count > 0 {
        //
        //                            if let relationShip = relationShipTxtFieldOutlet.text, relationShip.count > 0 {
        //
        //                                if isPasswordValid == true
        //                                {
        //                                    if isConfirmPasswordValid == true
        //                                    {
        //
        //
        //                                    }
        //                                    else{
        //                                        Utilities.showAlertView("Enter Valid Confirm Password", onView: self)
        //                                    }
        //                                }
        //                                else{
        //                                    Utilities.showAlertView("Enter Valid Password", onView: self)
        //                                }
        //
        //                            }
        //                            else{
        //                                Utilities.showAlertView("Enter Valid Relationship Status", onView: self)
        //                            }
        //
        //                        }
        //                        else{
        //                            Utilities.showAlertView("Enter Valid Gender", onView: self)
        //                        }
        //
        //                    }
        //                        else{
        //                             Utilities.showAlertView("Enter Valid Email ID", onView: self)
        //                        }
        //
        //                    }
        //                       else{
        //                        Utilities.showAlertView("Enter Valid Phone Number", onView: self)
        //                        }
        //                }
        //                    else{
        //                        Utilities.showAlertView("Enter Valid Country code", onView: self)
        //                    }
        //            }
        //                else{
        //                    Utilities.showAlertView("Enter Valid Username", onView: self)
        //                }
        //        }
        //            else{
        //                Utilities.showAlertView("Enter Valid Lastname", onView: self)
        //            }
        //    }
        //        else{
        //
        //            print("First Name \(firstNameTxtFieldOutlet.text!.count)")
        //            Utilities.showAlertView("Enter Valid Firstname", onView: self)
        //        }
        
        
        
        
        
    }
    
    @IBAction func logInBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: "919952739312")
        
        
    }
    // MARK:- Text Field Delegates
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == countryCodeTxtFieldOutlet {
            
            self.countryCodeTapped()
            return false
        }
        else if textField == genderTxtFieldOutlet {
            
            self.genderTapped()
            return false
        }
        else if textField == relationShipTxtFieldOutlet {
            
            self.relationShipTapped()
            return false
        }
            
        else
        {
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        
        if textField == userNameTxtFieldOutlet {
            
            WebServiceHandler.sharedInstance.userNameCheck(name: userNameTxtFieldOutlet.text!, CountryCode: countryCodeTxtFieldOutlet.text!, Phonenumber: "", Type: "1", viewController: self)
        }
        else if textField == passwordTxtFieldOutlet {
            
            if let password = textField.text, password.count >= 8 {
                passwordValidImageView.isHidden = false
                isPasswordValid = true
            }
            else
            {
                passwordValidImageView.isHidden = true
                isPasswordValid = false
            }
        }
        else if textField == phoneNumberTxtFieldOutlet {
            
           
            let country = SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!
            
            WebServiceHandler.sharedInstance.userNameCheck(name: " ", CountryCode: "+\(country)", Phonenumber: phoneNumberTxtFieldOutlet.text!, Type: "2", viewController: self)
            
        //    WebServiceHandler.sharedInstance.userNameCheck(name: " ", CountryCode: "+\(countryCode)", Phonenumber: phoneNumberTxtFieldOulet.text!, Type: "2", viewController: self)

            
           
        }
        else if textField == confirmPasswordTxtFieldOutlet {
            
            if let confirmPassword = textField.text, confirmPassword.count >= 8 {
                
                if confirmPasswordTxtFieldOutlet.text == passwordTxtFieldOutlet.text
                {
                    confirmPasswordValidImageView.isHidden = false
                    isConfirmPasswordValid = true
                }
                else
                {
                    confirmPasswordValidImageView.isHidden = true
                    isConfirmPasswordValid = false
                }
            }
            else
            {
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField == firstNameTxtFieldOutlet {
            
            lastNameTxtFieldOutlet.becomeFirstResponder()
        }
        else if textField == lastNameTxtFieldOutlet {
            
            userNameTxtFieldOutlet.becomeFirstResponder()
        }
        else if textField == userNameTxtFieldOutlet {
            
            textField.resignFirstResponder()
            WebServiceHandler.sharedInstance.userNameCheck(name: textField.text!, CountryCode: countryCodeTxtFieldOutlet.text!, Phonenumber: phoneNumberTxtFieldOutlet.text!, Type: "1", viewController: self)
            self.countryCodeTapped()
        }
        else if textField == countryCodeTxtFieldOutlet {
            textField.resignFirstResponder()
            
        }
        else if textField == phoneNumberTxtFieldOutlet {
            
            textField.resignFirstResponder()
              let country = SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!
            
            WebServiceHandler.sharedInstance.userNameCheck(name: textField.text!, CountryCode: "+\(country)", Phonenumber: phoneNumberTxtFieldOutlet.text!, Type: "2", viewController: self)
            self.genderTapped()
        }
        else if textField == genderTxtFieldOutlet {
            
            
        }
        else if textField == relationShipTxtFieldOutlet {
            
            self.relationShipTapped()
        }
        else if textField == passwordTxtFieldOutlet {
            
            if let password = textField.text, password.count >= 8 {
                passwordValidImageView.isHidden = false
                isPasswordValid = true
            }
            else
            {
                passwordValidImageView.isHidden = true
                isPasswordValid = false
            }
            
            confirmPasswordTxtFieldOutlet.becomeFirstResponder()
        }
        else if textField == confirmPasswordTxtFieldOutlet {
            
            if let confirmPassword = textField.text, confirmPassword.count >= 8 {
                
                if confirmPasswordTxtFieldOutlet.text == passwordTxtFieldOutlet.text
                {
                    confirmPasswordValidImageView.isHidden = false
                    isConfirmPasswordValid = true
                }
                else
                {
                    confirmPasswordValidImageView.isHidden = true
                    isConfirmPasswordValid = false
                }
            }
            else
            {
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
            }
            confirmPasswordTxtFieldOutlet.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == firstNameTxtFieldOutlet || textField == lastNameTxtFieldOutlet {
            
            return Utilities.firstAndLastNameVadidation(string, CharactersInRange: range, textField: textField)
            
        }
        else if textField == userNameTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
            }
        }
        else if textField == passwordTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
            }
        }
        else if textField == confirmPasswordTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
            }
        }
        else
        {
            return true
        }
    }
    
    // MARK:- Api Response Methods
    
    func didReceiveUserNameCheck(responseDict:[String:Any],type:String) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
            if(type == "2"){
                isPhoneNumberValid = true
            }else{
                userNameValidImageView.isHidden = false
                isUserNameValid = true
            }
           
        }else{
            if(type == "2"){
                isPhoneNumberValid = false
                Utilities.showAlertView("Enter Valid Phone Number", onView: self)
            }else{
                isUserNameValid = false
                userNameValidImageView.isHidden = true

            }
          
        }
    }
//    func didReceiveRegistrationResponse(responseDict:[String:Any]) {
//
//        print("Response:",responseDict)
//
//        let statusCode = responseDict["StatusCode"] as! Int
//        if statusCode == 200 {
//
//            WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))\(phoneNumberTxtFieldOutlet.text!)")
//
//
//
//        }else{
//
//            Utilities.showAlertView("Registration Failed", onView: self)
//        }
//    }
    
    func didReceiveOtpResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let status = responseDict["type"] as! String
        if status == "success" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
            vc.countrycode = self.countryCode
            vc.mobileNumber = self.phoneNumberTxtFieldOutlet.text!
            vc.issignup = true
            vc.registrationDetails = registrationDetails
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
        }
    }
    @IBAction func editImageTapped(_ sender: Any) {
        let alertcontroller = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        var cameraButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        var galleryButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        var cancelButton = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertcontroller.dismiss(animated: false, completion: nil)
        }
        
        alertcontroller.addAction(cancelActionButton)
       // alertcontroller.addAction(cancelButton)
        alertcontroller.addAction(cameraButton)
        alertcontroller.addAction(galleryButton)
        self.navigationController!.present(alertcontroller, animated: true, completion: {
            alertcontroller.view.superview?.isUserInteractionEnabled = true
            alertcontroller.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    func uploadimagedata(image:UIImage)
    {
        
        
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
        var publicName = "IMG-\(Date().toMillis()!)"
        let params = CLDUploadRequestParams().setFolder("Profile").setPublicId(publicName)
        params.setResourceType(.image)
        //    let request = cloudinary.createUploader().upload(url: imageUrl, uploadPreset: "Sample_preset")
        let uploader = cloudinary.createUploader()
        
        uploader.upload(data: imageData!, uploadPreset: "Sample_preset", params: params, progress: { Progress in
            
        }) { result,error in
            LoadingIndicatorView.hide()
            if(error == nil){
                print("Result \(result)")
                let fetch = result?.url
                var fetchedUrl = URL(string: fetch!)
                print("FETCH URL is \(fetchedUrl!)")
//                if(self.isCoverImage){
//                    self.coverImageView.sd_setImage(with: URL(string: fetch!), placeholderImage: UIImage(named: "placeHolder.jpg"))
//                    self.coverPhoto = "\(fetchedUrl!.lastPathComponent)"
//                }
//                else
//                {
                    self.userImageView.sd_setImage(with: URL(string: fetch!), placeholderImage: UIImage(named: "placeHolder.jpg"))
                    self.picurl = "\(fetchedUrl!.lastPathComponent)"
              //  }
                
            }
            else
            {
                print("Result \(error)")
                
            }
            
        }
        
    }
}
extension RegisterationViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            
            Utilities.showAlertView("No Image Chosen", onView: self)
            return
        }
        //  self.userProfile.image = image
        dismiss(animated: true) {
            print("image data\(image)")
            LoadingIndicatorView.show()
            self.uploadimagedata(image: image)
//            self.imageUpload = image
//            self.userImageView.image = image
//            UIView.transition(with: self.userImageView, duration: 0.4, options: .transitionCrossDissolve, animations: {
//                // self.imageViewSelected.isHidden = false
//            })
            
            //self.uploadimagedata(image: image)
        }
    }
    
}
