//
//  ContactsViewController.swift
//  Taggery
//
//  Created by Gowsika on 19/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import ContactsUI
import Kingfisher
import SwiftyJSON
import Contacts

class myContactsCell:UITableViewCell{
    
    @IBOutlet weak var contactProfile: UIImageView!{
        didSet{
            contactProfile.layer.masksToBounds = false
            contactProfile.layer.borderColor = UIColor.black.cgColor
            contactProfile.layer.cornerRadius = contactProfile.frame.height/2
            contactProfile.clipsToBounds = true
        }
    }
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var contactName: UILabel!{
        didSet{
            contactName.layer.borderColor = UIColor.clear.cgColor
            contactName.layer.borderWidth = 1
            contactName.layer.cornerRadius = 10
            contactName.clipsToBounds = true
            contactName.layer.masksToBounds = false

        }
    }
    
}


struct ContactNew{
    var username : String
    var phoneNumber : String
    var contactPic : String
}
class ContactsViewController: UIViewController {

    var contactsArrray = [suggestionContacts]()
    var phoneNumberArray = [String]()
    var numbers = [String]()
    


    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //getUserContacts()
        
        tableView.delegate = self
        tableView.dataSource = self
        Utilities.setBackgroungImage(self.view)
        getUserContacts()
       // getMyFriends()
        
    }
    
    @IBAction func nextButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getUserContacts(){
        let contactStore = CNContactStore()
        let contacts = [CNContact]()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey
            ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                
                // Array containing all unified contacts from everywhere
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        print("\(number.stringValue)")
                        self.phoneNumberArray.append(number.stringValue)
                    }
                }
                
            }
            self.getMyFriends()


        } catch {
            print("unable to fetch contacts")
        }
        
    }
    
    
    
    func getMyFriends(){
    
        let url = "\(Constants.k_Webservice_URL)/contact"
        let datas = ["numbers":self.phoneNumberArray,"user_id":"\(Constants.userId)","page":"1"] as [String:Any]
       // Utilities.showLoading()
        print("DATAS \(datas)")
        WebServiceHandler.sharedInstance.codeableAllArray(url: url, params: datas) { Data in
            print("DAta \(Data)")
           // Utilities.hideLoading()
            let decoder = JSONDecoder()
            
            do{
                let result = try decoder.decode(ContactsBase.self, from: Data)
                if(result.statusCode! == 200){
                 self.contactsArrray = result.suggestion!
                if(self.contactsArrray.count == 0){
                    self.tableView.setEmptyMessage("No Contacts Found")
                }
                else{
                self.tableView.reloadData()
                }
            }
                else{
                    self.tableView.setEmptyMessage("No Contacts Found")
                }
            }
            catch{
                print("Error")
            }
        }
    }
    
    @objc func sendRequest(button: UIButton){
        
        print("Button Tag \(button.tag)")
        
        
        let position = button.tag
        var status = self.contactsArrray[position].status
        var url = ""
        switch status {
        case "1":
            url = "\(Constants.k_Webservice_URL)/follow"
            break
        case "2":
            url = "\(Constants.k_Webservice_URL)/unfollow"
            break
        case "3":
            url = "\(Constants.k_Webservice_URL)/follow"
            break
        default:
            
            break
        }
      
        let params = ["FollowerUserID":"\(Constants.userId)","FollowingUserID":"\(self.contactsArrray[position].id!)"] as [String:String]
        
        print("Api URL :\(url)")
        print("Api Params:\(params)")
       
        Utilities.showLoading()
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in

            Utilities.hideLoading()
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
              Utilities.showAlertView(Message, onView: self)
              self.getMyFriends()
            }
            if(StatusCode == 500)
            {
                Utilities.showAlertView(Message, onView: self)
                self.getMyFriends()
            }
            
        }
    }


}
extension ContactsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactsArrray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myContactsCell", for: indexPath) as! myContactsCell
        cell.contactName.text = "\(self.contactsArrray[indexPath.row].userFirstName!)"
        let profilePicUrl = "\(Constants.taggeryProfileUrl)\(self.contactsArrray[indexPath.row].userPicture!)"
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: cell.contactProfile.frame.width, height: cell.contactProfile.frame.height)) >> RoundCornerImageProcessor(cornerRadius: cell.contactProfile.frame.height / 2)
        cell.contactProfile.kf.setImage(with: URL(string: profilePicUrl),placeholder: UIImage(named: "defaultAvatar"), options: [.processor(processor)])
        cell.statusButton.tag = indexPath.row
        
        print(" STATSU \(self.contactsArrray[indexPath.row].status!)")
        if(self.contactsArrray[indexPath.row].status! == "1"){
            cell.statusButton.setTitle("Requested", for: .normal)
            cell.statusButton.backgroundColor = Utilities.hexStringToUIColor(hex: "#502C96")
            cell.statusButton.titleLabel?.textColor = UIColor.white
            cell.statusButton.setTitleColor(UIColor.white, for: .normal)

            
        }
         if(self.contactsArrray[indexPath.row].status! == "2"){
            print("Following is \(self.contactsArrray[indexPath.row].status!)")
            cell.statusButton.setTitleColor(Utilities.hexStringToUIColor(hex: "#502C96"), for: .normal)
            cell.statusButton.setTitle("Following", for: .normal)
            cell.statusButton.backgroundColor = UIColor.white
            
        }
         if(self.contactsArrray[indexPath.row].status! == "3")
        {
            print("Follow is \(self.contactsArrray[indexPath.row].status!)")
            cell.statusButton.setTitle("Follow", for: .normal)
            cell.statusButton.setTitleColor(UIColor.white, for: .normal)
            cell.statusButton.backgroundColor = Utilities.hexStringToUIColor(hex: "#502C96")
            
        }
        cell.statusButton.addTarget(self, action: #selector(sendRequest(button:)), for: .touchUpInside)
        
        return cell
        
    }
}
