//
//  CreatePostController.swift
//  Taggery
//
//  Created by Gowsika on 04/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

import CoreLocation
import SwiftyJSON

import DateTimePicker

import GooglePlaces
import GooglePlacePicker
import AVKit



protocol ExpandingCellDelegate {
    func updated(height: CGFloat)
    func stopscroll()
    func startScroll()
    func updatetext(text:String)
}

class customTagList:UICollectionViewCell{
    @IBOutlet weak var clearButtonLabel: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
}

class dateTimePickerCell:UITableViewCell{
    
    @IBOutlet weak var calendarButton: UIButton!
    
    @IBOutlet weak var timeButton: UIButton!
    
    @IBAction func calendarClicked(_ sender: Any) {
         //let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostController") as! CreatePostController
        NotificationCenter.default.post(name: Notification.Name("datetimeselected"), object: nil, userInfo: nil)
      
    }
    @IBAction func dateClicked(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("datetimeselected"), object: nil, userInfo: nil)

    }
}

class universsalHomeScreenCell: UITableViewCell{
    
    @IBOutlet weak var labelTextValue: UILabel!
}
class multiLabelCell : UITableViewCell{
    
    @IBOutlet weak var durationLabel: UILabel!
    
    
}


class durationColletionLabel : UICollectionViewCell{
    @IBOutlet weak var collectionLabel: UILabel!
    override func awakeFromNib() {
        collectionLabel.layer.masksToBounds = true
        collectionLabel.layer.cornerRadius = 6
        collectionLabel.layer.borderWidth = 0.6
        collectionLabel.layer.borderColor = UIColor.white.cgColor
        
        
    }
}
class durationCell:UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagFriends.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customTagList", for: indexPath) as! customTagList
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 6
       // cell.layer.borderWidth = 0.6
        cell.layer.borderColor = UIColor.white.cgColor
        cell.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.userNameLabel.text = "\(tagFriends[indexPath.row].userFirstName!)"
        cell.clearButtonLabel.tag = indexPath.row
        cell.clearButtonLabel.addTarget(self, action: #selector(sendRequest(button:)), for: .touchUpInside)

        
        
//        cell.userNameLabel.sizeToFit()
        return cell
    }
    
    @objc func sendRequest(button: UIButton){
        let params  = ["row":button.tag]
        NotificationCenter.default.post(name: Notification.Name("durationSelected"), object: nil, userInfo: params)
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//            let cell = collectionView.cellForItem(at: indexPath)
//            let texts = "HelloPeoples"
//            let size = (texts as NSString).size(withAttributes: nil)
//            return CGSize(width:size.width + 30,height:10)
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let texts = "\(tagFriends[indexPath.row].userFirstName!) \(tagFriends[indexPath.row].userLastName!)"
        let size = (texts as NSString).size(withAttributes: nil)
        print("SIZE \(size.width)  \(size.height)")
        return CGSize(width:size.width + 40,height:size.height + 10)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
    }
    var durationArray = [timeDuration]()
    
    var tagFriends = [friendsContacts]()

    @IBOutlet weak var durationCell: UICollectionView!
    override func awakeFromNib() {
        self.durationCell.delegate = self
        self.durationCell.dataSource = self
        self.durationCell.reloadData()
        
        
    }
}
class CustomExpandableCell : UITableViewCell,UITextViewDelegate{
    @IBOutlet weak var expandableCell: UITextView!
    
    var delegate : ExpandingCellDelegate?
    override func awakeFromNib() {
        expandableCell.delegate = self
        expandableCell.attributedText = Utilities.postattributedPlaceholder(text: "Write a Caption")
       // expandableCell.textColor = UIColor.white
    }
    func textViewDidChange(_ textView: UITextView) {
        if(textView.text.count == 0 ){
            expandableCell.attributedText = Utilities.postattributedPlaceholder(text: "Write a Caption")
        }
        if(textView.attributedText == Utilities.postattributedPlaceholder(text: "Write a Caption")){
            expandableCell.text = ""
        }
        let height = textView.newHeight(withBaseHeight: 10)
        delegate?.updated(height: height)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.updatetext(text: textView.text)
        delegate?.startScroll()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(expandableCell.attributedText == Utilities.postattributedPlaceholder(text: "Write a Caption")){
            expandableCell.text = ""
        }
        delegate?.updatetext(text: textView.text)
        delegate?.stopscroll()
    }
}
class collectionLabel : UICollectionViewCell{
    @IBOutlet weak var titleLabel: UILabel!
   
    override func awakeFromNib() {
        titleLabel.layer.masksToBounds = true
        titleLabel.layer.cornerRadius = 6
      //  titleLabel.layer.borderWidth = 0.6
        titleLabel.layer.borderColor = UIColor.white.cgColor
        titleLabel.backgroundColor = UIColor.white.withAlphaComponent(0.3)
    }
}
class collectionTableView : UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var googlearrays = [GoogleMaps]()
    var timeDurations = [timeDuration]()
    var isLocationArray = false
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return googlearrays.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        {
            let size = (googlearrays[indexPath.row].placename as NSString).size(withAttributes: nil)
            return CGSize(width:size.width + 40,height:size.height + 10)
       
        
   }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let params  = ["locationposiion":indexPath.row]
       NotificationCenter.default.post(name: Notification.Name("locationselected"), object: nil, userInfo: params)
    
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionview", for: indexPath) as! collectionLabel
        //cell.titleLabel.textAlignment = .center
        cell.titleLabel.font.withSize(15)
        cell.titleLabel.text = googlearrays[indexPath.row].placename
        
       // cell.titleLabel.sizeToFit()
        
        
        //cell.setNeedsLayout()
       // cell.setNeedsDisplay()
        return cell
    }
    @IBOutlet weak var collectionview: UICollectionView!
    override func awakeFromNib() {
        self.collectionview.delegate = self
        self.collectionview.dataSource = self
        
        
//        if let flowlayout = collectionview.collectionViewLayout as? UICollectionViewFlowLayout{
//            flowlayout.estimatedItemSize = CGSize(width:1,height:1)
//            if #available(iOS 10.0, *) {
//             //   flowlayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
//            } else {
//                // Fallback on earlier versions
//            }
//
//        }
    }
}



class CreatePostController: UIViewController ,UITableViewDelegate,UITableViewDataSource,ExpandingCellDelegate,DateTimePickerDelegate,GMSPlacePickerViewControllerDelegate{
    
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        print("Place is \(place)")
        self.selectedlocation = place.name
        self.selectedlatitude = "\(place.coordinate.latitude)"
        self.selectedlongitude = "\(place.coordinate.longitude)"
        
        viewController.dismiss(animated: true) {
            self.TableViews.reloadData()
        }
        
    }
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true) {
            
        }
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        
        viewController.dismiss(animated: true) {
            
        }
    }
    
    
    func stopscroll(){
        isediting = true
    //    self.TableViews.isScrollEnabled = false
    }
    func startScroll(){
        isediting = false
     //   self.TableViews.isScrollEnabled = true
    }
    
    @IBOutlet weak var PostButton: UIButton!
    var tagFriends = [friendsContacts]()

    
    var selectedUsers = [Int]()
    let datepicker = UIDatePicker()
    var expandingCellHeight: CGFloat = 50
    let expandingIndexRow = 1
    var locationmanager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    let imageheader = UIImageView()
    @IBOutlet weak var labelCollections: UILabel!
    @IBOutlet weak var locationCollections: UICollectionView!
    @IBOutlet weak var TableViews: UITableView!
    @IBOutlet var mainbgview: UIView!
    @IBOutlet weak var ScrollMasterView: UIView!
    var googlePlaces = [GoogleMaps]()
    var timeDurations = [timeDuration]()
    var postduration = String()
    var deviceheight :CGFloat!
    var selectedlocation = "Select Location"
    var selectedduration = "infinity"
    var selectedimage : UIImage!
    var isediting = false
    var datetimepicker = DateTimePicker()
    
    var selecteddate = ""
    var selectedtime = ""
    var selectedlatitude = ""
    var selectedlongitude = ""
    var posttests = ""
    var selectedurationvalue = "infinity"
    var posttime = ""
   
    var placePicker = GMSPlacePickerViewController()
    var selectedImageData = Data()
    var imageURL:URL!
    
    var isimage = false
    var isvideourl : URL!
    
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    var nameFromCloudinary = ""
    var taglistarray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gettimelists()
         self.checkforlocationaccess()
        let min = Date().addingTimeInterval(-60 * 60 * 24 * 0)
        let max = Date().addingTimeInterval(60 * 60 * 24 * 3600)
        datetimepicker = DateTimePicker.create(minimumDate: min,maximumDate: max)
        datetimepicker.is12HourFormat = false
        datetimepicker.delegate = self
        
        
       self.selectedlatitude = SharedVariables.sharedInstance.currentLatitude
        self.selectedlongitude = SharedVariables.sharedInstance.currentLongitude
        
      
        


        let date = Date()
                let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        self.selectedtime = "\(hour):\(minutes):\(seconds)"
        self.selecteddate = formatter.string(from: date)
        
        self.posttime = "\(self.selecteddate) \(self.selectedtime)"

        
        datetimepicker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd"
            self.selecteddate = formatter.string(from: date)
            formatter.dateFormat = "hh:mm:ss"
            self.selectedtime = formatter.string(from: date)
            let indexpath =  IndexPath(row: 6, section: 0)
            self.posttime = "\(self.selecteddate) \(self.selectedtime)"
            self.TableViews.reloadRows(at: [indexpath], with: UITableViewRowAnimation.none)
        }
      
        
        
       
        TableViews.delegate = self
        TableViews.dataSource = self
        TableViews.backgroundColor = UIColor.clear
        deviceheight = UIScreen.main.bounds.height / 2
      //  self.TableViews.contentInset = UIEdgeInsetsMake(deviceheight,0,0,0)
      
        
        if(isimage)
        {
//            imageheader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 300)
//            imageheader.image = selectedimage
//            imageheader.contentMode = .scaleAspectFill
//            imageheader.clipsToBounds = true
            
            let imageView = UIImageView()
            imageView.image = self.selectedimage
            imageView.contentMode = .scaleAspectFill
            TableViews.parallaxHeader.view = imageView
            TableViews.parallaxHeader.height = UIScreen.main.bounds.size.height / 2
            TableViews.parallaxHeader.minimumHeight = 0
            TableViews.parallaxHeader.mode = .top
            
        }
        else
        {
            let views = UIView()
            views.frame = CGRect(x:0,y:20,width:UIScreen.main.bounds.size.width,height:300)
            
            player = AVPlayer(url: self.isvideourl)
            playerController = AVPlayerViewController()
            guard player != nil && playerController != nil else {
                return
            }
            playerController!.showsPlaybackControls = true
            playerController!.player = player!
            playerController!.view.frame = views.bounds
            self.addChildViewController(playerController!)
            views.addSubview(playerController!.view)
            playerController?.didMove(toParentViewController: self)
            
            TableViews.parallaxHeader.view = views
            TableViews.parallaxHeader.height = UIScreen.main.bounds.size.height / 2
            TableViews.parallaxHeader.minimumHeight = 0
            TableViews.parallaxHeader.mode = .top
             player?.play()
        }
        
        
        self.configuregoogle()
       
        
     //   self.view.addSubview(imageheader)
        self.TableViews.rowHeight = UITableViewAutomaticDimension
        self.TableViews.estimatedRowHeight = 65
        self.TableViews.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(locationselected(notification:)), name: NSNotification.Name("locationselected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(durationSelected(notification:)), name: NSNotification.Name("durationSelected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(datetimeSelected(notification:)), name: NSNotification.Name("datetimeselected"), object: nil)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
       
    }
    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    func updatetext(text: String) {
        self.posttests = text
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        self.tagFriends = Constants.tagFriends

        self.TableViews.reloadData()
    }
    @IBAction func postButtonClicked(_ sender: Any) {
        
        if(tagFriends.count > 0){
            for(index,value) in tagFriends.enumerated(){
                self.taglistarray.append("\(tagFriends[index].id!)")
            }
        }
        
        if(self.posttests == "Write a Caption"){
            self.posttests = ""
        }
        
        let params = ["PostUserID":"\(Constants.userId)","PostText":self.posttests,"PostType":isimage ? "1":"2","PostLocation":"\(self.selectedlocation)","PostLatitude":"\(Constants.UserLatitude)","PostLongitude":"\(Constants.UserLongitude)","PostTimeFrame":"\(self.selectedurationvalue)","PostTagList":self.taglistarray,"PostTime":"\(self.posttime)","mediaType":isimage ? "1":"2","PostImage":isimage ? "\(self.nameFromCloudinary)":"\(isvideourl.lastPathComponent)"] as [String : Any]
        LoadingIndicatorView.show("Loading")
        print("params \(params)")
        WebServiceHandler.sharedInstance.addNewPosts(parameters: params) { JSON in
           print("JSOn \(JSON)")
            LoadingIndicatorView.hide()
            guard let status = JSON["Status"].string,let statusCode = JSON["StatusCode"].int,let mesage = JSON["Message"].string else{
                return
            }
             print("JSOn \(JSON)")
            if(statusCode == 200)
            {
              // Utilities.showAlertView(mesage, onView: self)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! PostsViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else{
                 Utilities.showAlertView(mesage, onView: self)
                 LoadingIndicatorView.hide()
            }
           // let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! PostsViewController
            //self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        
        if(indexPath.row == 0)
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "emptycell", for: indexPath)
            cell.selectionStyle = .none
            

        }
        if(indexPath.row == 1){
            cell = tableView.dequeueReusableCell(withIdentifier: "CustomExpandableCell", for: indexPath)
            if let expandablecall = cell as? CustomExpandableCell{
                expandablecall.delegate = self
                expandablecall.expandableCell.layer.masksToBounds = true
                expandablecall.expandableCell.layer.cornerRadius = 6
                expandablecall.expandableCell.layer.borderWidth = 0.6
                expandablecall.expandableCell.layer.borderColor = UIColor.white.cgColor
                expandablecall.backgroundColor = UIColor.clear
            }
        }
        if (indexPath.row == 2){
          cell = tableView.dequeueReusableCell(withIdentifier: "secondCell", for: indexPath)
        }
        
        
        
        
        if (indexPath.row == 3){
           
            let collections = tableView.dequeueReusableCell(withIdentifier: "seventhCell",for: indexPath) as! durationCell
            collections.tagFriends = self.tagFriends
            collections.durationCell.reloadData()
            
            cell = collections
            
            
            
            
        }
        if (indexPath.row == 4){
           
            let cells = tableView.dequeueReusableCell(withIdentifier: "thirdCell", for: indexPath) as! universsalHomeScreenCell
            
            cells.labelTextValue.text = self.selectedlocation
            cells.labelTextValue.sizeToFit()
            cell = cells
            
            
          
        }
        if (indexPath.row == 5){
      
            let collections = tableView.dequeueReusableCell(withIdentifier: "fourthCell",for: indexPath) as! collectionTableView
            collections.googlearrays = self.googlePlaces
            collections.collectionview.reloadData()
            collections.isLocationArray = true
            cell = collections
            
            
           
            
        }
        if(indexPath.row == 6){
        
            let datepickercell = tableView.dequeueReusableCell(withIdentifier: "fifthCell",for: indexPath) as! dateTimePickerCell
            
            //let datepickercell = tableView.cellForRow(at: indexPath) as! dateTimePickerCell
            //datepickercell.calendarButton.titleLabel?.text = "30-11-1992"
            // datepickercell.timeButton.titleLabel?.text = "00:00:00"
            
            
            datepickercell.calendarButton.setTitle(self.selecteddate, for: UIControlState.normal)
            datepickercell.timeButton.setTitle(self.selectedtime, for: UIControlState.normal)
            
            cell = datepickercell
            
            
       
            
            
        }
        if (indexPath.row == 7){
          //  cell = tableView.dequeueReusableCell(withIdentifier: "seventhCell",for: indexPath)
         
            
            
            let multilabelCell = tableView.dequeueReusableCell(withIdentifier: "sixthCell", for: indexPath) as! multiLabelCell
            multilabelCell.durationLabel.text = self.selectedduration
            cell = multilabelCell
            
            
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func configuregoogle()
    {
            let center = CLLocationCoordinate2D(latitude:self.latitude, longitude: self.longitude)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            var config = GMSPlacePickerConfig(viewport: viewport)
            placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 1)
        {
            return expandingCellHeight
        }
        if(indexPath.row == 0)
        {
            return 5
        }
        
        if(indexPath.row == 3){
            if(self.tagFriends.count > 0){
                return UITableViewAutomaticDimension
            }
            else{
                return 0
            }
        }
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cell = UITableViewCell()
       // cell.selectionStyle = .none
        if(indexPath.row == 1){
          //
            cell = tableView.cellForRow(at: indexPath) as! CustomExpandableCell
        }
        if (indexPath.row == 2){
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController") as! TagUserViewController!
            
           // viewController?.tagFriends = self.tagFriends
            self.navigationController?.pushViewController(viewController!, animated: true)
            cell = tableView.cellForRow(at: indexPath)!
        }
        if (indexPath.row == 3){
            
           
          //  self.navigationController?.pushViewController(mapviewController, animated: false)
        }
        if (indexPath.row == 4){
            
            let mapviewController = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            
            present(placePicker, animated: true, completion: nil)
            
            
        }
        if (indexPath.row == 5){
            
            cell = tableView.cellForRow(at: indexPath) as! collectionTableView

            
          
        }
        if(indexPath.row == 6){
//            cell = tableView.cellForRow(at: indexPath)!
//            cell.selectionStyle = .none
//            datetimepicker.show()

        }
        if (indexPath.row == 7){



        }
      //  cell.selectionStyle = .none
       
    }
    
    @objc func updatelocation(){
    
    self.TableViews.reloadData()
    
    }
    
    
    
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func gettimelists()
    {
        self.timeDurations.append(timeDuration(timename: "infinity", duration: "infinity"))
        for index in 1...10{
            self.timeDurations.append(timeDuration(timename: "\(index) sec", duration: "\(index)"))
        }
        self.TableViews.reloadData()
        
       // self.TableViews.reloadData()
        
    }
    @objc func datetimeSelected(notification:NSNotification)
    {
    
    self.datetimepicker.show()
    
    
    }
    
    @objc func durationSelected(notification:NSNotification)
    {
        let arrayposition = notification.userInfo?["row"] as! Int
        self.tagFriends.remove(at: arrayposition)
        let indexPathRow:Int = 3
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        self.TableViews.reloadRows(at: [indexPosition], with: UITableViewRowAnimation.none)

    }
    
    @objc func locationselected(notification:NSNotification)
    {
        let arrayposition = notification.userInfo?["locationposiion"] as! Int
        self.selectedlocation = self.googlePlaces[arrayposition].placename
        let indexPathRow:Int = 4
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        self.TableViews.reloadRows(at: [indexPosition], with: UITableViewRowAnimation.none)
        
      
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        guard let userInfo = notification.userInfo,
            let keyBoardValueBegin = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let keyBoardValueEnd = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, keyBoardValueBegin != keyBoardValueEnd else {
                return
        }
        let keyboardHeight = keyBoardValueEnd.height
        TableViews.contentInset.bottom = keyboardHeight
    }
    func updated(height: CGFloat) {
        expandingCellHeight = height
        UIView.setAnimationsEnabled(false)
        TableViews.beginUpdates()
        TableViews.endUpdates()
        //UIView.setAnimationsEnabled(true)
        let indexPath = IndexPath(row: expandingIndexRow, section: 0)
        TableViews.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if(isediting)
        {
            
        }
        else
        {
            let y = deviceheight - (scrollView.contentOffset.y + deviceheight)
            let height = min(max(y, 60), deviceheight)
    //        imageheader.frame = CGRect(x: 0, y: -10, width: UIScreen.main.bounds.size.width, height: height)
        }
       
        
        
    }
    
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        print("Date \(picker.selectedDateString)")

        
    }
    
    func checkforlocationaccess(){
        locationmanager.requestWhenInUseAuthorization()
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways){
            latitude = (locationmanager.location?.coordinate.latitude)!
            longitude = (locationmanager.location?.coordinate.longitude)!
        
            self.getgoogleplaces()
        }
    }
    func getgoogleplaces()
    {
        let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(self.latitude),\(self.longitude)&radius=1500&key=AIzaSyDuVjK3ZuFr70Q7v5C90SGhyIqR8RPaI9g"
        WebServiceHandler.sharedInstance.getUniversalJsonRequest(url:url) { JSON in
            guard let status = JSON["status"].string else{
                return
            }
            print("LAT lng \(self.latitude)  \(self.longitude)")
            
            print("JSON  is \(JSON)")
            if(status == "OK")
            {
                let data = JSON["results"].array
                for datas in data!{
                    let placename = datas["name"].string
                    let lat = datas["geometry"]["location"]["lat"].double
                    let lng = datas["geometry"]["location"]["lng"].double
                    self.googlePlaces.append(GoogleMaps(latitude:lat!,longitude:lng!,placename:placename!))
                }
                self.selectedlocation = self.googlePlaces[0].placename
                let indexPosition = IndexPath(row: 4, section: 0)

                print("Count is \(self.googlePlaces.count)")
                self.TableViews.reloadRows(at: [indexPosition], with: UITableViewRowAnimation.none)
               self.TableViews.reloadData()
                self.configuregoogle()
            }
        }
    }
}
