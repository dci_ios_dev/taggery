//
//  TryMeController.swift
//  Taggery
//
//  Created by Gowsika on 05/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class TryMeController: UIViewController,UITableViewDataSource,UITableViewDelegate {
 

    @IBOutlet weak var testtableview: UITableView!
    let imageheader = UIImageView()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.testtableview.delegate = self
        self.testtableview.dataSource = self
      
        self.testtableview.contentInset = UIEdgeInsetsMake(300,0,0,0)
    
        imageheader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 300)
        imageheader.image = UIImage.init(named: "image1.jpg")
        imageheader.contentMode = .scaleAspectFill
        imageheader.clipsToBounds = true
        self.view.addSubview(imageheader)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = 300 - (scrollView.contentOffset.y + 300)
        let height = min(max(y, 60), 400)
        imageheader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)

        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
    
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
