//
//  SettingsViewController.swift
//  Taggery
//
//  Created by Gowsika on 11/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SwiftyJSON
protocol cellPrivateProfile {
    func privateProfile(indexpath : Int)
}



class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var settingsTableView: UITableView!
    var profileType = 0
    var businessProfile = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setBackgroungImage(self.view)
        self.settingsTableView.backgroundColor = UIColor.clear
        
        self.settingsTableView.delegate = self
        self.settingsTableView.dataSource = self
        self.settingsTableView.separatorStyle = .none
        
        self.settingsTableView.estimatedRowHeight = 80
        self.settingsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.settingsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "normalCell")
        
        
        
        let userdetails = Utilities.getJSON("userDetail")
        profileType  = userdetails["UserProfileType"].int ?? 1
        businessProfile = userdetails["UserProfileType"].int ?? 1
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        
        // Do any additional setup after loading the view.
    }
    // 
    func handleLogout(){
        let params = ["user_id":"\(Constants.userId)"] as [String:String]
         let url = "\(Constants.k_Webservice_URL)/logout"
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            print("RESP \(JSON)")
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                UserDefaults.standard.removeObject(forKey: "userDetail")
                UserDefaults.standard.set(false, forKey: "isLogin")
                self.navigationController?.popToRootViewController(animated: true)
                //UIApplication.shared.keyWindow?.rootViewController = vc

               // self.navigationController?.pushViewController(vc, animated: true)
            
            }
            if(StatusCode == 500)
            {
                
            }
        }
    }
    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 7
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 0)
        {
          let editprofileController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(editprofileController, animated: true)
            
        }
        //
        if(indexPath.row == 1)
        {
            let changePasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.navigationController?.pushViewController(changePasswordViewController, animated: true)
        }
        if(indexPath.row == 2){
            let changePasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
            self.navigationController?.pushViewController(changePasswordViewController, animated: true)
            
            print("Called")
        }
        if(indexPath.row == 5){
            self.handledeletebutton()
        }
        if(indexPath.row == 6){
            self.handleLogout()
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
       
        if(indexPath.row == 0){
             let formcell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsCell
            formcell.label.text = "Edit Profile"
            formcell.label.textColor = UIColor.white
            
            cell = formcell
        }
        if(indexPath.row == 1){
             let formcell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsCell
            formcell.label.text = "Change Password"
            formcell.label.textColor = UIColor.white

            cell = formcell

        }
        if(indexPath.row == 2){
//             let formcell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsCell
//            formcell.label.text = "Bussiness Profile"
//            formcell.label.textColor = UIColor.white
//
//            cell = formcell
            let formcell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsCell
            formcell.label.text = "Privacy Settings"
            formcell.label.textColor = UIColor.white
            
            cell = formcell

        }
        if(indexPath.row == 3){
            let formcell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SettingsRadioCell
            formcell.switchRadioCell.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
            formcell.switchRadioCell.backgroundColor = UIColor.clear
            formcell.switchLabel.text = "Private Profile"
            formcell.switchLabel.textColor = UIColor.white
            formcell.switchRadioCell.addTarget(self, action:#selector(handlePrivateProfile), for: .touchUpInside)
            if(businessProfile == 1){
                formcell.switchRadioCell.isOn = false

            }
            else if(businessProfile == 2){
                formcell.switchRadioCell.isOn = true

            }
            else
            {
                formcell.switchRadioCell.isOn = false
            }
            
            cell = formcell

        }
        if(indexPath.row == 4){
            //cell = formcell switchCell
        
            let formcell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SettingsRadioCell
            formcell.switchRadioCell.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
            formcell.switchRadioCell.backgroundColor = UIColor.clear
            formcell.switchLabel.text = "Business Profile"
            formcell.switchLabel.textColor = UIColor.white
            formcell.switchRadioCell.addTarget(self, action:#selector(handleBusinessProfile), for: .touchUpInside)
            if(profileType == 1){
                formcell.switchRadioCell.isOn = true
            }
            else if(profileType == 2){
                formcell.switchRadioCell.isOn = false

            }
            else
            {
                formcell.switchRadioCell.isOn = false
            }
            cell = formcell
        }
        if(indexPath.row == 5){
            //cell = formcell
//            let formcell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SettingsRadioCell
//            formcell.switchRadioCell.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
//            formcell.switchLabel.textColor = UIColor.white
//            formcell.switchLabel.text = "Deactivate Likes & Rank"
//            formcell.switchRadioCell.backgroundColor = UIColor.clear
//            cell = formcell
            let cells = tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath)
            cells.textLabel?.text = "Delete Account"
            cells.textLabel?.textColor = UIColor.black
            cell = cells
            
        }
        if(indexPath.row == 6)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath)
            cells.textLabel?.text = "Logout"
            cells.textLabel?.textColor = UIColor.white
            cell = cells
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        return cell
        
    }
    
    @objc func handlePrivateProfile(sender: Any){
        if(businessProfile == 1){
            businessProfile = 2
        }
        else if(businessProfile == 2)
        {
            businessProfile = 1

        }
        
        let params = ["user_id":"\(Constants.userId)","ProfileType":"\(businessProfile)"]
        let urls = "\(Constants.k_Webservice_URL)changeprofiletype"
        WebServiceHandler.sharedInstance.commonPostRequest(url: urls, params: params) { JSON in
            print("\(JSON)")
            let statuscode = JSON["StatusCode"].int
            if(statuscode == 200){
                let userDict = JSON["UserDetails"]
                UserDefaults.standard.set(userDict.rawString(), forKey: "userDetail")
                self.settingsTableView.reloadData()
            }
        }
    }
    @objc func handleBusinessProfile(sender: Any){
        
        if(profileType == 1){
            profileType = 3
        }
        else if(profileType == 2)
        {
        }
        else
        {
            profileType = 1
        }
        
        let params = ["user_id":"\(Constants.userId)","ProfileType":"\(profileType)"]
        let urls = "\(Constants.k_Webservice_URL)changeprofiletype"
        WebServiceHandler.sharedInstance.commonPostRequest(url: urls, params: params) { JSON in
            print("\(JSON)")
            let statuscode = JSON["StatusCode"].int
            if(statuscode == 200){
                let userDict = JSON["UserDetails"]
                UserDefaults.standard.set(userDict.rawString(), forKey: "userDetail")
                self.settingsTableView.reloadData()
            }
        }
    }
     func handledeletebutton(){
        let alert = UIAlertController(title: "UIAlertController", message: "Do you want to Delet your Account", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler: {
            action in
            let params = ["user_id":"\(Constants.userId)"]
            let urls = "\(Constants.k_Webservice_URL)deleteaccount"
            WebServiceHandler.sharedInstance.commonPostRequest(url: urls, params: params) { JSON in
                print("\(JSON)")
                let statuscode = JSON["StatusCode"].int
                if(statuscode == 200){
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                    UserDefaults.standard.removeObject(forKey: "userDetail")
                    UserDefaults.standard.set(false, forKey: "isLogin")
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)

        
        
     
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
