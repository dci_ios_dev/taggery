//
//  ChangePasswordViewController.swift
//  Taggery
//
//  Created by Gowsika on 11/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var currentPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        currentPassword.delegate = self
        newPassword.delegate = self
        confirmPassword.delegate = self
        Utilities.setBackgroungImage(self.view)
        // Do any additional setup after loading the view.
    }
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            let transition = CATransition()
            transition.duration = 0.40
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func currentPasswordEye(_ sender: Any) {
        
        currentPassword.isSecureTextEntry = !currentPassword.isSecureTextEntry
    }
    
    @IBAction func newPassEye(_ sender: Any) {
        newPassword.isSecureTextEntry = !newPassword.isSecureTextEntry
    }
    
    @IBAction func confirmPassEye(_ sender: Any) {
        confirmPassword.isSecureTextEntry = !confirmPassword.isSecureTextEntry
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        
       guard let currentPassword = self.currentPassword.text,!currentPassword.isEmpty else{
        Utilities.showAlertView("Enter Current Password", onView: self)

            return
        }
        guard let newPassword = self.newPassword.text,!newPassword.isEmpty,newPassword.count >= 8 else{
            Utilities.showAlertView("Enter Valid New Password ", onView: self)

            return
        }
        guard let confirmPassword = self.confirmPassword.text,!confirmPassword.isEmpty,confirmPassword.contains(newPassword) else{
            Utilities.showAlertView("Password Does not Match", onView: self)
            return
        }
        
        
        let params = ["current_password":"\(currentPassword)","new_password":"\(newPassword)","confirm_password":"\(confirmPassword)","user_id":"\(Constants.userId)"] as [String:String]
        print("\(params)")
        let url = "\(Constants.k_Webservice_URL)changepassword"
        WebServiceHandler.sharedInstance.commonPostRequest(url: url, params: params) { JSON in
            print("\(JSON)")
            
            guard let StatusCode = JSON["StatusCode"].int else{
                return
            }
            guard let Message = JSON["Message"].string else{
                return
            }
            if(StatusCode == 200)
            {
              Utilities.showAlertView("\(Message)", onView: self)
            }
            if(StatusCode == 500)
            {
                Utilities.showAlertView("\(Message)", onView: self)
            }
            
            
            
            
            
            
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChangePasswordViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        
    }
}
