//
//  EditProfileViewController.swift
//  Taggery
//
//  Created by Gowsika on 11/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Cloudinary
import ParallaxHeader
import SDWebImage

class editprofileNormalCell : UITableViewCell{
    
    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var validateUserImage: UIImageView!
    
    
    
}


class EditProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var footerView: UIView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var coverImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var editProfileTableView: UITableView!
    var currenttextfield = UITextField()
    
    var firstNameText = ""
    var lastNameText = ""
    var userNameText = ""
    var relationShipStatus = ""
    var userid = ""
    var emailid = ""
    var gender = ""
    var isUserNameValid = true
    var defaultusername = ""
    var picurl = ""
    var imagePicker = UIImagePickerController()
    var coverPhoto = ""
    var userDescription = ""
    
    var isCoverImage = false

    @IBOutlet var headerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.isUserInteractionEnabled = true
        imagePicker.delegate = self
        Utilities.setBackgroungImage(self.view)
        self.editProfileTableView.delegate = self
        self.editProfileTableView.dataSource = self
        self.editProfileTableView.separatorStyle = .none
        self.editProfileTableView.estimatedRowHeight = 100
        self.editProfileTableView.rowHeight = UITableViewAutomaticDimension
        getuserprofiles()
        self.editProfileTableView.reloadData()
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.black.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        self.headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.bounds.height / 1.5)
        self.headerView.isUserInteractionEnabled = true
        self.editProfileTableView.isUserInteractionEnabled = true
        self.coverImageConstraint.constant = self.headerView.frame.height / 2
        //self.mainContent.addSubview(headerView)
        // Utilities.setBackgroungImage(self.Scrollviews)
        self.editProfileTableView.parallaxHeader.view = self.headerView
        self.editProfileTableView.parallaxHeader.height = self.view.bounds.height / 2.0
        self.editProfileTableView.parallaxHeader.minimumHeight = 0
        
        let gesturerecognizer = UITapGestureRecognizer(target: self, action: #selector(self.opencamera))
        profileImage.isUserInteractionEnabled = true
        gesturerecognizer.numberOfTapsRequired = 1
        gesturerecognizer.numberOfTouchesRequired = 1
        profileImage.addGestureRecognizer(gesturerecognizer)
        
        let gesturerecognizerCover = UITapGestureRecognizer(target: self, action: #selector(self.openCover))
        coverImageView.isUserInteractionEnabled = true
        gesturerecognizerCover.numberOfTapsRequired = 1
        gesturerecognizerCover.numberOfTouchesRequired = 1
        coverImageView.addGestureRecognizer(gesturerecognizerCover)
        self.editProfileTableView.tableFooterView = footerView
     //   coverImageView.isUserInteractionEnabled = true
      //  self.headerView.addGestureRecognizer(gesturerecognizer)
        
      // Do any additional setup after loading the view.
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @IBAction func updateButon(_ sender: Any) {
        self.updatedata()
    }
    
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    
    func getuserprofiles()
    {
        self.userid = Constants.userId
        
        let userdetails = Utilities.getJSON("userDetail")
        self.firstNameText = userdetails["UserFirstName"].string ?? ""
        self.lastNameText = userdetails["UserLastName"].string ?? ""
        self.userNameText = userdetails["UserName"].string ?? ""
        self.relationShipStatus = userdetails["UserRelationshipStatus"].string ?? ""
       //self.userid = "\(userdetails["id"])"
        self.emailid = userdetails["UserEmail"].string ?? ""
        self.gender = userdetails["UserGender"].string ?? ""
        self.picurl = userdetails["UserPicture"].string ?? ""
        self.coverPhoto = userdetails["UserCoverPhoto"].string ?? ""
        self.userDescription = userdetails["UserDescription"].string ?? ""
        if(self.picurl != nil || self.picurl != "")
        {
          let picUrl = "\(Constants.taggeryProfileUrl)\(self.picurl)"
            
          self.profileImage.sd_setImage(with: URL(string: picUrl), placeholderImage: UIImage(named: "placeHolder.jpg"))
         
        }
        if(self.coverPhoto != nil || self.coverPhoto != ""){
            let coverPhoto = "\(Constants.taggeryProfileUrl)\(self.coverPhoto)"
            self.coverImageView.sd_setImage(with: URL(string: coverPhoto), placeholderImage: UIImage(named: "placeHolder.jpg"))
        }
        self.defaultusername = self.userNameText
        self.editProfileTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        if(indexPath.row == 0)
        {
          let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "First Name"
            cells.validateUserImage.isHidden = true
            cells.firstNameTextField.delegate = self
            cells.firstNameTextField.text = self.firstNameText
            cells.firstNameTextField.tag = indexPath.row
            cell = cells
        }
        if(indexPath.row == 1)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "Last Name"
            cells.validateUserImage.isHidden = true
            cells.firstNameTextField.delegate = self
            cells.firstNameTextField.text = self.lastNameText
            cells.firstNameTextField.tag = indexPath.row
            cell = cells
        }
        if(indexPath.row == 2)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "User Name"
            cells.validateUserImage.isHidden = isUserNameValid
            cells.firstNameTextField.delegate = self
            cells.firstNameTextField.text = self.userNameText
            cells.firstNameTextField.tag = indexPath.row
            cells.validateUserImage.image = UIImage(named: "Tick.png")
            cell = cells
        }
        
        if(indexPath.row == 3)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "Email ID"
            cells.validateUserImage.isHidden = true
            cells.validateUserImage.image = UIImage(named: "Tick.png")
            cells.firstNameTextField.text = self.emailid
            cells.firstNameTextField.tag = indexPath.row
            cells.firstNameTextField.isEnabled = true
            cells.selectionStyle = .none
            cells.firstNameTextField.delegate = self
            cell = cells
        }
        if(indexPath.row == 4)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "Gender"
            cells.validateUserImage.isHidden = false
            cells.validateUserImage.image = UIImage(named: "down.png")
            cells.firstNameTextField.text = self.gender
            cells.firstNameTextField.tag = indexPath.row
            cells.firstNameTextField.isEnabled = false
            cells.selectionStyle = .none
            cell = cells
        }
        
        if(indexPath.row == 5)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "RelationShip Status"
            cells.validateUserImage.isHidden = false
            cells.validateUserImage.image = UIImage(named: "down.png")
            cells.firstNameTextField.text = self.relationShipStatus
            cells.firstNameTextField.tag = indexPath.row
            cells.firstNameTextField.isEnabled = false
            cells.selectionStyle = .none
            cell = cells
        }
        if(indexPath.row == 6)
        {
            let cells = tableView.dequeueReusableCell(withIdentifier: "editnamecell", for: indexPath) as! editprofileNormalCell
            cells.firstNameTextField.placeholder = "Add about your description"
            cells.validateUserImage.isHidden = true
            cells.firstNameTextField.text = self.userDescription
            cells.firstNameTextField.tag = indexPath.row
            cells.firstNameTextField.isEnabled = true
            cells.selectionStyle = .none
            cells.firstNameTextField.delegate = self
            cell = cells
        }
        return cell
    }
    
    
    
    
   
    func updatedata()
    {
        currenttextfield.resignFirstResponder()
        if(firstNameText.count < 2)
        {
            Utilities.showAlertView("Enter Valid Firstname", onView: self)
            return
        }
        if(lastNameText.count < 2)
        {
            Utilities.showAlertView("Enter Valid Lastname", onView: self)
            return
        }
        if(isUserNameValid == false)
        {
            Utilities.showAlertView("Enter Valid Username", onView: self)
            return
        }
        let params = ["user_id":"\(self.userid)","UserName":"\(self.userNameText)","FirstName":"\(self.firstNameText)","LastName":"\(self.lastNameText)","RelationShipStatus":"\(self.relationShipStatus)","UserPicture":self.picurl,"EmailID":"\(self.emailid)","Gender":"\(self.gender)","UserDescription":"\(self.userDescription)","UserCoverPhoto":"\(self.coverPhoto)"]
        let urls = "\(Constants.k_Webservice_URL)editprofile"
        WebServiceHandler.sharedInstance.commonPostRequest(url: urls, params: params) { JSON in
        print("\(JSON)")
        let statuscode = JSON["StatusCode"].int
        let message = JSON["Message"].string
        Utilities.showAlertView(message!, onView: self)

        if(statuscode == 200){
                let userDict = JSON["UserDetails"]
                UserDefaults.standard.set(userDict.rawString(), forKey: "userDetail")
            }
        }
    }
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 5){
            self.relationShipTapped()
        }
        if(indexPath.row == 4){
            self.genderTapped()
        }
    }
    
    @objc func openCover(){
        isCoverImage = true
        let alertcontroller = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        var cameraButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        var galleryButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertcontroller.dismiss(animated: false, completion: nil)
        }
        
        alertcontroller.addAction(cancelActionButton)
        alertcontroller.addAction(cameraButton)
        alertcontroller.addAction(galleryButton)
        self.navigationController!.present(alertcontroller, animated: true, completion: {
            alertcontroller.view.superview?.isUserInteractionEnabled = true
            alertcontroller.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    @objc func opencamera(){
        isCoverImage = false
        let alertcontroller = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        var cameraButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        var galleryButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertcontroller.dismiss(animated: false, completion: nil)
        }
        
        alertcontroller.addAction(cancelActionButton)
        alertcontroller.addAction(cameraButton)
        alertcontroller.addAction(galleryButton)
        self.navigationController!.present(alertcontroller, animated: true, completion: {
            alertcontroller.view.superview?.isUserInteractionEnabled = true
            alertcontroller.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    func genderTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let maleButton = UIAlertAction(title: "Male", style: .default, handler: { (action) -> Void in
            print("Male")
            self.gender = "Male"
            let indexPath = IndexPath(row: 4, section: 0)
            self.editProfileTableView.reloadRows(at: [indexPath], with: .none)
        })
        let  femaleButton = UIAlertAction(title: "Female", style: .default, handler: { (action) -> Void in
            print("Female")
            self.gender = "Female"
            let indexPath = IndexPath(row: 4, section: 0)
            self.editProfileTableView.reloadRows(at: [indexPath], with: .none)
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertController.dismiss(animated: false, completion: nil)
        }
        
        alertController.addAction(cancelActionButton)
        alertController.addAction(maleButton)
        alertController.addAction(femaleButton)
        self.navigationController!.present(alertController, animated: true, completion: {
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    func relationShipTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let singleButton = UIAlertAction(title: "Single", style: .default, handler: { (action) -> Void in
            print("Single")
            self.relationShipStatus = "Single"
            let indexPath = IndexPath(row: 5, section: 0)
            self.editProfileTableView.reloadRows(at: [indexPath], with: .none)
        })
        let  complecatedButton = UIAlertAction(title: "Complicated", style: .default, handler: { (action) -> Void in
            print("Complicated")
            self.relationShipStatus = "Complicated"
            self.editProfileTableView.reloadData()
            let indexPath = IndexPath(row: 3, section: 0)
            self.editProfileTableView.reloadRows(at: [indexPath], with: .none)
        })
        let relationShipButton = UIAlertAction(title: "In a Relationship", style: .default, handler: { (action) -> Void in
            print("In a Relationship")
            self.relationShipStatus = "In a Relationship"
            self.editProfileTableView.reloadData()
            let indexPath = IndexPath(row: 5, section: 0)
            self.editProfileTableView.reloadRows(at: [indexPath], with: .none)
        })
        let  noneButton = UIAlertAction(title: "None", style: .default, handler: { (action) -> Void in
            print("None")
            self.relationShipStatus = "None"
            self.editProfileTableView.reloadData()
            let indexPath = IndexPath(row: 5, section: 0)
            self.editProfileTableView.reloadRows(at: [indexPath], with: .none)
        })
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel Tapped")
            alertController.dismiss(animated: false, completion: nil)
        }
        
        alertController.addAction(cancelActionButton)
        alertController.addAction(singleButton)
        alertController.addAction(complecatedButton)
        alertController.addAction(relationShipButton)
        alertController.addAction(noneButton)
        self.navigationController!.present(alertController, animated: true, completion: {
            
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currenttextfield = textField
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 0 ){
            if let text = textField.text,
                let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange,
                                                           with: string)
                if(updatedText.count >= 8)
                {
                    return false
                }
            }
        }
        if(textField.tag == 1 ){
            if let text = textField.text,
                let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange,
                                                           with: string)
                if(updatedText.count >= 8)
                {
                    return false
                }
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currenttextfield = textField
        if(textField.tag == 0){
            self.firstNameText = textField.text!
        }
        if(textField.tag == 1){
            self.lastNameText = textField.text!
        }
        if(textField.tag == 2){
            self.userNameText = textField.text!
            if(self.userNameText.contains(self.defaultusername)){
                isUserNameValid = true
            }
            else{
                WebServiceHandler.sharedInstance.userNameCheck(name: self.userNameText, CountryCode: "", Phonenumber: "", Type: "1", viewController: self)
            }
        }
        if(textField.tag == 3){
            if(Utilities.isValidEmail(testStr: textField.text!)){
               self.emailid = textField.text!
            }
            else
            {
                Utilities.showAlertView("Enter Valid Email", onView: self)
            }
        }
        if(textField.tag == 6){
            self.userDescription = textField.text!
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.tag == 2){
        WebServiceHandler.sharedInstance.userNameCheck(name: self.userNameText, CountryCode: "", Phonenumber: "", Type: "1", viewController: self)
        }
        return true
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    func didReceiveUserNameCheck(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
           // userNameValidImageView.isHidden = false
            isUserNameValid = true
        }else{
           // userNameValidImageView.isHidden = true
            isUserNameValid = false
        }
        let indexPaths = IndexPath(row: 3, section: 0)
        self.editProfileTableView.reloadRows(at: [indexPaths], with: .none)
        
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func uploadimagedata(image:UIImage)
    {
        
        LoadingIndicatorView.show()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        let config = CLDConfiguration(cloudName: "cloudtaggery18", apiKey: "122236358764349")
        let cloudinary = CLDCloudinary(configuration: config)
        var publicName = "IMG-\(Date().toMillis()!)"
        let params = CLDUploadRequestParams().setFolder("Profile").setPublicId(publicName)
        params.setResourceType(.image)
        //    let request = cloudinary.createUploader().upload(url: imageUrl, uploadPreset: "Sample_preset")
        let uploader = cloudinary.createUploader()
        
        uploader.upload(data: imageData!, uploadPreset: "Sample_preset", params: params, progress: { Progress in
            
        }) { result,error in
            LoadingIndicatorView.hide()
            if(error == nil){
                print("Result \(result)")
                let fetch = result?.url
                var fetchedUrl = URL(string: fetch!)
                print("FETCH URL is \(fetchedUrl!)")
                if(self.isCoverImage){
                    self.coverImageView.sd_setImage(with: URL(string: fetch!), placeholderImage: UIImage(named: "placeHolder.jpg"))
                    self.coverPhoto = "\(fetchedUrl!.lastPathComponent)"
                }
                else
                {
                    self.profileImage.sd_setImage(with: URL(string: fetch!), placeholderImage: UIImage(named: "placeHolder.jpg"))
                    self.picurl = "\(fetchedUrl!.lastPathComponent)"
                }
                    
            }
            else
            {
                print("Result \(error)")
                
            }
            
        }
        
    }
}
extension EditProfileViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            Utilities.showAlertView("No Image Chosen", onView: self)
            return
        }
        
        
        
        
        //  self.userProfile.image = image
        dismiss(animated: true) {
            self.uploadimagedata(image: image)
        }
    }
    
}
extension UIImagePickerController{
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationBar.topItem?.rightBarButtonItem?.tintColor = UIColor.black
        self.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
    }

}
