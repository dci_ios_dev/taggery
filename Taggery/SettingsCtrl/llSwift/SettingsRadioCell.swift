//
//  SettingsRadioCell.swift
//  Taggery
//
//  Created by Gowsika on 11/12/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class SettingsRadioCell: UITableViewCell {

    @IBOutlet weak var switchRadioCell: UISwitch!
    
    @IBOutlet weak var switchLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
