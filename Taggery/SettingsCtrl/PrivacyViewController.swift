//
//  PrivacyViewController.swift
//  Taggery
//
//  Created by Gowsika on 18/01/19.
//  Copyright © 2019 Aravind Kumar. All rights reserved.
//

import UIKit

class PrivacyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        Utilities.setBackgroungImage(self.view)
        tableView.delegate = self
        tableView.dataSource = self
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        self.tableView.addGestureRecognizer(swipeRight)
        
        // Do any additional setup after loading the view.
    }
    @objc  func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            self.navigationController?.popViewController(animated: true)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
            
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PrivacyViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()

        if(indexPath.row == 0){
            let formcell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SettingsRadioCell
            formcell.switchRadioCell.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
            formcell.switchRadioCell.backgroundColor = UIColor.clear
            formcell.switchLabel.text = "Notification "
            formcell.switchLabel.textColor = UIColor.white
            return formcell
        }
        if(indexPath.row == 1){
            let formcell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SettingsRadioCell
            formcell.switchRadioCell.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
            formcell.switchRadioCell.backgroundColor = UIColor.clear
            formcell.switchLabel.text = "Deactivate Likes & Ranks"
            formcell.switchLabel.textColor = UIColor.white
            return formcell
        }
        return cell
        
        }
}
