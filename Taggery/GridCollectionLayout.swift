//
//  GridCollectionLayout.swift
//  Universal
//
//  Created by Aravind on 27/11/18.
//  Copyright © 2018 Aravind. All rights reserved.
//

import UIKit

class GridCollectionLayout: UICollectionViewFlowLayout {
    
    // here you can define the height of each cell
    var itemHeight: CGFloat = 120
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout()
    {
        //itemHeight = ((collectionView?.frame.size.width)! / 3) - 1
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .vertical
        
    }
    
    func itemWidth() -> CGFloat
    {
        return ((collectionView?.frame.size.width)! / 3) - 1
    }
    
    override var itemSize: CGSize {
        set {
                self.itemSize = CGSize(width: itemWidth(), height: itemWidth())
        }
        get {
            return CGSize(width: itemWidth(), height: itemWidth())
        }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
